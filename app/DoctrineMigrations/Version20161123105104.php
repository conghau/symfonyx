<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161123105104 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $employee = $schema->createTable('employees');
        $employee->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $employee->addColumn('last_name', 'string', array('length' => 30));
        $employee->addColumn('first_name', 'string', array('length' => 30));
        $employee->addColumn('address', 'string', array('length' => 50));
        $employee->addColumn('status', Type::BOOLEAN, ['default' => true]);
    }
    
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $schema->dropTable('employees');
        
    }
}
