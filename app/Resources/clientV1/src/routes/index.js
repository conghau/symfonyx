import CoreLayout from '../layouts/CoreLayout/CoreLayout';
import Home from './Home';
import * as importRouting from './UploadMapping';
import * as billingRouting from './Billing';
import * as iCareMemberRouting from './ICareMember';

export function createRoutes(store) {
    return {
        path: '/',
        component: CoreLayout,
        indexRoute: Home,
        childRoutes: [
            importRouting.uploadFile(store),
            importRouting.selectMappings(store),
            importRouting.validateMappings(store),
            importRouting.processingResults(store),
            importRouting.uploadSummary(store),
            importRouting.employeeData(store),

            //Billing
            billingRouting.billingSetting(store),
            billingRouting.billingDetail(store),
            billingRouting.cycleTemplate(store),
            billingRouting.customerOverview(store),
            billingRouting.adminOverview(store),
            billingRouting.billingRecord(store),
            billingRouting.confirmPayment(store),

            //ICare Member
            iCareMemberRouting.iCareMember(store),
            iCareMemberRouting.iCareMembers(store)
        ]
    };
}

export default createRoutes;
