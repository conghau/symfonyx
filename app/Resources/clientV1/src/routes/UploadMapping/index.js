import {injectReducer} from 'reducers/index';

export function uploadFile(store) {
    return {
        path: 'employee/import/mapping/upload',
        /*  Async getComponent is only invoked when route matches   */
        getComponent (nextState, cb) {
            require.ensure([], (require) => {
                /*  Webpack - use require callback to define
                 dependencies for bundling   */
                const UploadPage = require('containers/Import/UploadPage').default;
                const uploadFileReducer = require('reducers/Import/uploadFileReducer').default;

                /*  Add the reducer to the store on key 'counter'  */
                injectReducer(store, 'uploadFile', uploadFileReducer);

                /*  Return getComponent   */
                cb(null, UploadPage);

                /* Webpack named bundle   */
            }, 'import-file');
        }
    };
}

export function selectMappings(store) {
    return {
        path: 'employee/import/mapping/select/:id',
        getComponent (nextState, cb) {
            require.ensure([], (require) => {
                const MappingPage = require('containers/Import/MappingPage').default;
                const mappingReducer = require('reducers/Import/mappingReducer').default;
                injectReducer(store, 'selectMappings', mappingReducer);
                cb(null, MappingPage);
            }, 'import-file');
        }
    };
}

export function validateMappings(store) {
    return {
        path: 'employee/import/mapping/validate/:id',
        getComponent (nextState, cb) {
            require.ensure([], (require) => {
                const MappingValidatePage = require('../../containers/Import/MappingValidatePage').default;
                const validateMappingReducer = require('../../reducers/Import/validateMappingReducer').default;
                injectReducer(store, 'validateMappings', validateMappingReducer);
                cb(null, MappingValidatePage);
            }, 'import-file');
        }
    };
}

export function processingResults(store) {
    return {
        path: 'employee/import/mapping/processing/:id',
        getComponent (nextState, cb) {
            require.ensure([], (require) => {
                const ProcessingStatisticPage = require('../../containers/Import/ProcessingStatisticPage').default;
                const processingReducer = require('../../reducers/Import/processingReducer').default;
                injectReducer(store, 'processingResults', processingReducer);
                cb(null, ProcessingStatisticPage);
            }, 'import-file');
        }
    };
}
export function uploadSummary(store) {
    return {
        path: 'employee/import/mapping/statistic/:id',
        getComponent (nextState, cb) {
            require.ensure([], (require) => {
                const StatisticPage = require('../../containers/Import/StatisticPage').default;
                const uploadSummaryReducer = require('../../reducers/Import/uploadSummaryReducer').default;
                injectReducer(store, 'uploadSummary', uploadSummaryReducer);
                cb(null, StatisticPage);
            }, 'import-file');
        }
    };
}
export function employeeData(store) {
    return {
        path: 'employee/import/mapping/resolve/:transId/:id',
        getComponent (nextState, cb) {
            require.ensure([], (require) => {
                const ResolvePage = require('../../containers/Import/ResolvePage').default;
                const resolveReducer = require('../../reducers/Import/resolveReducer').default;
                injectReducer(store, 'employeeData', resolveReducer);
                cb(null, ResolvePage);
            }, 'import-file');
        }
    };
}

