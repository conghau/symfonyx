import {injectReducer} from 'reducers/index';

export function iCareMember(store) {
    return {
        path: 'icare/employee/icare-members',
        /*  Async getComponent is only invoked when route matches   */
        getComponent (nextState, cb) {
            require.ensure([], (require) => {
                /*  Webpack - use require callback to define
                 dependencies for bundling   */
                const ICareMemberPage = require('../../containers/ICareMember/ICareMemberPage').default;
                const iCareMemberReducer = require('../../reducers/ICareMember/iCareMemberReducer').default;

                /*  Add the reducer to the store on key 'counter'  */
                injectReducer(store, 'iCareMember', iCareMemberReducer);

                /*  Return getComponent   */
                cb(null, ICareMemberPage);

                /* Webpack named bundle   */
            }, 'iCare-member');
        }
    };
}

export function iCareMembers(store) {
    return {
        path: 'icare/employee/list',
        /*  Async getComponent is only invoked when route matches   */
        getComponent (nextState, cb) {
            require.ensure([], (require) => {
                /*  Webpack - use require callback to define
                 dependencies for bundling   */
            const ICareMembersPage = require('../../containers/ICareMember/ICareMembersPage').default;
            const iCareMembersReducer = require('../../reducers/ICareMember/iCareMembersReducer').default;

            /*  Add the reducer to the store on key 'counter'  */
            injectReducer(store, 'iCareMembers', iCareMembersReducer);

            /*  Return getComponent   */
            cb(null, ICareMembersPage);

            /* Webpack named bundle   */
        }, 'iCare-member');
        }
    };
}
