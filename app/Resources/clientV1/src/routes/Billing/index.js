import {injectReducer} from 'reducers/index';

    export function billingSetting(store) {
        return {
            path: '/billing/setting/index',
            /*  Async getComponent is only invoked when route matches   */
            getComponent (nextState, cb) {
                require.ensure([], (require) => {
                    /*  Webpack - use require callback to define
                     dependencies for bundling   */
                    const DetailPage = require('../../containers/Billing/DetailPage').default;
                    const settingReducer = require('../../reducers/Billing/settingReducer').default;

                    /*  Add the reducer to the store on key 'counter'  */
                    injectReducer(store, 'billingSetting', settingReducer);

                    /*  Return getComponent   */
                    cb(null, DetailPage);

                    /* Webpack named bundle   */
                }, 'billing');
            }
        };
    }

export function billingDetail(store) {
    return {
        path: '/billing/detail/:id',
        /*  Async getComponent is only invoked when route matches   */
        getComponent (nextState, cb) {
            require.ensure([], (require) => {
                /*  Webpack - use require callback to define
                 dependencies for bundling   */
                const DetailPage = require('../../containers/Billing/DetailPage').default;
                const settingReducer = require('../../reducers/Billing/settingReducer').default;

                /*  Add the reducer to the store on key 'counter'  */
                injectReducer(store, 'billingSetting', settingReducer);

                /*  Return getComponent   */
                cb(null, DetailPage);

                /* Webpack named bundle   */
            }, 'billing');
        }
    };
}

export function cycleTemplate(store) {
    return {
        path: '/billing/cycle-template',
        /*  Async getComponent is only invoked when route matches   */
        getComponent (nextState, cb) {
            require.ensure([], (require) => {
                /*  Webpack - use require callback to define
                 dependencies for bundling   */
                const CycleTemplatePage = require('../../containers/Billing/CycleTemplatePage').default;
                const cycleTemplateReducer = require('../../reducers/Billing/cycleTemplateReducer').default;

                /*  Add the reducer to the store on key 'counter'  */
                injectReducer(store, 'cycleTemplate', cycleTemplateReducer);

                /*  Return getComponent   */
                cb(null, CycleTemplatePage);

                /* Webpack named bundle   */
            }, 'billing');
        }
    };
}

export function customerOverview(store) {
    return {
        path: '/billing/customer/overview',
        getComponent (nextState, cb) {
            require.ensure([], (require) => {
                const CustomerOverviewPage = require('../../containers/Billing/CustomerOverviewPage').default;
                const customerOverviewReducer = require('../../reducers/Billing/customerOverviewReducer').default;

                injectReducer(store, 'customerOverview', customerOverviewReducer);
                cb(null, CustomerOverviewPage);
            }, 'billing');
        }
    };
}

export function adminOverview(store) {
    return {
        path: '/billing/admin/overview',
        getComponent (nextState, cb) {
            require.ensure([], (require) => {
                const AdminOverviewPage = require('../../containers/Billing/AdminOverviewPage').default;
                const adminOverviewReducer = require('../../reducers/Billing/adminOverviewReducer').default;
                injectReducer(store, 'adminOverview', adminOverviewReducer);
                cb(null, AdminOverviewPage);
            }, 'billing');
        }
    };
}

export function billingRecord(store) {
    return {
        path: '/billing/record-payment/:id',
        /*  Async getComponent is only invoked when route matches   */
        getComponent (nextState, cb) {
            require.ensure([], (require) => {
                /*  Webpack - use require callback to define
                 dependencies for bundling   */
                const RecordPaymentPage = require('../../containers/Billing/RecordPaymentPage').default;
                const recordPaymentReducer = require('../../reducers/Billing/recordPaymentReducer').default;

                /*  Add the reducer to the store on key 'counter'  */
                injectReducer(store, 'recordPayment', recordPaymentReducer);

                /*  Return getComponent   */
                cb(null, RecordPaymentPage);

                /* Webpack named bundle   */
            }, 'billing');
        }
    };
}

export function confirmPayment(store) {
    return {
        path: '/billing/confirm-payment/:billingId/:paymentId',
        /*  Async getComponent is only invoked when route matches   */
        getComponent (nextState, cb) {
            require.ensure([], (require) => {
                /*  Webpack - use require callback to define
                 dependencies for bundling   */
                const ConfirmPaymentPage = require('../../containers/Billing/ConfirmPaymentPage').default;
                const confirmPaymentReducer = require('../../reducers/Billing/confirmPaymentReducer').default;

                /*  Add the reducer to the store on key 'counter'  */
                injectReducer(store, 'confirmPayment', confirmPaymentReducer);

                /*  Return getComponent   */
                cb(null, ConfirmPaymentPage);

                /* Webpack named bundle   */
            }, 'billing');
        }
    };
}