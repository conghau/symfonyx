import React, {PropTypes} from 'react';
import {FormGroup, FormControl, ControlLabel, HelpBlock, InputGroup, Button} from 'react-bootstrap';
import {FormattedNumber, FormattedMessage, FormattedDate} from 'react-intl';

class PersonalInfo extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            isActive: props.personalInfo.isActive || '1',
            creditLimit: props.personalInfo.creditLimit || '',
            dueLimit: props.personalInfo.dueLimit || ''
        };
        this.handleChangeCreditLimit = this.handleChangeCreditLimit.bind(this);
        this.handleChangeDueLimit = this.handleChangeDueLimit.bind(this);
        this.handleChangeInactivate = this.handleChangeInactivate.bind(this);
        this.handleCancelChange = this.handleCancelChange.bind(this);
        this.handleSubmitForm = this.handleSubmitForm.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        const newState = {
            isActive: nextProps.personalInfo.isActive || '1',
            creditLimit: nextProps.personalInfo.creditLimit || '',
            dueLimit: nextProps.personalInfo.dueLimit || ''
        };
        this.setState(newState);
    }

    handleCancelChange() {
        const newState = {
            isActive: this.props.personalInfo.isActive || '1',
            creditLimit: this.props.personalInfo.creditLimit || '',
            dueLimit: this.props.personalInfo.dueLimit || ''
        };
        this.setState(newState);
    }

    handleSubmitForm(event) {
        event.preventDefault();
        this.props.onSave(this.state);
    }

    handleChangeInactivate(event) {
        this.setState({isActive: event.target.checked ? '0' : '1'});
    }

    handleChangeCreditLimit(event) {
        this.setState({creditLimit: event.target.value});
    }

    handleChangeDueLimit(event) {
        this.setState({dueLimit: event.target.value});
    }

    render() {
        return (
            <div className="portlet light m-b-none">
                <div className="portlet-body form">
                    <form onSubmit={this.handleSubmitForm}>
                        <div className="form-body">
                            <div className="row">
                                <div className="col-md-4">
                                    <FormGroup>
                                        <ControlLabel> <FormattedMessage id="personal_info.name"/></ControlLabel>
                                        <FormControl type="text" value={this.props.personalInfo.fullName || ''}
                                                     disabled/>
                                    </FormGroup>
                                </div>
                                <div className="col-md-4">
                                    <FormGroup>
                                        <ControlLabel> <FormattedMessage id="personal_info.employee_id"/></ControlLabel>
                                        <FormControl type="text" value={this.props.personalInfo.employeeId || ''}
                                                     disabled/>
                                    </FormGroup>
                                </div>
                                <div className="col-md-4">
                                    <FormGroup>
                                        <ControlLabel>&nbsp;</ControlLabel>
                                        <FormattedMessage id="personal_info.inactivate">{ (msg) =>
                                            <label className="mt-checkbox mt-checkbox-outline">{msg}
                                                <input type="checkbox" value="1" disabled={this.props.isDisable}
                                                       checked={this.state.isActive == '0'}
                                                       onChange={this.handleChangeInactivate}/>
                                                <span/>
                                            </label>
                                        }</FormattedMessage>
                                    </FormGroup>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-4">
                                    <FormGroup>
                                        <ControlLabel><FormattedMessage id="personal_info.social_id"/></ControlLabel>
                                        <FormControl type="text" value={this.props.personalInfo.socialId || ''}
                                                     disabled/>
                                    </FormGroup>
                                </div>
                                <div className="col-md-4">
                                    <FormGroup>
                                        <ControlLabel><FormattedMessage id="personal_info.job_title"/></ControlLabel>
                                        <FormControl type="text" value={this.props.personalInfo.jobTitle || ''}
                                                     disabled/>
                                    </FormGroup>
                                </div>
                                <div className="col-md-4">
                                    <FormGroup
                                        bsClass={this.props.errors && this.props.errors.hasOwnProperty('creditLimit') ? 'form-group has-error' : 'form-group'}>
                                        <ControlLabel><FormattedMessage id="personal_info.credit_limit"/></ControlLabel>
                                        <FormControl type="text" value={this.state.creditLimit}
                                                     onChange={this.handleChangeCreditLimit}
                                                     disabled={this.props.isDisable}/>
                                        {this.props.errors && this.props.errors.hasOwnProperty('creditLimit') &&
                                        <HelpBlock
                                            bsClass="help-block"><FormattedMessage
                                            id={'personal_info.errors.' + this.props.errors['creditLimit']}/></HelpBlock>}
                                    </FormGroup>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-4">
                                    <FormGroup>
                                        <ControlLabel><FormattedMessage id="personal_info.gender"/></ControlLabel>
                                        <FormControl componentClass="select"
                                                     value={this.props.personalInfo.gender || 3} disabled>
                                            <FormattedMessage id="personal_info.male">{(msg) => <option
                                                value="1">{msg}</option> }</FormattedMessage>
                                            <FormattedMessage id="personal_info.female">{(msg) => <option
                                                value="2">{msg}</option> }</FormattedMessage>
                                            <FormattedMessage id="personal_info.not_specified">{(msg) => <option
                                                value="3">{msg}</option> }</FormattedMessage>
                                        </FormControl>
                                    </FormGroup>
                                </div>
                                <div className="col-md-4">
                                    <FormGroup>
                                        <ControlLabel><FormattedMessage id="personal_info.department"/></ControlLabel>
                                        <FormControl type="text" value={this.props.personalInfo.department || ''}
                                                     disabled/>
                                    </FormGroup>
                                </div>
                                <div className="col-md-4">
                                    <FormGroup
                                        bsClass={this.props.errors && this.props.errors.hasOwnProperty('dueLimit') ? 'form-group has-error' : 'form-group'}>
                                        <ControlLabel><FormattedMessage id="personal_info.due_limit"/></ControlLabel>
                                        <FormControl type="text" value={this.state.dueLimit}
                                                     onChange={this.handleChangeDueLimit}
                                                     disabled={this.props.isDisable}/>
                                        {this.props.errors && this.props.errors.hasOwnProperty('dueLimit') && <HelpBlock
                                            bsClass="help-block"><FormattedMessage
                                            id={'personal_info.errors.' + this.props.errors['dueLimit']}/></HelpBlock>}
                                    </FormGroup>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-4">
                                    <FormGroup>
                                        <ControlLabel><FormattedMessage id="personal_info.day_of_birth"/></ControlLabel>
                                        <InputGroup bsClass="input-group date date-picker">
                                            <FormControl type="text" value={this.props.personalInfo.birthDate || ''}
                                                         disabled/>
                                            <InputGroup.Button>
                                                <Button bsClass="btn default" disabled><i
                                                    className="fa fa-calendar"/></Button>
                                            </InputGroup.Button>
                                        </InputGroup>
                                    </FormGroup>
                                </div>
                                <div className="col-md-4">
                                    <FormGroup>
                                        <ControlLabel><FormattedMessage id="personal_info.salary"/></ControlLabel>
                                        <FormControl type="text" value={this.props.personalInfo.salary || ''} disabled/>
                                    </FormGroup>
                                </div>
                                <div className="col-md-4">
                                    <FormGroup>
                                        <ControlLabel><FormattedMessage
                                            id="personal_info.maternity_leave_start"/></ControlLabel>
                                        <InputGroup bsClass="input-group date date-picker">
                                            <FormControl type="text"
                                                         value={this.props.personalInfo.maternityLeaveStart || ''}
                                                         disabled/>
                                            <InputGroup.Button>
                                                <Button bsClass="btn default" disabled><i
                                                    className="fa fa-calendar"/></Button>
                                            </InputGroup.Button>
                                        </InputGroup>
                                    </FormGroup>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-4">
                                    <FormGroup>
                                        <ControlLabel><FormattedMessage id="personal_info.phone"/></ControlLabel>
                                        <FormControl type="text" value={this.props.personalInfo.phone || ''} disabled/>
                                    </FormGroup>
                                </div>
                                <div className="col-md-4">
                                    <FormGroup>
                                        <ControlLabel><FormattedMessage id="personal_info.hiring_date"/></ControlLabel>
                                        <InputGroup bsClass="input-group date date-picker">
                                            <FormControl type="text" value={this.props.personalInfo.hiringDate || ''}
                                                         disabled/>
                                            <InputGroup.Button>
                                                <Button bsClass="btn default" disabled><i
                                                    className="fa fa-calendar"/></Button>
                                            </InputGroup.Button>
                                        </InputGroup>
                                    </FormGroup>
                                </div>
                                <div className="col-md-4">
                                    <FormGroup>
                                        <ControlLabel><FormattedMessage
                                            id="personal_info.maternity_leave_end"/></ControlLabel>
                                        <InputGroup bsClass="input-group date date-picker">
                                            <FormControl type="text"
                                                         value={this.props.personalInfo.maternityLeaveEnd || ''}
                                                         disabled/>
                                            <InputGroup.Button>
                                                <Button bsClass="btn default" disabled><i
                                                    className="fa fa-calendar"/></Button>
                                            </InputGroup.Button>
                                        </InputGroup>
                                    </FormGroup>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-4">
                                    <FormGroup>
                                        <ControlLabel><FormattedMessage id="personal_info.email"/></ControlLabel>
                                        <FormControl type="text" value={this.props.personalInfo.email || ''} disabled/>
                                    </FormGroup>
                                </div>
                                <div className="col-md-4">
                                    <FormGroup>
                                        <ControlLabel><FormattedMessage
                                            id="personal_info.termination_date"/></ControlLabel>
                                        <InputGroup bsClass="input-group date date-picker">
                                            <FormControl type="text"
                                                         value={this.props.personalInfo.terminationDate || ''}
                                                         disabled/>
                                            <InputGroup.Button>
                                                <Button bsClass="btn default" disabled><i
                                                    className="fa fa-calendar"/></Button>
                                            </InputGroup.Button>
                                        </InputGroup>
                                    </FormGroup>
                                </div>
                                <div className="col-md-4">
                                    <FormGroup>
                                        <ControlLabel><FormattedMessage
                                            id="personal_info.salary_payment_method"/></ControlLabel>
                                        <FormControl type="text"
                                                     value={this.props.personalInfo.salaryPaymentMethod || ''}
                                                     disabled/>
                                        <HelpBlock bsClass="help-block hide"> This is inline help </HelpBlock>
                                    </FormGroup>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-4">
                                    <FormGroup>
                                        <ControlLabel><FormattedMessage id="personal_info.address"/></ControlLabel>
                                        <FormControl componentClass="textarea" rows="5"
                                                     value={this.props.personalInfo.address || ''} disabled/>
                                        <HelpBlock bsClass="help-block hide"> This is inline help </HelpBlock>
                                    </FormGroup>
                                </div>
                                <div className="col-md-4">
                                    <FormGroup>
                                        <ControlLabel><FormattedMessage
                                            id="personal_info.contract_type"/></ControlLabel>
                                        <FormControl type="text" value={this.props.personalInfo.contractType || ''}
                                                     disabled/>
                                    </FormGroup>
                                </div>
                                <div className="col-md-4">
                                    <FormGroup>
                                        <ControlLabel><FormattedMessage id="personal_info.bank_name"/></ControlLabel>
                                        <FormControl type="text" value={this.props.personalInfo.bankName || ''}
                                                     disabled/>
                                    </FormGroup>
                                </div>
                            </div>
                        </div>
                        <div className="form-actions right">
                            <FormattedMessage id="personal_info.cancel">
                                { (msg) =>
                                    <button type="button" className="btn default" onClick={this.handleCancelChange}
                                            disabled={this.props.isDisable}>{msg}
                                    </button>
                                }
                            </FormattedMessage>
                            <FormattedMessage id="personal_info.save">
                                { (msg) =>
                                    <button type="submit" className="btn green"
                                            disabled={this.props.isDisable}>{msg}</button>
                                }
                            </FormattedMessage>

                        </div>
                    </form>
                </div>
            </div>
        );
    }
}
PersonalInfo.propTypes = {
    personalInfo: PropTypes.object.isRequired,
    onSave: PropTypes.func.isRequired,
    isDisable: PropTypes.bool,
    errors: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ]),
};
export default PersonalInfo;
