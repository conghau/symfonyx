import React, {PropTypes} from 'react';
import ReactDOM from 'react-dom';
import {FormControl} from 'react-bootstrap';
import {FormattedMessage} from 'react-intl';

class SearchBar extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.onClickSearch = this.onClickSearch.bind(this);
    }

    onFormSubmit(event) {
        event.preventDefault();
        this.props.onSearch(this._input.value);
    }

    onClickSearch(event) {
        this.props.onSearch(this._input.value);
    }

    render() {
        return (
            <div className='inputs pull-right'>
                <form className='portlet-input input-inline input-medium' onSubmit={this.onFormSubmit}>
                    <div className='form-body'>
                        <div className='input-icon right'>
                            <i className='icon-magnifier' onClick={this.onClickSearch}/>
                            <FormattedMessage id={this.props.placeHolderText}>
                                { (msg) => < FormControl type='text' bsClass='form-control input-circle'
                                                         placeholder={msg}
                                                         ref={(ref) => this._input = ReactDOM.findDOMNode(ref)}/>
                                }
                            </FormattedMessage>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

SearchBar.propsType = {
    onSearch: PropTypes.func.isRequired,
    placeHolderText: PropTypes.string
};
export default SearchBar;
