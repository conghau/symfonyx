import React, {PropTypes} from 'react';
import Textbox from '../../components/common/Textbox';
import {FormattedNumber, FormattedMessage} from 'react-intl';


class BillingDetailRow extends React.Component {
    constructor(props, context) {
        super(props, context);
        const client = Object.assign({}, props.client);
        const accept = Object.assign({}, props.accept);
        if (accept[client['clientExternalId']]) {
            client.outstandingAmount = accept[client['clientExternalId']]['outstandingAmount'];
        } else {
            if (!client.hasOwnProperty('outstandingAmount')) {
                client.outstandingAmount = client.previousOutstandingAmount + client.principalDueAmount - client.paidAmount;
            }
        }

        this.state = {client: client};
        this.handleChangePaymentAmount = this.handleChangePaymentAmount.bind(this);
        this.handleExcludeClient = this.handleExcludeClient.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        const client = Object.assign({}, nextProps.client);
        if (!client.hasOwnProperty('outstandingAmount')) {
            if (nextProps.readOnlyBill) {
                if (nextProps.accept[client.clientExternalId]) {
                    client.outstandingAmount = nextProps.accept[client.clientExternalId]['outstandingAmount'];
                }
            } else {
                client.outstandingAmount = client.previousOutstandingAmount + client.principalDueAmount - client.paidAmount;
            }
        } else {
            if (isNaN(client.outstandingAmount)) {
                client.outstandingAmount = 0;
            }
        }

        this.setState({client: client});
    }

    handleChangePaymentAmount(client, event) {
        debugger;
        const newClient = Object.assign({}, client, {outstandingAmount: event.target.value});
        this.setState({client: newClient}, function () {
            this.props.handleChangePaymentAmount(client, newClient.outstandingAmount);
        }.bind(this));
    }

    handleExcludeClient(client) {
        const customerId = parseInt(client['clientExternalId']);
        const iCareMemberId = this.props.customers[customerId];
        var dataClient = Object.assign({}, client, {iCareMemberId: iCareMemberId});
        this.props.handleExcludeClient(dataClient);
    }

    render() {
        const client = this.state ? this.state.client : null;
        const customerId = parseInt(client['clientExternalId']);
        const customers = this.props.customers;
        const exclude = this.props.exclude;
        const accept = this.props.accept;

        // do not render if client in exclusion list
        if (exclude[client['clientExternalId']]) {
            return null;
        }

        // Do not has accept list, return null
        if (!accept[client['clientExternalId']]) {
            return null;
        }
        const valuePaymentAmount = accept[client['clientExternalId']]['outstandingAmount'];

        if (client) {
            return (
                <tr>
                    <td>{customers[customerId]}</td>
                    <td>{client.clientName}</td>
                    <td>
                        <FormattedNumber
                            value={client.previousOutstandingAmount}
                            style='currency'
                            minimumFractionDigits={0}
                            currency={iCareCurrency}/>

                    </td>
                    <td>
                        <FormattedNumber
                            value={client.principalDueAmount}
                            style='currency'
                            minimumFractionDigits={0}
                            currency={iCareCurrency}/>

                    </td>
                    <td>
                        <FormattedNumber
                            value={client.paidAmount}
                            style='currency'
                            minimumFractionDigits={0}
                            currency={iCareCurrency}/>

                    </td>
                    <td>
                        <FormattedNumber
                            value={parseFloat(client.previousOutstandingAmount) + parseFloat(client.principalDueAmount) - parseFloat(client.paidAmount)}
                            style='currency'
                            minimumFractionDigits={0}
                            currency={iCareCurrency}/>

                    </td>
                    <td>
                        <Textbox
                            name='paidAmount'
                            maxLength={10}
                            onChange={this.handleChangePaymentAmount.bind(null, client)}
                            value={valuePaymentAmount}
                            readOnly={this.props.readOnlyBill}
                            className='form-control'
                        />
                    </td>
                    <td>
                        {
                            this.props.readOnlyBill !== true ?
                                <a onClick={this.handleExcludeClient.bind(null, client)}
                                   className='btn btn-outline btn-circle btn-sm green-haze'><FormattedMessage
                                    id="bill_setting.action.exclude"/></a>
                                : <a disabled='disabled'
                                     className='btn btn-outline btn-circle btn-sm green-haze'><FormattedMessage
                                id="bill_setting.action.exclude"/></a>
                        }

                    </td>
                </tr>
            );
        }
        return null;
    }
}
BillingDetailRow.propTypes = {
    handleChangePaymentAmount: PropTypes.func.isRequired,
    client: PropTypes.object.isRequired,
    readOnlyBill: PropTypes.bool
};

export default BillingDetailRow;
