/*global
 iCareLocale
 */
import React, {PropTypes} from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router';
import { FormGroup, FormControl, ControlLabel, HelpBlock, InputGroup, Radio, Button, ButtonGroup } from 'react-bootstrap';
import {FormattedNumber, FormattedMessage, FormattedDate} from 'react-intl';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { Fade, Alert } from 'react-bootstrap';

import 'react-datepicker/dist/react-datepicker.css';

class PaymentForm extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = { isShowMessage: false, value: '', paymentFormTransactionAmount: '', paymentFormTransactionType: '', paymentFormAccountNumber: '', paymentFormBankName: '', paymentFormBranchName: '', paymentFormNote: '', paymentFormAttachment: '', paymentFormAttachmentFile: '', paymentFormStatus: '', paymentFormTransactionDate: moment()};
        this.handleChange = this.handleChange.bind(this);
        this.onDateChange = this.onDateChange.bind(this);
        this.onSubmitPayment = this.onSubmitPayment.bind(this);
        this.onShowMessage = this.onShowMessage.bind(this);
        this.getFilename = this.getFilename.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        const message = nextProps.errors || {};
        if (message.hasOwnProperty('serverException') || message.hasOwnProperty('doctrineException')) {
            this.setState({isShowMessage: true});
        }
        if (nextProps.billing.length > 0) {
            const billing = nextProps.billing[0];
            if (billing.payments.length > 0) {
                const payment = billing.payments[0];
                this.setState({
                    paymentFormAccountNumber: payment.bank_account,
                    paymentFormBankName: payment.bank_name,
                    paymentFormBranchName: payment.branch_name,
                    paymentFormNote: payment.note,
                    paymentFormTransactionDate: (payment.transaction_date) ? moment(`${payment.transaction_date}`, 'YYYY-MM-DD') : null,
                    paymentFormAttachmentFile: payment.attachment,
                    paymentFormStatus: billing.state,
                });
            }
        }
    }
    getFilename(path) {
        return path.split('/').reverse()[0];
    }

    onShowMessage() {
        setTimeout(function () {
            this.setState({ isShowMessage: !this.state.isShowMessage });
        }.bind(this), 6000);
    }

    onDateChange (date) {
        //const data = { 'target':{ 'value':date.format('YYYY-MM-DD'), 'name': this.props.fieldName } };
        this.setState({ paymentFormTransactionDate: date });
    }

    handleChange(e) {
        const formControlData = {}
        formControlData[e.target.name] = e.target.value;
        this.setState(formControlData);
    }
    handleFileChange(e) {

    }
    onSubmitPayment(event){
        event.preventDefault();
        const fileComponent = this._fileInput;
        var file = fileComponent.files[0];
        if(file){
            var reader = new FileReader();
            reader.onload = (upload) => {
                var data = {
                    billingApprovalId: this.props.billingID,
                    paymentFormTransactionDate: this.state.paymentFormTransactionDate ? this.state.paymentFormTransactionDate.format("YYYY-MM-DD"): '',
                    paymentFormAccountNumber: this.state.paymentFormAccountNumber,
                    paymentFormBankName: this.state.paymentFormBankName,
                    paymentFormBranchName: this.state.paymentFormBranchName,
                    paymentFormNote: this.state.paymentFormNote,
                    attachmentFileName: file.name,
                    attachmentFileType: file.type,
                    attachmentFileData: upload.target.result
                };
                this.props.onSubmitPayment(data);
            };
            reader.readAsDataURL(file);
        }else{
            var data = {
                billingApprovalId: this.props.billingID,
                paymentFormTransactionDate: this.state.paymentFormTransactionDate ? this.state.paymentFormTransactionDate.format("YYYY-MM-DD"): '',
                paymentFormAccountNumber: this.state.paymentFormAccountNumber,
                paymentFormBankName: this.state.paymentFormBankName,
                paymentFormBranchName: this.state.paymentFormBranchName,
                paymentFormNote: this.state.paymentFormNote,
                attachmentFileName: "",
                attachmentFileType: "",
                attachmentFileData: ""
            };
            this.props.onSubmitPayment(data);
        }
    }
    render() {
        let billingData = {monthText:"",totalPayment:""};
        let readOnly = false;
        if (this.props.billing.length > 0) {
            const billing = this.props.billing[0];
            if (billing.payments.length > 0) {
                readOnly = true;
            }
            const monthText = <FormattedDate
                value={moment(`${moment(`${billing.start_date}`).format('Y')}-${billing.billing_month}-01`)}
                year='numeric'
                month='long'/>;
            billingData = {monthText: monthText, totalPayment: billing.total_payment};
        }
        return (
            <div className="col-md-12">
                <div className="portlet light ">
                    <div id="record-payment-error">
                        { this.state.isShowMessage ?
                            <Fade in={this.state.isShowMessage} transitionAppear={true} onEntered={this.onShowMessage}>
                                <Alert bsStyle="danger">
                                    <strong>
                                        {this.props.errors && this.props.errors.hasOwnProperty('serverException') ? <FormattedMessage id="bill_payment.server_exception"/> : ''}
                                        {this.props.errors && this.props.errors.hasOwnProperty('doctrineException') ? <FormattedMessage id="bill_payment.doctrine_exception"/> : ''}
                                    </strong>
                                </Alert>
                            </Fade> :
                            <span></span>
                        }
                    </div>
                    <div className="portlet-title">
                        <div className="caption">
                            <i className="fa fa-calculator"></i>
                            <span className="caption-subject font-dark sbold uppercase"><FormattedMessage id="bill_payment.billing_payment"/></span>
                        </div>
                    </div>
                    <div className="portlet-body form">
                        <form className="form-horizontal" role="form" method="post" encType="multipart/form-data" onSubmit={this.onSubmitPayment}>
                            <div className="form-body">
                                <div className="form-group">
                                    <label className="col-md-4 control-label"><FormattedMessage id="bill_payment.bill_cycle"/></label>
                                    <div className="col-md-8">
                                        <label className="control-label">{billingData.monthText}</label>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label className="col-md-4 control-label"><FormattedMessage id="bill_payment.approved_payment_amount"/></label>
                                    <div className="col-md-8">
                                        <label className="control-label">
                                            <FormattedNumber
                                                value={billingData.totalPayment}
                                                style='currency'
                                                minimumFractionDigits={0}
                                                currency={iCareCurrency}/></label>
                                    </div>
                                </div>

                                <FormGroup controlId="paymentType" >
                                    <ControlLabel className="col-md-4"><FormattedMessage id="bill_payment.payment_type"/></ControlLabel>
                                    <div className="col-md-8">
                                        <ButtonGroup>
                                            <Radio checked inline disabled tabIndex="1">
                                                <FormattedMessage id="bill_payment.paid_in_full"/>
                                            </Radio>
                                            <Radio inline disabled tabIndex="2">
                                                <FormattedMessage id="bill_payment.partially_paid"/>
                                            </Radio>
                                        </ButtonGroup>
                                    </div>
                                </FormGroup>
                                <FormGroup controlId="paymentFormTransactionAmount" >
                                    <ControlLabel className="col-md-4"><FormattedMessage id="bill_payment.transaction_amount"/></ControlLabel>
                                    <div className="col-md-8">
                                        <InputGroup>
                                            <InputGroup.Addon><i className="fa fa-usd"></i></InputGroup.Addon>
                                            <FormControl
                                                type="text"
                                                readOnly="true"
                                                tabIndex="3"
                                                //value={this.state.paymentFormTransactionAmount}
                                                value={billingData.totalPayment}
                                                placeholder="Transaction Amount"
                                                onChange={this.handleChange}
                                            />
                                        </InputGroup>
                                        <FormControl.Feedback />
                                        <HelpBlock className="hide">Validation is based on string length.</HelpBlock>
                                    </div>
                                </FormGroup>
                                <FormGroup bsClass={this.props.errors && this.props.errors.hasOwnProperty('paymentInfo') && !(this.state.paymentFormTransactionDate) ? 'form-group has-error' : 'form-group'}>
                                    <ControlLabel className="col-md-4"><FormattedMessage id="bill_payment.transaction_date"/></ControlLabel>
                                    <div className="col-md-8">
                                        <DatePicker
                                            name="paymentFormTransactionDate"
                                            className="form-control"
                                            dateFormat='YYYY-MM-DD'
                                            selected={this.state.paymentFormTransactionDate}
                                            onChange={this.onDateChange}
                                            tabIndex={4}
                                            readOnly = {readOnly}
                                        />
                                        <FormControl.Feedback />
                                        <HelpBlock className={this.props.errors && this.props.errors.hasOwnProperty('paymentInfo') && !(this.state.paymentFormTransactionDate) ? 'block' : 'hide'}><FormattedMessage id="bill_payment.transaction_date_blank"/></HelpBlock>
                                    </div>
                                </FormGroup>
                                <FormGroup controlId="paymentFormTransactionType" >
                                    <ControlLabel className="col-md-4"><FormattedMessage id="bill_payment.transaction_type"/></ControlLabel>
                                    <div className="col-md-8">
                                        <FormControl
                                            readOnly="true"
                                            tabIndex="4"
                                            componentClass="select"
                                            value={this.state.paymentFormTransactionType} placeholder="please select" onChange={this.handleChange}>
                                            <option value="select">Bank Transfer</option>
                                        </FormControl>
                                        <FormControl.Feedback />
                                        <HelpBlock className="hide">Validation is based on string length.</HelpBlock>
                                    </div>
                                </FormGroup>
                                <FormGroup bsClass={this.props.errors && this.props.errors.hasOwnProperty('paymentInfo') && this.state.paymentFormAccountNumber == "" ? 'form-group has-error' : 'form-group'}>
                                    <ControlLabel className="col-md-4"><FormattedMessage id="bill_payment.account_number"/></ControlLabel>
                                    <div className="col-md-8">
                                        <FormControl
                                            name="paymentFormAccountNumber"
                                            type="text"
                                            tabIndex="5"
                                            value={this.state.paymentFormAccountNumber}
                                            placeholder="Enter text"
                                            onChange={this.handleChange}
                                            readOnly = {readOnly}
                                        />
                                        <FormControl.Feedback />
                                        <HelpBlock className={this.props.errors && this.props.errors.hasOwnProperty('paymentInfo') && this.state.paymentFormAccountNumber == "" ? 'block' : 'hide'}><FormattedMessage id="bill_payment.account_number_blank"/></HelpBlock>
                                    </div>
                                </FormGroup>
                                <FormGroup bsClass={this.props.errors && this.props.errors.hasOwnProperty('paymentInfo') && this.state.paymentFormBankName == "" ? 'form-group has-error' : 'form-group'}>
                                    <ControlLabel className="col-md-4"><FormattedMessage id="bill_payment.bank_name"/></ControlLabel>
                                    <div className="col-md-8">
                                        <FormControl
                                            name="paymentFormBankName"
                                            type="text"
                                            tabIndex="6"
                                            value={this.state.paymentFormBankName}
                                            placeholder="Enter text"
                                            onChange={this.handleChange}
                                            readOnly = {readOnly}
                                        />
                                        <FormControl.Feedback />
                                        <HelpBlock className={this.props.errors && this.props.errors.hasOwnProperty('paymentInfo') && this.state.paymentFormBankName == "" ? 'block' : 'hide'}><FormattedMessage id="bill_payment.bank_name_blank"/></HelpBlock>
                                    </div>
                                </FormGroup>
                                <FormGroup bsClass={this.props.errors && this.props.errors.hasOwnProperty('paymentInfo') && this.state.paymentFormBranchName == "" ? 'form-group has-error' : 'form-group'}>
                                    <ControlLabel className="col-md-4"><FormattedMessage id="bill_payment.branch_name"/></ControlLabel>
                                    <div className="col-md-8">
                                        <FormControl
                                            name="paymentFormBranchName"
                                            type="text"
                                            tabIndex="7"
                                            value={this.state.paymentFormBranchName}
                                            placeholder="Enter text"
                                            onChange={this.handleChange}
                                            readOnly = {readOnly}
                                        />
                                        <FormControl.Feedback />
                                        <HelpBlock className={this.props.errors && this.props.errors.hasOwnProperty('paymentInfo') && this.state.paymentFormBranchName == "" ? 'block' : 'hide'}><FormattedMessage id="bill_payment.branch_name_blank"/></HelpBlock>
                                    </div>
                                </FormGroup>
                                <FormGroup >
                                    <ControlLabel className="col-md-4"><FormattedMessage id="bill_payment.note"/></ControlLabel>
                                    <div className="col-md-8">
                                        <FormControl
                                            name="paymentFormNote"
                                            componentClass="textarea"
                                            type="text"
                                            tabIndex="9"
                                            value={this.state.paymentFormNote}
                                            placeholder="Enter text"
                                            onChange={this.handleChange}
                                            readOnly = {readOnly}
                                        />
                                        <FormControl.Feedback />
                                        <HelpBlock className="hide">Validation is based on string length.</HelpBlock>
                                    </div>
                                </FormGroup>
                                <FormGroup bsClass={this.props.errors && this.props.errors.hasOwnProperty('fileTypeAttachment') ? 'form-group has-error' : 'form-group'}>
                                    <ControlLabel className="col-md-4"><FormattedMessage id="bill_payment.attachment"/></ControlLabel>
                                    <div className="col-md-8">
                                        {readOnly && this.state.paymentFormAttachmentFile ?
                                            <p className="form-control-static"><i className="fa fa-paperclip"
                                                                                  aria-hidden="true"/> <a
                                                href={this.state.paymentFormAttachmentFile}
                                                target="_blank">{this.getFilename(this.state.paymentFormAttachmentFile)}</a></p>
                                            :
                                            <FormControl
                                                name="paymentFormAttachment"
                                                type="file"
                                                tabIndex="10"
                                                value={this.state.paymentFormAttachment}
                                                placeholder="Enter text"
                                                onChange={this.handleChange}
                                                ref={(ref) => this._fileInput = ReactDOM.findDOMNode(ref)}
                                            />
                                        }
                                        <FormControl.Feedback />
                                        <HelpBlock className={this.props.errors && this.props.errors.hasOwnProperty('fileTypeAttachment') ? 'block' : 'hide'}><FormattedMessage id="bill_payment.attachment_invalid"/></HelpBlock>
                                    </div>
                                </FormGroup>
                                {
                                    (this.state.paymentFormStatus)?
                                        <FormGroup >
                                            <ControlLabel className="col-md-4"><FormattedMessage id="bill_payment.status"/></ControlLabel>
                                            <div className="col-md-8">
                                                <label className="control-label text-capitalize"><FormattedMessage id={`admin_overview.status.${this.state.paymentFormStatus}`}/></label>
                                            </div>
                                        </FormGroup>
                                        :
                                        null
                                }

                            </div>
                            <div className={readOnly ? 'hide form-actions' : 'block form-actions'}>
                                <div className="row">
                                    <div className="col-md-offset-3 col-md-8">

                                        <button type="submit" tabIndex="11" className="btn green"><FormattedMessage id="bill_payment.submit"/></button>
                                        <button type="button" tabIndex="12" className="btn default"><FormattedMessage id="bill_payment.cancel"/></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

PaymentForm.propTypes = {
    uploadResponse: PropTypes.object,
    billing: PropTypes.array,
    data: PropTypes.object,
    billingID: PropTypes.string,
    onSubmitPayment: PropTypes.func,
    saving: PropTypes.bool,
    errors: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ])
};

export default PaymentForm;