import React, {PropTypes} from 'react';
import {FormattedMessage} from 'react-intl';
import BillingExcludeRow from './BillingExcludeRow';

class BillingExclude extends React.Component {
    render() {
        return (
            <div className='portlet'>
                <div className='portlet-title'>
                    <div className='caption'>
                        <i className='icon-social-dribbble font-green'/>
                        <span className='caption-subject font-green bold uppercase'>
                          <FormattedMessage id="bill_setting.employees_excluded_from_bill"/>
                        </span>
                    </div>
                </div>
                <div className='portlet-body'>
                    <div className='fixed-table-header'>
                        <table className='table table-hover'>
                            <thead>
                            <tr>
                                <th width='3%'>
                                    <div className='th-inner '><FormattedMessage id='bill_setting.detail.id'/></div>
                                    <div className='fht-cell'/>
                                </th>
                                <th width='15%'>
                                    <div className='th-inner '><FormattedMessage id='bill_setting.detail.name'/></div>
                                    <div className='fht-cell'/>
                                </th>
                                <th width='15%'>
                                    <div className='th-inner '><FormattedMessage
                                        id='bill_setting.exclude.exclusion_reason'/></div>
                                    <div className='fht-cell'/>
                                </th>
                                <th width='10%'>
                                    <div className='th-inner '><FormattedMessage
                                        id='bill_setting.exclude.termination_date'/></div>
                                    <div className='fht-cell'/>
                                </th>
                                <th width='10%'>
                                    <div className='th-inner '><FormattedMessage id='bill_setting.exclude.leave_start'/>
                                    </div>
                                    <div className='fht-cell'/>
                                </th>
                                <th width='10%'>
                                    <div className='th-inner '><FormattedMessage id='bill_setting.exclude.leave_end'/>
                                    </div>
                                    <div className='fht-cell'/>
                                </th>
                                <th width='15%'>
                                    <div className='th-inner '><FormattedMessage id='bill_setting.action'/></div>
                                    <div className='fht-cell'/>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            {Object.keys(this.props.exclude).map((iCareMemberId, index) =>
                                <BillingExcludeRow
                                    key={index}
                                    readOnlyBill={this.props.readOnlyBill}
                                    listClients={this.props.listClients}
                                    data={this.props.exclude[iCareMemberId]}
                                    updateRejectClient={this.props.updateRejectClient}
                                />
                            )}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}
BillingExclude.propTypes = {
    listClients: PropTypes.array.isRequired,
    exclude: PropTypes.object,
    updateRejectClient: PropTypes.func,
    readOnlyBill: PropTypes.bool
};

export default BillingExclude;
