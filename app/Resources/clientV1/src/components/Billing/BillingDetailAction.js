import React from 'react';
import {FormattedMessage} from 'react-intl';
import * as authHelper from '../../utils/AuthHelper';

class BillingDetailAction extends React.Component {
    render() {
        const billing = this.props.billing;
        let button = null;
        switch (billing.state) {
            case "billed":
            case "re_opened":
                button = <div>
                    {(authHelper.isVisibleForAdmin()) ?
                        <button onClick={this.props.handleVoidBill} className='btn btn-circle btn-danger'>
                            <i className='fa fa-close'/> <FormattedMessage id="bill_setting.action.void"/>
                        </button> : null}
                    &nbsp;
                    <button onClick={this.props.handleApproveBill} className='btn btn-circle btn-success'>
                        <i className='fa fa-check-square-o'/> <FormattedMessage id="bill_setting.action.approve"/>
                    </button>
                </div>
                break;
            case "approved":
                button = (authHelper.isVisibleForAdmin()) ? <div>
                    <button onClick={this.props.handleVoidBill} className='btn btn-circle btn-danger'>
                        <i className='fa fa-close'/> <FormattedMessage id="bill_setting.action.void"/>
                    </button>
                    &nbsp;
                    <button onClick={this.props.handleReopenBill} className='btn btn-circle  btn-warning'>
                        <i className='fa fa-reply'/> <FormattedMessage id="bill_setting.action.reopen"/>
                    </button>
                </div> : null;
                break;
        }

        return (
            <div className='actions'>
                <div className='btn-group btn-group-devided' data-toggle='buttons'>
                    {button}
                </div>
            </div>
        );
    }
}
BillingDetailAction.propTypes = {
    billing: React.PropTypes.object.isRequired,
    handleApproveBill: React.PropTypes.func.isRequired,
    handleReopenBill: React.PropTypes.func.isRequired,
    handleVoidBill: React.PropTypes.func.isRequired,
}
export default BillingDetailAction;
