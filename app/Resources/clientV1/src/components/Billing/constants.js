export default {
    reasonExclusion: [
        {
            'value': 1,
            'text': 'bill_setting.exclude.reason.resigned'
        },
        {
            'value': 2,
            'text': 'bill_setting.exclude.reason.maternity_leave'
        },
        {
            'value': 3,
            'text': 'bill_setting.exclude.reason.did_not_purchase'
        }
    ],

    billingStatus: [
        {
            'value': 'approved',
            'text': 'customer_overview.latest_billing_list.status.approved',
        },
        {
            'value': 'billed',
            'text': 'customer_overview.latest_billing_list.status.billed',
        },
        {
            'value': 'closed',
            'text': 'customer_overview.latest_billing_list.status.closed',
        },
        {
            'value': 'paid',
            'text': 'customer_overview.latest_billing_list.status.paid',
        },
        {
            'value': 'partial_received',
            'text': 'customer_overview.latest_billing_list.status.partial_received',
        },
        {
            'value': 'received',
            'text': 'customer_overview.latest_billing_list.status.received',
        },
        {
            'value': 're_opened',
            'text': 'customer_overview.latest_billing_list.status.re_opened',
        },
        {
            'value': 'voided',
            'text': 'customer_overview.latest_billing_list.status.voided',
        }
    ]
};
