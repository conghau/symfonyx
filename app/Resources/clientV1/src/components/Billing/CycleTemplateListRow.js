import React, {PropTypes} from 'react';
import {Link} from 'react-router';
import {FormattedDate} from 'react-intl';
import DatePicker from 'react-datepicker';
import moment from 'moment';

class CycleTemplateListRow extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {viewMode: '', editMode: 'hide', endDateData: this.props.data.end_date};

        this.onDateChange = this.onDateChange.bind(this);
        this.onSaveCycleTemplate = this.onSaveCycleTemplate.bind(this);
        this.onCancelCycleTemplate = this.onCancelCycleTemplate.bind(this);
        this.onUpdateCycleTemplate = this.onUpdateCycleTemplate.bind(this);
    }

    onSaveCycleTemplate(event) {
        event.preventDefault();
        this.setState({viewMode: 'hide'});
        this.setState({editMode: ''});
        // this.props.actions.resetSelectMapping(event);
    }

    onCancelCycleTemplate(event) {
        event.preventDefault();
        const data = {'indexKey': this.props.data.id};
        this.props.onCancel(event, data);
        this.setState({viewMode: '', editMode: 'hide', endDateData: this.props.data.end_date});
        event.stopPropagation();
    }

    onUpdateCycleTemplate(event) {
        event.preventDefault();
        const data = {'value': this.state.endDateData, 'indexKey': this.props.data.id};
        this.props.onUpdate(event, data);
        this.setState({viewMode: ''});
        this.setState({editMode: 'hide'});
        event.stopPropagation();
    }

    onDateChange(date) {
        const data = {'target': {'value': date.format('YYYY-MM-DD'), 'indexKey': this.props.index}};
        this.props.onChange(data);
        this.setState({endDateData: date.format('YYYY-MM-DD')});
    }

    render() {
        const data = this.props.data;
        const index = this.props.index;
        const editRoute = '/employee/import/mapping/resolve/';

        const monthText = <FormattedDate
            value={moment(`2016-${data.month > 9 ? data.month : '0' + data.month}-01`)}
            month='long'/>;

        const startDate = (data.start_date) ? moment(`${data.start_date}`, 'YYYY-MM-DD') : null;
        const endDate = (this.state.endDateData) ? moment(`${this.state.endDateData}`, 'YYYY-MM-DD') : null;
        const errorMessage = (this.props.error[data.id]) ?
            <span className='help-block text-danger'><FormattedMessage id={this.props.error[data.id]}/></span> : null;
        const currentEditMode = (this.props.error[data.id]) ? '' : this.state.editMode;
        const currentViewMode = (this.props.error[data.id]) ? 'hide' : this.state.viewMode;
        const editLink = (data.exception == 1) ?
            <div>
        <span className={currentViewMode}>
          <a onClick={this.onSaveCycleTemplate}>
            <i className='fa fa-edit'/> Edit </a>
        </span>
                <span className={currentEditMode}>
          <a onClick={this.onCancelCycleTemplate}>
            <i className='fa fa-close'/> Cancel </a>&nbsp;
                    <a onClick={this.onUpdateCycleTemplate}>
            <i className='fa fa-save'/> Save </a>
        </span>
            </div>
            :
            null;
        return (
            <tr onClick={this.onSaveCycleTemplate}>
                <td>{index + 1}</td>
                <td>{monthText}</td>
                <td>
                    {startDate.format('MMM DD YYYY')}
                </td>
                <td>
                    {data.exception == 1 ?
                        <div>
                            <DatePicker
                                dateFormat='MMM DD YYYY'
                                selected={endDate}
                                onChange={this.onDateChange}
                                className={currentEditMode}
                            />
                            <span className={currentViewMode}>{endDate.format('MMM DD YYYY')}</span>
                            {errorMessage}
                        </div>
                        :
                        <span>{endDate.format('MMM DD YYYY')}</span>
                    }
                </td>
                <td>{editLink}</td>
            </tr>
        );
    }
}
CycleTemplateListRow.propTypes = {
    data: PropTypes.object,
    error: PropTypes.object,
    onChange: PropTypes.func,
    onCancel: PropTypes.func,
    onUpdate: PropTypes.func
};

export default CycleTemplateListRow;
