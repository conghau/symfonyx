import React, {PropTypes} from 'react';
import constants from './constants';
import {FormattedMessage} from 'react-intl';

class BillingExcludeRow extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.handleUpdateRejectClient = this.handleUpdateRejectClient.bind(this);
    }

    handleUpdateRejectClient(data, client) {
        var dataClient = Object.assign({}, client, {iCareMemberId: data['iCareMemberId']});
        this.props.updateRejectClient(data, dataClient);
    }

    render() {
        const data = this.props.data;
        var clientDetail = this.props.listClients.find(function (c) {
            return c.clientExternalId == data.customerId;
        });

        if (data) {
            const reason = constants.reasonExclusion.find(function (r) {
                return r.value == data.reasonType;
            });
            return (
                <tr>
                    <td>{data.iCareMemberId}</td>
                    <td>{(clientDetail) ? clientDetail.clientName : ''}</td>
                    <td><FormattedMessage id={reason.text}/></td>
                    <td>{(data.reasonType == 1) ? data.terminalDate : ''}</td>
                    <td>{(data.reasonType == 2) ? data.leaveStartDate : ''}</td>
                    <td>{(data.reasonType == 2) ? data.leaveEndDate : ''}</td>
                    <td>
                        {this.props.readOnlyBill !== true ?
                            <a onClick={this.handleUpdateRejectClient.bind(null, data, clientDetail)}
                               className='btn btn-outline btn-circle btn-sm green-haze'><FormattedMessage
                                id="bill_setting.action.edit"/></a>
                            : <a disabled='disabled'
                                 className='btn btn-outline btn-circle btn-sm green-haze'><FormattedMessage
                            id="bill_setting.action.edit"/></a>
                        }
                    </td>
                </tr>
            );
        }
        else {
            return (<tr />);
        }
    }
}
BillingExcludeRow.propTypes = {
    data: PropTypes.object.isRequired,
    readOnlyBill: PropTypes.bool
};

export default BillingExcludeRow;
