import React, {PropTypes} from 'react';
import {Form, FormGroup, FormControl, ControlLabel, Radio, Col} from 'react-bootstrap';
import {FormattedNumber, FormattedMessage, FormattedDate} from 'react-intl';
import moment from 'moment';

class ConfirmPaymentForm extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            confirmNote: ''
        };
        this.handleCancelChange = this.handleCancelChange.bind(this);
        this.handleSubmitForm = this.handleSubmitForm.bind(this);
        this.handleNoteChange = this.handleNoteChange.bind(this);
        this.handleGoBack = this.handleGoBack.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        const newState = {
            confirmNote: nextProps.billPaymentInfo.confirmNote || ''
        };
        this.setState(newState);
    }

    handleNoteChange(e) {
        const newState = {
            confirmNote: e.target.value
        };
        this.setState(newState);
    }

    handleCancelChange() {
        const newState = {
            confirmNote: this.props.billPaymentInfo.confirmNote || ''
        };
        this.setState(newState);
    }
    handleGoBack() {
        this.props.onGoBack();
    }
    getFilename(path) {
        return path.split('/').reverse()[0];
    }

    handleSubmitForm(event) {
        event.preventDefault();
        this.props.onConfirm(this.state);
    }

    render() {
        let billPaymentInfo = this.props.billPaymentInfo;
        let isViewOnly = billPaymentInfo.billingState != 'paid';
        return (
            <div className='portlet box green'>
                <div className="portlet-title">
                    <div className="caption"><FormattedMessage id='confirm_payment.form_caption'/></div>
                </div>
                <div className='portlet-body form'>
                    <Form horizontal role="form" onSubmit={this.handleSubmitForm}>
                        <div className="form-body">
                            <FormGroup>
                                <Col componentClass={ControlLabel} md={4}><FormattedMessage
                                    id='confirm_payment.billing_cycle'/></Col>
                                <Col md={8}>
                                    <FormControl.Static><FormattedDate
                                        value={moment(`${billPaymentInfo.billingYear}-${billPaymentInfo.billingMonth}-01`)}
                                        year='numeric'
                                        month='long'/></FormControl.Static>
                                </Col>
                            </FormGroup>
                            <FormGroup>
                                <Col componentClass={ControlLabel} md={4}><FormattedMessage
                                    id='confirm_payment.approved_payment_amount'/></Col>
                                <Col md={8}>
                                    <FormControl.Static><FormattedNumber
                                        value={billPaymentInfo.totalPayment}
                                        style='currency'
                                        minimumFractionDigits={0}
                                        currency={iCareCurrency}/></FormControl.Static>
                                </Col>
                            </FormGroup>
                            <FormGroup>
                                <Col componentClass={ControlLabel} md={4}><FormattedMessage
                                    id='confirm_payment.payment_type'/></Col>
                                <Col md={8}>
                                    <label className="radio-inline">
                                        { billPaymentInfo.transactionType == 1 ?
                                            <input type="radio" checked readOnly/> :
                                            <input type="radio" disabled/> }<FormattedMessage
                                        id='confirm_payment.paid_in_full'/>
                                    </label>
                                    &nbsp;&nbsp;&nbsp;
                                    <label className="radio-inline">
                                        { billPaymentInfo.transactionType == 3 ?
                                            <input type="radio" checked readOnly/> :
                                            <input type="radio" disabled/> }<FormattedMessage
                                        id='confirm_payment.partially_paid'/>
                                    </label>
                                </Col>
                            </FormGroup>
                            <FormGroup>
                                <Col componentClass={ControlLabel} md={4}><FormattedMessage
                                    id='confirm_payment.transaction_amount'/></Col>
                                <Col md={8}>
                                    <FormControl.Static><FormattedNumber
                                        value={billPaymentInfo.transactionAmount}
                                        style='currency'
                                        minimumFractionDigits={0}
                                        currency={iCareCurrency}/></FormControl.Static>
                                </Col>
                            </FormGroup>
                            <FormGroup>
                                <Col componentClass={ControlLabel} md={4}><FormattedMessage
                                    id='confirm_payment.transaction_date'/></Col>
                                <Col md={8}>
                                    <FormControl.Static><FormattedDate value={moment(billPaymentInfo.transactionDate)}
                                                                       year='numeric'
                                                                       month='long'
                                                                       day='2-digit'/></FormControl.Static>
                                </Col>
                            </FormGroup>
                            <FormGroup>
                                <Col componentClass={ControlLabel} md={4}><FormattedMessage
                                    id='confirm_payment.transaction_type'/></Col>
                                <Col md={8}>
                                    <FormControl.Static>{ billPaymentInfo.transactionType == 1 ?
                                        <FormattedMessage id='confirm_payment.bank_transfer'/> :
                                        <span/> }</FormControl.Static>
                                </Col>
                            </FormGroup>
                            <FormGroup>
                                <Col componentClass={ControlLabel} md={4}><FormattedMessage
                                    id='confirm_payment.account_number'/></Col>
                                <Col md={8}>
                                    <FormControl.Static>{billPaymentInfo.bankAccount}</FormControl.Static>
                                </Col>
                            </FormGroup>
                            <FormGroup>
                                <Col componentClass={ControlLabel} md={4}><FormattedMessage
                                    id='confirm_payment.bank_name'/></Col>
                                <Col md={8}>
                                    <FormControl.Static>{billPaymentInfo.bankName}</FormControl.Static>
                                </Col>
                            </FormGroup>
                            <FormGroup>
                                <Col componentClass={ControlLabel} md={4}><FormattedMessage
                                    id='confirm_payment.branch_name'/></Col>
                                <Col md={8}>
                                    <FormControl.Static>{billPaymentInfo.branchName}</FormControl.Static>
                                </Col>
                            </FormGroup>
                            <FormGroup>
                                <Col componentClass={ControlLabel} md={4}><FormattedMessage
                                    id='confirm_payment.confirm_note'/></Col>
                                <Col md={8}>
                                    <FormControl
                                        componentClass="textarea"
                                        type="text"
                                        value={this.state.confirmNote}
                                        onChange={this.handleNoteChange}
                                        disabled={isViewOnly}
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup>
                                <Col componentClass={ControlLabel} md={4}><FormattedMessage
                                    id='confirm_payment.attachment'/></Col>
                                <Col md={8}>
                                    { billPaymentInfo.attachment ?
                                        <p className="form-control-static"><i className="fa fa-paperclip"
                                                                              aria-hidden="true"/> <a
                                            href={billPaymentInfo.attachment}
                                            target="_blank">{this.getFilename(billPaymentInfo.attachment)}</a></p>
                                        :
                                        <span/>
                                    }

                                </Col>
                            </FormGroup>
                        </div>
                        <div className="form-actions">
                            <div className="row">
                                <div className="col-md-offset-4 col-md-8">
                                    { isViewOnly ?
                                        <div>
                                            <button type="button" className="btn green" onClick={this.handleGoBack}>
                                                <FormattedMessage
                                                    id='confirm_payment.go_back'/></button>
                                        </div>
                                        :
                                        <div>
                                            <button type="button" className="btn default"
                                                    onClick={this.handleCancelChange}>
                                                <FormattedMessage id='confirm_payment.cancel'/>
                                            </button>
                                            <button type="submit" className="btn green"><FormattedMessage
                                                id='confirm_payment.confirm'/></button>
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </Form>
                </div>
            </div>
        );
    }
}
ConfirmPaymentForm.propTypes = {
    billPaymentInfo: PropTypes.object.isRequired,
    onConfirm: PropTypes.func.isRequired,
    onGoBack: PropTypes.func.isRequired
};
export default ConfirmPaymentForm;
