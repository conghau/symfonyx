import React from 'react';
import {FormattedMessage} from 'react-intl';
import BillingDetailRow from './BillingDetailRow';

class BillingDetail extends React.Component {
  render () {
    return (
      <div className='portlet'>
        <div className='portlet-title'>
          <div className='caption'>
            <i className='icon-social-dribbble font-green' />
            <span className='caption-subject font-green bold uppercase'>
              <FormattedMessage id="bill_setting.bill_detail"/>
            </span>
          </div>
        </div>
        <div className='portlet-body'>
          <div className='fixed-table-header'>
            <table className='table table-hover'>
              <thead>
                <tr>
                  <th width='3%'>
                    <div className='th-inner '><FormattedMessage id='bill_setting.detail.id' /></div>
                    <div className='fht-cell' />
                  </th>
                  <th width='15%'>
                    <div className='th-inner '><FormattedMessage id='bill_setting.detail.name' /></div>
                    <div className='fht-cell' />
                  </th>
                  <th width='15%'>
                    <div className='th-inner '><FormattedMessage id='bill_setting.detail.previous_outstanding' /></div>
                    <div className='fht-cell' />
                  </th>
                  <th width='10%'>
                    <div className='th-inner '><FormattedMessage id='bill_setting.detail.principal_due' /></div>
                    <div className='fht-cell' />
                  </th>
                  <th width='10%'>
                    <div className='th-inner '><FormattedMessage id='bill_setting.detail.paid' /></div>
                    <div className='fht-cell' />
                  </th>
                  <th width='10%'>
                    <div className='th-inner '><FormattedMessage id='bill_setting.detail.outstanding' /></div>
                    <div className='fht-cell' />
                  </th>
                  <th width='12%'>
                    <div className='th-inner '><FormattedMessage id='bill_setting.detail.payment_amount' /></div>
                    <div className='fht-cell' />
                  </th>
                  <th width='15%'>
                    <div className='th-inner '><FormattedMessage id='bill_setting.action' /></div>
                    <div className='fht-cell' />
                  </th>
                </tr>
              </thead>
              <tbody>
                {this.props.detail.map((client, index) =>
                  <BillingDetailRow
                    key={index}
                    client={client}
                    customers={this.props.customers}
                    exclude={this.props.exclude}
                    accept={this.props.accept}
                    handleChangePaymentAmount={this.props.handleChangePaymentAmount}
                    handleExcludeClient={this.props.handleExcludeClient}
                    readOnlyBill={this.props.readOnlyBill}
                                />
                            )}
              </tbody>
            </table>
          </div>
        </div>
      </div>

        );
  }
}

export default BillingDetail;
