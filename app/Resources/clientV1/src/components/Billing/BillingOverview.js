import React, { PropTypes } from 'react';
import { FormattedNumber,FormattedMessage } from 'react-intl';
import moment from 'moment';

class BillingOverview extends React.Component {
  render () {
    return (
      <div className='row'>
        <div className='col-lg-4 col-md-4 col-sm-6 col-xs-12 '>
          <div className='dashboard-stat2 bordered border-default'>
            <div className='display'>
              <div className='number'>
                <h4><FormattedMessage id="bill_setting.oveview.total_bill"/></h4>
                <h3 className='font-blue-sharp'>
                  <span data-counter='counterup'>
                    <FormattedNumber
                        value={Object.keys(this.props.billingApproval).length > 0 ? this.props.billingApproval.total_bill : 0}
                        style='currency'
                        minimumFractionDigits={0}
                        currency={iCareCurrency} />
                  </span>
                </h3>
              </div>
            </div>
          </div>
        </div>
        <div className='col-lg-4 col-md-4 col-sm-6 col-xs-12'>
          <div className='dashboard-stat2 bordered border-default'>
            <div className='display'>
              <div className='number'>
                <h4>
                  <FormattedMessage id="bill_setting.oveview.total_payment"/>
                </h4>
                <h3 className='font-blue-sharp'>
                  <span data-counter='counterup'
                    data-value={this.props.totalPayment}>
                    <FormattedNumber
                        value={this.props.totalPayment}
                        style='currency'
                        minimumFractionDigits={0}
                        currency={iCareCurrency} />
                  </span>
                </h3>
              </div>
            </div>
          </div>
        </div>
        <div className='col-lg-4 col-md-4 col-sm-6 col-xs-12'>
          <div className='dashboard-stat2 bordered border-default'>
            <div className='display'>
              <div className='number'>
                <h4>
                  <FormattedMessage id="bill_setting.oveview.due_date"/>
                </h4>
                <h3 className='font-blue-sharp'>
                  <span data-counter='counterup'
                    data-value={this.props.billingApproval}>
                    {Object.keys(this.props.billingApproval).length > 0 ? moment(this.props.billingApproval.due_date).format('MMM Do YY') : 0}
                  </span>
                </h3>
              </div>
            </div>
          </div>
        </div>
      </div>
        );
  }
}

BillingOverview.propsType = {
  billingApproval: PropTypes.object.isRequired,
  totalPayment: PropTypes.number.isRequired
};

export default BillingOverview;
