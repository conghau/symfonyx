import React, { PropTypes } from 'react';
import CycleTemplateListRow from './CycleTemplateListRow';

class CycleTemplateList extends React.Component {
  constructor (props, context) {
    super(props, context);
    const cloneCycleTemplateData = Object.assign({}, this.props.cycleTemplate.data);
    this.state = { currentCycleTemplateData: cloneCycleTemplateData };
    this.handleTextChange = this.handleTextChange.bind(this);
    this.onUpdate = this.onUpdate.bind(this);
    this.onCancel = this.onCancel.bind(this);
  }
  handleTextChange (event) {
    this.state.currentCycleTemplateData[event.target.indexKey] = event.target.value;
  }

  onUpdate (event, data) {
    event.preventDefault();
    this.props.onSave(data);
  }

  onCancel (event, data) {
    event.preventDefault();
    this.props.onCancel(data);
  }

  render () {
    if (this.props.cycleTemplate.data && this.props.cycleTemplate.data.length > 0) {
      return (
        <div className='portlet'>
          <div className='portlet-title'>
            <div className='caption'>
              <i className='fa fa-calendar' /> Billing Cycle Template
            </div>
          </div>
          <div className='portlet-body'>
            <div className='fixed-table-header'>
              <table className='table table-hover'>
                <thead>
                  <tr>
                    <th width='5%'>
                        <div className='th-inner '>No</div>
                        <div className='fht-cell' />
                      </th>
                    <th width='15%'>
                        <div className='th-inner '>Cycle Month</div>
                        <div className='fht-cell' />
                      </th>
                    <th width='20%'>
                        <div className='th-inner '>Start Date</div>
                        <div className='fht-cell' />
                      </th>
                    <th width='20%'>
                        <div className='th-inner '>End Date</div>
                        <div className='fht-cell' />
                      </th>
                    <th width='15%'>
                        <div className='th-inner '>Action</div>
                        <div className='fht-cell' />
                      </th>
                  </tr>
                </thead>
                <tbody>

                  { this.props.cycleTemplate.data.map((templateData, index) =>
                    <CycleTemplateListRow
                        key={index}
                        index={index}
                        data={templateData}
                        error={this.props.cycleTemplate.errors}
                        onChange={this.handleTextChange}
                        onCancel={this.onCancel}
                        onUpdate={this.onUpdate} />
                                )}
                </tbody>
              </table>
            </div>
          </div>
        </div>

            );
    } else {
      return false;
    }
  }
}
CycleTemplateList.propTypes = {
  cycleTemplate: PropTypes.object,
  errors: PropTypes.object,
  onSave: PropTypes.func,
  onCancel: PropTypes.func,
  saving: PropTypes.bool
};
export default CycleTemplateList;
