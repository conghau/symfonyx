import React, {PropTypes} from 'react';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import constants from './constants';
import {FormattedMessage} from 'react-intl';

class BillingExcludeForm extends React.Component {
    constructor(props, context) {
        super(props, context);
        const excludeData = (props.exclude[props.client['clientExternalId']]) ? props.exclude[props.client['clientExternalId']] : {};
        let formData = Object.assign(excludeData, {
            terminalDate: moment().format('YYYY-MM-DD'),
            leaveStartDate: moment().format('YYYY-MM-DD'),
            leaveEndDate: moment().format('YYYY-MM-DD'),
            reasonType: 1,
            isExcluded: 0
        });

        this.state = {
            showModal: false,
            client: {},
            data: formData
        };
        this.cancelRejectClient = this.cancelRejectClient.bind(this);
        this.handleExcludeBill = this.handleExcludeBill.bind(this);
        this.handleTerminalDateChange = this.handleTerminalDateChange.bind(this);
        this.handleReasonChange = this.handleReasonChange.bind(this);
        this.handleLeaveStartDateChange = this.handleLeaveStartDateChange.bind(this);
        this.handleLeaveEndDateChange = this.handleLeaveEndDateChange.bind(this);
        this.handleSaveRejectClient = this.handleSaveRejectClient.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.hasOwnProperty('showModal')) {
            this.setState({showModal: nextProps.showModal});
        }
        const excludeData = (nextProps.exclude[nextProps.client['clientExternalId']]) ? nextProps.exclude[nextProps.client['clientExternalId']] : {};
        let formData = Object.assign({
            terminalDate: moment().format('YYYY-MM-DD'),
            leaveStartDate: moment().format('YYYY-MM-DD'),
            leaveEndDate: moment().format('YYYY-MM-DD'),
            reasonType: 1,
            isExcluded: 0
        }, excludeData);
        this.setState({data: formData});
    }

    handleExcludeBill(event) {
        const isExcluded = event.target.checked ? 1 : 0;
        const newData = Object.assign({}, this.state.data, {isExcluded: isExcluded});
        this.setState({data: newData});
    }

    handleTerminalDateChange(data) {
        const newData = Object.assign({}, this.state.data, {terminalDate: data.format('YYYY-MM-DD')});
        this.setState({data: newData});
    }

    handleLeaveStartDateChange(data) {
        const newData = Object.assign({}, this.state.data, {leaveStartDate: data.format('YYYY-MM-DD')});
        this.setState({data: newData});
    }

    handleLeaveEndDateChange(data) {
        const newData = Object.assign({}, this.state.data, {leaveEndDate: data.format('YYYY-MM-DD')});
        this.setState({data: newData});
    }

    handleReasonChange(event) {
        const reasonType = event.target.value;
        let newData = Object.assign({}, this.state.data,
            {
                reasonType: reasonType
            });
        this.setState({data: newData});
    }

    handleSaveRejectClient() {
        this.props.saveRejectClient(this.state.data, this.props.client);
    }

    cancelRejectClient() {
        this.props.cancelRejectClient();
    }

    render() {
        const showReasonExcludeFormGroup = `form-group ${this.state.data.isExcluded == 1 ? "" : "hide"}`;
        return (
            <div className='row'>
                <div className='col-lg-8 col-md-8 col-sm-12 col-xs-12 '>
                    <Modal show={this.state.showModal} onHide={this.close}>
                        <Modal.Header>
                            {this.props.client.clientName}
                        </Modal.Header>
                        <Modal.Body>
                            <div className='form-body'>
                                <div className='form-group m-b-n-sm'>
                                    <div className='mt-checkbox-list'>
                                        <label className='mt-checkbox mt-checkbox-outline'>
                                            <input
                                                defaultValue={this.state.data.isExcluded}
                                                defaultChecked={this.state.data.isExcluded == 1 ? true : false}
                                                onChange={this.handleExcludeBill}
                                                type='checkbox'
                                            />
                                            <FormattedMessage tagName="div" id="bill_setting.exclude.exclude_from_bill">
                                            </FormattedMessage>
                                            <span />
                                        </label>
                                    </div>
                                </div>
                                <div className={showReasonExcludeFormGroup}>
                                    <label><FormattedMessage id="bill_setting.exclude.exclusion_reason"/></label>
                                    <select
                                        defaultValue={this.state.data.reasonType}
                                        onChange={this.handleReasonChange}
                                        className='form-control'
                                    >
                                        {constants.reasonExclusion.map(reason =>
                                            <FormattedMessage key={reason.value} id={reason.text}>
                                                {(message) => <option key={reason.value}
                                                                      value={reason.value}>{message}</option>}
                                            </FormattedMessage>
                                        )}
                                    </select>
                                </div>
                                {this.state.data.reasonType == 1 ? (<div className={showReasonExcludeFormGroup}>
                                    <label><FormattedMessage id="bill_setting.exclude.termination_date"/></label>
                                    <div>
                                        <DatePicker
                                            name='terminalDate'
                                            dateFormat='YYYY/MM/DD'
                                            selected={
                                                this.state.data.terminalDate ?
                                                    moment(this.state.data.terminalDate, 'YYYY-MM-DD') :
                                                    moment()
                                            }
                                            onChange={this.handleTerminalDateChange}
                                        />
                                    </div>
                                </div>) : null}
                                {this.state.data.reasonType == 2 ? (<div className={showReasonExcludeFormGroup}>
                                    <label><FormattedMessage id="bill_setting.exclude.leave_start"/></label>
                                    <div>
                                        <DatePicker
                                            name='leaveStartDate'
                                            dateFormat='YYYY/MM/DD'
                                            selected={
                                                this.state.data.leaveStartDate ?
                                                    moment(this.state.data.leaveStartDate, 'YYYY-MM-DD') :
                                                    moment()
                                            }
                                            onChange={this.handleLeaveStartDateChange}
                                        />
                                    </div>
                                </div>) : null}

                                {this.state.data.reasonType == 2 ? (<div className={showReasonExcludeFormGroup}>
                                    <label><FormattedMessage id="bill_setting.exclude.leave_end"/></label>
                                    <div>
                                        <DatePicker
                                            name='leaveEndDate'
                                            dateFormat='YYYY/MM/DD'
                                            selected={
                                                this.state.data.leaveEndDate ?
                                                    moment(this.state.data.leaveEndDate, 'YYYY-MM-DD') :
                                                    moment()
                                            }
                                            onChange={this.handleLeaveEndDateChange}
                                        />
                                    </div>
                                </div>) : null}
                            </div>

                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={this.cancelRejectClient}>
                                <FormattedMessage id="bill_setting.action.cancel"/>
                            </Button>
                            <button className='btn btn-primary'
                                    disabled={this.props.saving}
                                    onClick={this.handleSaveRejectClient}>
                                {this.props.saving ?
                                    <FormattedMessage id='bill_setting.action.saving'/> :
                                    <FormattedMessage id='bill_setting.action.save'/>}
                            </button>
                        </Modal.Footer>
                    </Modal>
                </div>
            </div>
        );
    }
}

BillingExcludeForm.propsType = {
    client: PropTypes.object.isRequired,
    showModal: PropTypes.bool.isRequired,
    saveExcludeBilling: PropTypes.func.isRequired,
    cancelRejectClient: PropTypes.func.isRequired,
    saving: PropTypes.boolean

};

export default BillingExcludeForm;
