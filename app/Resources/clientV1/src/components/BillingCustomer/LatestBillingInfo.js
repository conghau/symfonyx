import React, {PropTypes} from 'react';
import {FormattedMessage} from 'react-intl';
import moment from 'moment';
import LatestInfoStep from './LatestInfoStep';

class LatestBillingInfo extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        var latestBillingInfo = this.props.info;
        var steps = this.getStepByState();

        const startDate = (latestBillingInfo.start_date) ? moment(latestBillingInfo.start_date).format('DD, MMM YYYY') : null;
        const endDate = (latestBillingInfo.end_date) ? moment(latestBillingInfo.end_date).format('DD, MMM YYYY') : null;

        return (
            <div className='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                <div className='portlet light'>
                    <div className='portlet-title'>
                        <div className='caption'>
                            <i className="icon-credit-card font-green"></i>
                            <span className='caption-subject bold uppercase font-green'>
                                Current Bill Cycle: {startDate} - {endDate}
                            </span>
                        </div>
                    </div>

                    <div className='portlet-body'>
                        <div className="cd-horizontal-timeline mt-timeline-horizontal loaded">
                            <div className="timeline">
                                <div className="events-wrapper">
                                    <div className="events col-lg-10 col-md-10 col-sm-10">
                                        <ol>
                                            { Object.keys(steps).map((index) =>
                                                <LatestInfoStep
                                                    key={index}
                                                    index={index}
                                                    step={steps[index]}/>
                                            )}
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    getStepByState() {
        var steps = [];
        var billingStatuses = [];

        if (typeof(this.props.info.billing_statuses) !== 'undefined') {
            billingStatuses = this.props.info.billing_statuses;
        }

        var step1 = {
            name: 'Invoiced',
            date: moment(this.props.info.created_at).format('DD, MMM YYYY'),
            delay: 0
        };

        var step4 = {
            name: 'Received',
            date: '',
            delay: 0
        };

        var step2 = {
            name: 'Approved',
            date: '',
            delay: 0
        };
        var step3 = {
            name: 'Paid',
            date: '',
            delay: 0
        };

        if (this.props.info.billing_delay == 'approval_delayed') {
            step2.name = 'Approval Delayed';
            step2.date = '';
            step2.delay = 1;
        } else if (this.props.info.billing_delay == 'payment_delayed') {
            step3.name = 'Paid Delayed';
            step3.Date = '';
            step3.delay = 1;
        }

        for (var i = 0; i < billingStatuses.length; i++) {
            if (billingStatuses[i].state == 'approved') {
                step2.date = moment(billingStatuses[i].createdAt).format('DD, MMM YYYY');
            }

            if (billingStatuses[i].state == 'paid') {
                step3.date = moment(billingStatuses[i].createdAt).format('DD, MMM YYYY');
            }

            if (billingStatuses[i].state == 'received') {
                step4.date = moment(billingStatuses[i].createdAt).format('DD, MMM YYYY');
            }
        }


        steps.push(step1);
        steps.push(step2);
        steps.push(step3);
        steps.push(step4);

        return steps;
    }
}
LatestBillingInfo.propTypes = {
    info: PropTypes.object
};
export default LatestBillingInfo;