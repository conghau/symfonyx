import React, {PropTypes} from 'react';
import {FormattedDate} from 'react-intl';
import moment from 'moment';

class ExceptionMonthRow extends React.Component {

    render() {
        //const messageId = `exception_month.month_${this.props.index}`;

        const monthText = <FormattedDate
            value={moment(`2016-${this.props.index > 9 ? this.props.index : '0' + this.props.index}-01`)}
            month='long'/>;
        //const monthText = <FormattedMessage id={messageId}/>;
        if (this.props.isExceptionMonth == 1) {
            return (
                <div className='col-lg-4 col-md-4 col-sm-4 m-b-md'>
                    <button disabled="disabled" type='button' className='btn green btn-block'>{monthText}</button>
                </div>
            );
        } else {
            return false;
        }
    }
}

ExceptionMonthRow.propTypes = {
    isExceptionMonth: PropTypes.number
};

export default ExceptionMonthRow;