/**
 * Created by gundamkid on 25/10/16.
 */

import React, { PropTypes } from 'react';

class LatestInfoStep extends React.Component {

    render() {
        var step = this.props.step;
        var itemWidth = this.props.index * 32;

        var nodeClassName = step.delay == 0 ? "border-after-blue bg-after-blue" : "border-after-blue bg-after-blue selected font-red";
        return (
            <li>
                <a href="#" className={nodeClassName} style={{left: itemWidth + "%"}}>{step.name}<br/>{step.date}</a>
            </li>
        );
    };
}

LatestInfoStep.propTypes = {
    step: PropTypes.object,
    index: PropTypes.string
};

export default LatestInfoStep;