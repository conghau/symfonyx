/* global
 iCareLocale
 */
import React, {PropTypes} from 'react';
import ReactPaginate from 'react-paginate';
import {FormattedMessage} from 'react-intl';

import constants from '../Billing/constants';
import * as globalConstant from '../common/constant';

import LatestBillingListItem from './LatestBillingListItem';


class LatestBillingList extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.handleChangeState = this.handleChangeState.bind(this);
        this.handlePageClick = this.handlePageClick.bind(this);
    }

    handleChangeState(event) {
        event.preventDefault();
        let filter = {state: event.target.value};
        this.setState(filter);
        this.props.onChange(filter);
    }
    handlePageClick(event) {
        let selected = event.selected;
        let state = (this.state == null) ? '' : this.state.state;
        this.props.onPagingClick({page: (selected + 1), state:state})
    }

    render() {
        var billingStatuses = constants.billingStatus;
        var pageNumber = Math.floor(parseInt(this.props.totalRecords) / parseInt(globalConstant.PAGE_SIZE));
        var reminder = parseInt(this.props.totalRecords) % parseInt(globalConstant.PAGE_SIZE);
        if (reminder > 0) {
            pageNumber++;
        }

        var noResult = false;
        if (this.props.billingList.length == 0) {
            noResult = true;
        }

        return (
            <div className='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                <div className='portlet light'>
                    <div className='portlet-title'>
                        <div className='caption'>
                            <i className="icon-list font-green"></i>
                            <span className='caption-subject bold uppercase font-green'>
                                <FormattedMessage id="customer_overview.latest_billing_list.title" />
                            </span>
                        </div>
                    </div>

                    <div className='portlet-title pull-right' style={{"border-bottom": "none"}}>
                        <div className= "col-lg-6 col-md-6 col-sm-6 col-xs-6 m-t-xs">
                            <FormattedMessage id="customer_overview.latest_billing_list.dropdown_label"/>
                        </div>
                        <div className= "col-lg-6 col-md-6 col-sm-6 col-xs-6">

                            <select
                                onChange={this.handleChangeState}
                                className='form-control pull-right'>
                                <option key='' value=''>All</option>
                            {billingStatuses.map(billingStatus =>
                                <option key={billingStatus.value} value={billingStatus.value}>
                                    <FormattedMessage id={billingStatus.text}/>
                                </option>
                            )}
                            </select>
                        </div>
                    </div>

                    <div className='portlet-body'>
                        { !noResult ?
                            <div className='table-responsive'>
                                <table className='table table-bordered'>
                                    <thead>
                                        <tr>
                                            <th> <FormattedMessage id='customer_overview.latest_billing_list.header.cycle' /></th>
                                            <th> <FormattedMessage id='customer_overview.latest_billing_list.header.cycle_period' /></th>
                                            <th> <FormattedMessage id='customer_overview.latest_billing_list.header.payment_due' /></th>
                                            <th> <FormattedMessage id='customer_overview.latest_billing_list.header.status' /></th>
                                            <th> <FormattedMessage id='customer_overview.latest_billing_list.header.action' /></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {Object.keys(this.props.billingList).map((row, index) =>
                                        <LatestBillingListItem
                                            key={index}
                                            index={index}
                                            billingItem={this.props.billingList[index]}
                                        />
                                    )}
                                    </tbody>
                                </table>

                                <ReactPaginate previousLabel={'<'}
                                               nextLabel={'>'}
                                               breakLabel={<a href=''>...</a>}
                                               breakClassName={'break-me'}
                                               pageNum={pageNumber}
                                               marginPagesDisplayed={2}
                                               pageRangeDisplayed={10}
                                               clickCallback={this.handlePageClick}
                                               containerClassName={'pagination'}
                                               subContainerClassName={'pages pagination'}
                                               activeClassName={'active'} />
                            </div> : <div className="alert alert-info"><FormattedMessage id="customer_overview.latest_billing_list.empty"/></div>
                        }
                    </div>
                </div>
            </div>
        );
    }
}

LatestBillingList.propTypes = {
    billingList: PropTypes.array,
    onChange: PropTypes.func,
    onPagingClick: PropTypes.func,
    totalRecords: PropTypes.string
};

export default LatestBillingList;