import React, {PropTypes} from 'react';
import { Link } from 'react-router';
import moment from 'moment';
import {FormattedMessage} from 'react-intl';

class LatestBillingListItem extends React.Component {
    render() {
        var item = this.props.billingItem;
        var itemStatusId = 'customer_overview.latest_billing_list.status.' + item.state;
        var isShowRecordPaymentLink = false;
        if (item.state == 'approved' || item.state == 're-opened') {
            isShowRecordPaymentLink = true;
        }

        var recordPaymentLink = '/billing/record-payment/' + item.id;
        var viewBilling = '/billing/detail/' + item.id;
        return (
            <tr>
                <td>{moment([item.billingYear, (item.billingMonth - 1)]).format('MMM YYYY')}</td>
                <td>
                    {moment(item.startDate).format('DD MMM YYYY')} - {moment(item.endDate).format('DD MMM YYYY')}
                </td>
                <td>{moment(item.dueDate).format('DD MMM YYYY')}</td>
                <td>
                    <FormattedMessage id={itemStatusId} />&nbsp;
                    {item.delay.length > 0 &&
                        <span className="font-red">{item.delay}</span>
                    }
                </td>
                <td>
                    <Link to={viewBilling}>
                        <FormattedMessage id="customer_overview.latest_billing_list.action.view" />
                    </Link>&nbsp;&nbsp;&nbsp;
                    {isShowRecordPaymentLink == true &&
                        <Link to={recordPaymentLink}>
                            <FormattedMessage id="customer_overview.latest_billing_list.action.record_payment" />
                        </Link>
                    }
                </td>
            </tr>
        )
    }
}

LatestBillingListItem.propTypes = {
    billingItem: PropTypes.object
};

export default LatestBillingListItem;
