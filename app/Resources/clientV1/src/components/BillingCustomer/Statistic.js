import React, {PropTypes} from 'react';
import {FormattedMessage} from 'react-intl';
import {FormattedNumber} from 'react-intl';
import ExceptionMonth from '../../components/BillingCustomer/ExceptionMonth';


class Statistic extends React.Component {
    render() {
        return (
            <div className="row m-b-md" style={{display: "flex"}}>
                <div className='col-lg-3 col-md-3 col-sm-6 col-xs-12'>
                    <div className='portlet light' style={{height: '100%'}}>
                        <div className='portlet-title'>
                            <div className='caption'>
                                <i className="icon-layers font-green"></i>
                                <span className='caption-subject bold uppercase font-green'>
                                    <FormattedMessage id="customer_overview.statistic.payable_header"/>
                                </span>
                            </div>
                        </div>

                        <div className='portlet-body'>
                            <div className='dashboard-stat2 dashboard-stat2-no-padding'>
                                <div className='display'>
                                    <div className='number'>
                                        <h4><FormattedMessage id="customer_overview.statistic.mtd"/>: </h4>
                                    </div>
                                    <div className='number pull-right'>
                                        <h4>
                                            <FormattedNumber
                                                value={this.props.statistics.payable_mtd}
                                                style='currency'
                                                minimumFractionDigits={0}
                                                currency='USD'/>
                                            <br />
                                        </h4>
                                    </div>
                                </div>
                                <div className='display'>
                                    <div className='number'>
                                        <h4><FormattedMessage id="customer_overview.statistic.ytd"/>: </h4>
                                    </div>
                                    <div className='number pull-right'>
                                        <h4>
                                            <FormattedNumber
                                                value={this.props.statistics.payable_ytd}
                                                style='currency'
                                                minimumFractionDigits={0}
                                                currency='USD'/>
                                            <br />
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='col-lg-3 col-md-3 col-sm-6 col-xs-12'>
                    <div className='portlet light'  style={{height: '100%'}}>
                        <div className='portlet-title'>
                            <div className='caption'>
                                <i className="icon-layers font-green"></i>
                                <span className='caption-subject bold uppercase font-green'>
                                    <FormattedMessage id="customer_overview_statistic.received_header"/>
                                </span>
                            </div>
                        </div>

                        <div className='portlet-body'>
                            <div className='dashboard-stat2 dashboard-stat2-no-padding'>
                                <div className='display'>
                                    <div className='number'>
                                        <h4><FormattedMessage id="customer_overview.statistic.mtd"/>: </h4>
                                    </div>
                                    <div className='number pull-right'>
                                        <h4>
                                            <FormattedNumber
                                                value={this.props.statistics.received_mtd}
                                                style='currency'
                                                minimumFractionDigits={0}
                                                currency='USD'/>
                                            <br />
                                        </h4>
                                    </div>
                                </div>
                                <div className='display'>
                                    <div className='number'>
                                        <h4><FormattedMessage id="customer_overview.statistic.ytd"/>: </h4>
                                    </div>
                                    <div className='number pull-right'>
                                        <h4>
                                            <FormattedNumber
                                                value={this.props.statistics.received_ytd}
                                                style='currency'
                                                minimumFractionDigits={0}
                                                currency='USD'/>
                                            <br />
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ExceptionMonth exceptionMonths={this.props.exceptionMonths}/>
            </div>
        );
    }
}

Statistic.propTypes = {
    statistics: PropTypes.object,
    exceptionMonths: PropTypes.object
};

export default Statistic;
