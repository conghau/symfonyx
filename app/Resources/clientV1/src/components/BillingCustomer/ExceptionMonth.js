import React, {PropTypes} from 'react';
import {FormattedMessage} from 'react-intl';
import ExceptionMonthRow from './ExceptionMonthRow';

class ExceptionMonth extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        let emptyExceptionMonth = 0;
        for (let index = 1; index <= 12; index++) {
            if (this.props.exceptionMonths[index]) {
                emptyExceptionMonth = 1;
            }
        }
        return (
            <div className='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                <div className='portlet light' style={{height: '100%'}}>
                    <div className='portlet-title'>
                        <div className='caption'>
                            <i className="icon-layers font-green"></i>
                            <span className='caption-subject bold uppercase font-green'>
                                <FormattedMessage id="customer_overview.exception_month.header"/>
                            </span>
                        </div>
                    </div>
                    <a href="/billing/cycle-template">
                        <div className='portlet-body'>
                            {emptyExceptionMonth == 1 ?
                                <div className="dashboard-stat2 dashboard-stat2-no-padding">
                                    <div className='display'>
                                        { Object.keys(this.props.exceptionMonths).map((index) =>
                                            <ExceptionMonthRow
                                                key={index}
                                                index={index}
                                                isExceptionMonth={this.props.exceptionMonths[index]}/>
                                        )}
                                    </div>
                                </div>
                                :
                                <div style={{'font-style': 'italic'}}>
                                    <FormattedMessage id="customer_overview.exception_month.empty"/>
                                </div>
                            }
                        </div>
                    </a>
                </div>
            </div>
        )
    }
}

ExceptionMonth.propTypes = {
    exceptionMonths: PropTypes.object
};
export default ExceptionMonth;

