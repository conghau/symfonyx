import React, {PropTypes} from 'react';
import {FormattedNumber, FormattedMessage} from 'react-intl';

class BillingOverview extends React.Component {
    render() {
        const className = `btn blue btn-outline btn-circle btn-sm mt-ladda-btn ladda-button `;
        return (
            <div id="billing_admin_overview_block" className="col-md-6 col-sm-6">
                <div className="portlet light" style={{height: "100%"}}>
                    <div className="portlet-title tabbable-line">
                        <div className="caption">
                            <i className="icon-pie-chart font-red-sunglo"></i>
                            <span className="caption-subject font-red-sunglo sbold">
                                <FormattedMessage id="admin_overview.billing_overview.receivable_pedding_payment"/>
                            </span>
                        </div>

                    </div>
                    <div className="portlet-body">
                        <div className=' m-b-md'>
                            <div className="actions">
                                <div className="btn-group btn-group-devided" data-toggle="buttons">
                                    <button onClick={this.props.filterStatistic.bind(null, 30)}
                                            data-style="zoom-in"
                                            data-size="zoom-in"
                                            className={this.props.period == 30 ? className + " active" : className}>
                                    <span className="ladda-label">
                                        <FormattedMessage id="admin_overview.billing_overview.filter.30_days"/>
                                    </span>
                                        <span className="ladda-spinner"></span>
                                    </button>
                                    <button onClick={this.props.filterStatistic.bind(null, 60)}
                                            className={this.props.period == 60 ? className + " active" : className}>
                                    <span className="ladda-label">
                                        <FormattedMessage id="admin_overview.billing_overview.filter.60_days"/>
                                    </span>
                                        <span className="ladda-spinner"></span>
                                    </button>
                                    <button onClick={this.props.filterStatistic.bind(null, 90)}
                                            className={this.props.period == 90 ? className + " active" : className}>
                                    <span className="ladda-label">
                                        <FormattedMessage id="admin_overview.billing_overview.filter.90_days"/>
                                    </span>
                                        <span className="ladda-spinner"></span>
                                    </button>
                                </div>
                            </div>
                            <div className='display m-t-lg'>
                                <div className='number'>
                                    <div>
                                        <h3 className="text-success">
                                            <small><FormattedMessage id="admin_overview.billing_overview.receivable"/>
                                            </small>
                                            &nbsp; <span className="sbold"><FormattedNumber
                                            value={Object.keys(this.props.statistic).length > 0 ? this.props.statistic.receivable : 0}
                                            style='currency'
                                            minimumFractionDigits={0}
                                            currency={iCareCurrency}/>
                                            </span>
                                        </h3>
                                    </div>
                                    <div className="m-t-md">
                                        <h3 className="text-success">
                                            <small><FormattedMessage
                                                id="admin_overview.billing_overview.payment_waiting_confirmation"/>
                                            </small>
                                            &nbsp; <span className="sbold">
                                            <FormattedNumber
                                                value={Object.keys(this.props.statistic).length > 0 ? this.props.statistic.awaitingConfirmation : 0}
                                                style='currency'
                                                minimumFractionDigits={0}
                                                currency={iCareCurrency}/>
                                          </span>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

BillingOverview.propsType = {
    statistic: PropTypes.object.isRequired
};

export default BillingOverview;
