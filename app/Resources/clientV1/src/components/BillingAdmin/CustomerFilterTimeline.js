import React, {PropTypes} from 'react';
import {initTimeline} from './common/HorizontalTimeline';
import $ from 'jquery';
import {FormattedMessage} from 'react-intl';

class CustomerFilter extends React.Component {
    componentDidMount() {
        initTimeline($(this.refs.timeLineContainer));
    }

    componentDidUpdate() {
        initTimeline($(this.refs.timeLineContainer));
    }

    handleClick(state, event) {
    }

    render() {
        return (
            <div className="m-t-lg">
                <div ref="timeLineContainer" className="cd-horizontal-timeline mt-timeline-horizontal loaded">
                    <div className="timeline">
                        <div className="events-wrapper">
                            <div className="events" style={{width: '500px'}}>
                                <ol>
                                    <li>
                                        <a onClick={this.props.filterCustomerByState.bind(null, 'billed')} href="#0"
                                           data-date="01/01/2010"
                                           className="border-after-blue bg-after-blue selected"
                                           style={{left: '0'}}>
                                            {this.props.statusesStatistic.billed}
                                            <br/>
                                            <FormattedMessage id="admin_overview.status.billed"/>
                                        </a>
                                    </li>
                                    <li>
                                        <a onClick={this.props.filterCustomerByState.bind(null, 'approved')} href="#0"
                                           data-date="03/01/2010"
                                           className="border-after-blue bg-after-blue "
                                           style={{left: '0px'}}>
                                            {this.props.statusesStatistic.approved}
                                            <br/>
                                            <FormattedMessage id="admin_overview.status.approved"/>
                                        </a>
                                    </li>
                                    <li>
                                        <a onClick={this.props.filterCustomerByState.bind(null, 'paid')} href="#0"
                                           data-date="12/01/2010" className="border-after-blue bg-after-blue"
                                           style={{left: '0px'}}>
                                            {this.props.statusesStatistic.paid}
                                            <br/>
                                            <FormattedMessage id="admin_overview.status.paid"/>
                                        </a>
                                    </li>
                                    <li>
                                        <a onClick={this.props.filterCustomerByState.bind(null, 'received')} href="#0"
                                           data-date="25/01/2010" className="border-after-blue bg-after-blue "
                                           style={{left: '0px'}}>
                                            {this.props.statusesStatistic.received}
                                            <br/>
                                            <FormattedMessage id="admin_overview.status.received"/>
                                        </a>
                                    </li>
                                </ol>
                                <span className="filling-line bg-blue bold" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

CustomerFilter.propsType = {
    filterCustomerByState: PropTypes.func.isRequired
};

export default CustomerFilter;
