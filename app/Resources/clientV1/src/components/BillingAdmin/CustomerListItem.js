import React, {PropTypes} from 'react';
import {FormattedMessage} from 'react-intl';
import billingConstants from '../../components/Billing/constants';

import CustomerListItemAction from './CustomerListItemAction';

class CustomerListItem extends React.Component {
    render() {
        const b = this.props.billing;
        const customers = this.props.customers;
        const translateStatus = billingConstants.billingStatus.find(status=> {
            return status.value == b['state'];
        });
        const ownerBilling = customers.find(customer => {
            return customer.net_suite_customer_id == b['customerId']
        });
        return (
            <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                <div className="mt-card-item">
                    <div className="mt-card-content">
                        <CustomerListItemAction billing={b}/>
                        <h3 className="mt-card-name m-t-none">{ownerBilling.company_name}</h3>
                        <p className="mt-card-desc font-grey-mint">
                            <FormattedMessage
                                id="admin_overview.customers.cycle_billing"/>: {b['billingMonth']}
                            - {b['billingYear']}
                        </p>
                        <p className="mt-card-desc font-grey-mint">
                            <FormattedMessage id="admin_overview.customers.status"/>: <FormattedMessage
                            id={translateStatus.text}/></p>
                    </div>
                </div>
            </div>
        );
    }
}

CustomerListItem.propsType = {};

export default CustomerListItem;
