/* global
 iCareLocale
 */
import React, {PropTypes} from 'react';
import BillingExceptionMonthRow from './BillingExceptionMonthRow';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import moment from 'moment';
import {FormattedMessage,FormattedDate,FormattedHTMLMessage} from 'react-intl';

class BillingExceptionMonth extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            showModal: false,
            data: {},
            messageText: ''
        };
        this.onChange = this.onChange.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onClose = this.onClose.bind(this);
    }

    onClose() {
        this.setState({showModal: false});
    }

    onChange(event, data) {
        event.preventDefault();
        let messageText = '';
        if (data.value == 1) {
            messageText = <FormattedHTMLMessage id="exception_month.message.confirm.add"
                                            values={{month: iCareLocale[`exception_month.month_${data.indexKey}`]}}
            />
        } else {
            messageText =
                <FormattedHTMLMessage id="exception_month.message.confirm.remove"
                                  values={{month: iCareLocale[`exception_month.month_${data.indexKey}`]}}/>
        }
        this.setState({showModal: true, data: data, messageText: messageText});
    }

    onSave() {
        this.props.onChange(this.state.data);
        this.onClose();
    }

    render() {

        return (
            <div className="col-md-6 col-sm-6" >
                <div className='portlet light portlet-fit' style={{height:"100%"}}>
                        <div className='portlet-body'>
                            <div className='mt-element-list'>
                                <div className='mt-list-head list-default green-haze'>
                                    <div className='row'>
                                        <div className='col-xs-8'>
                                            <div className='list-head-title-container'>
                                                <h3 className='list-title sbold'>
                                                    <FormattedMessage id="exception_month"/>

                                                </h3>
                                                <div className='list-date'>
                                                    <FormattedDate value={moment()}
                                                                   year='numeric'
                                                                   month='long'
                                                                   day='2-digit'
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div className='col-xs-4'>
                                            <div className='list-head-summary-container'>
                                                <div className='list-done'>
                                                    <div
                                                        className='badge badge-default list-count'>{this.props.exceptionMonthRecords.totalMonth}</div>
                                                    &nbsp;
                                                    <div className='list-label'>
                                                        <FormattedMessage
                                                            id="exception_month.counter.multi"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className='mt-list-container list-simple'>
                                    <div className='row'>
                                        { Object.keys(this.props.exceptionMonthRecords.data).map((index) =>
                                            <BillingExceptionMonthRow
                                                key={index}
                                                index={index}
                                                isExceptionMonth={this.props.exceptionMonthRecords.data[index]}
                                                hasEditRole={this.props.hasEditRole}
                                                onChange={this.onChange}/>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <Modal show={this.state.showModal} onHide={this.onClose}>
                    <Modal.Header>
                        <FormattedMessage id="exception_month.confirmation"/>
                    </Modal.Header>
                    <Modal.Body>
                        <p>{this.state.messageText}</p>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.onClose}>
                            <FormattedMessage id="exception_month.confirmation.no"/>
                        </Button>
                        <Button className='btn btn-primary' onClick={this.onSave}>
                            <FormattedMessage id="exception_month.confirmation.yes"/>
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}
BillingExceptionMonth.propTypes = {
    exceptionMonthRecords: PropTypes.object,
    hasEditRole: PropTypes.bool,
    onChange: PropTypes.func
};
export default BillingExceptionMonth;
