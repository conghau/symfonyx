import React, {PropTypes} from 'react';
import {FormattedMessage} from 'react-intl';
import CustomerFilter from './CustomerFilter';
import CustomerList from './CustomerList';
import CustomerFilterSearchBox from './CustomerFilterSearchBox';

class BillingCustomer extends React.Component {
    render() {
        return (
            <div className="col-md-12   ">
                <div className='portlet light'>
                    <div className='portlet-title'>
                        <div className='caption'>
                            <i className='icon-users font-red-sunglo'/>
                            <span className='caption-subject font-red-sunglo bold uppercase'>
                                <FormattedMessage id="admin_overview.customers"/>
                            </span>
                        </div>
                        <CustomerFilterSearchBox
                            keyword={this.props.keyword}
                            filterCustomerByName={this.props.filterCustomerByName}
                        />
                    </div>
                    <div className='portlet-body'>
                        <CustomerFilter
                            period={this.props.period}
                            statusesStatistic={this.props.statusesStatistic}
                            filterCustomerByDate={this.props.filterCustomerByDate}
                            filterCustomerByState={this.props.filterCustomerByState}
                        />
                        {(this.props.billings.length) > 0 ?
                            <CustomerList customers={this.props.customers} billings={this.props.billings}/>
                            : null
                        }
                    </div>


                </div>
            </div>
        );
    }
}

BillingCustomer.propsType = {};

export default BillingCustomer;
