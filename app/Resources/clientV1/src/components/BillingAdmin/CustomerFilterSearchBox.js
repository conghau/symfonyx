import React, {PropTypes} from 'react';
import {FormattedMessage} from 'react-intl';
class CustomerFilterSearchBox extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {keyword: ''};
        this.onSubmit = this.onSubmit.bind(this);
        this.onChangeKeyword = this.onChangeKeyword.bind(this);
    }

    onSubmit(event) {
        event.preventDefault();
        this.props.filterCustomerByName(this.state.keyword);
        return false;
    }

    onChangeKeyword(event) {
        this.setState({
            keyword: event.target.value
        });
    }

    render() {
        return (
            <div className="actions">
                <div className="portlet-input input-inline input-small">
                    <form onSubmit={this.onSubmit}>
                        <div className="input-icon right">
                            <i className="icon-magnifier"></i>
                            <input type="text" onChange={this.onChangeKeyword} defaultValue={this.props.keyword}
                                   className="form-control input-circle"
                                   placeholder="search..."/>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}


export default CustomerFilterSearchBox;
