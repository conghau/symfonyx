import React, {PropTypes} from 'react';
import CustomerListItem from './CustomerListItem';

class CustomerList extends React.Component {
    render() {
        return (
            <div className="row">
                <div className="mt-element-card mt-card-round mt-element-overlay">
                    {this.props.billings && this.props.billings.map(billing=>
                        <CustomerListItem key={billing.id} billing={billing} customers={this.props.customers}/>
                    )}
                </div>
            </div>
        );
    }
}

CustomerList.propsType = {
    customers: React.PropTypes.array.isRequired,
    billings: React.PropTypes.array.isRequired

};

export default CustomerList;
