/* global
 iCareLocale
 */
import React, {PropTypes} from 'react';
import moment from 'moment';
import {FormattedMessage, FormattedDate} from 'react-intl';

class BillingExceptionMonthRow extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.onChangeStatus = this.onChangeStatus.bind(this);
    }

    onChangeStatus(event) {
        const month = Number(moment().format('M'));
        if (month < this.props.index) {
            if (this.props.hasEditRole == true) {
                event.preventDefault();
                const data = {'indexKey': this.props.index, 'value': ((this.props.isExceptionMonth) ? '0' : '1')};
                this.props.onChange(event, data);
                event.stopPropagation();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    render() {
        const isExceptionMonth = this.props.isExceptionMonth;
        const active = (isExceptionMonth) ? 'btn blue btn-block 1' : 'btn default btn-block';
        //const messageId = `exception_month.month_${this.props.index}`;

        const message = <FormattedDate
            value={moment(`2016-${this.props.index}-01`)}
            month='long'/>;
        //const message = iCareLocale[messageId];
        //const monthText = <FormattedMessage id={message}/>;
        const classNameContainer = 'col-lg-3 col-md-4 col-sm-6 margin-bottom-10';
        return (
            <div className={classNameContainer}>
                <button type='button' onClick={this.onChangeStatus} className={active}>{message}</button>
            </div>
        );
    }
}
BillingExceptionMonthRow.propTypes = {
    isExceptionMonth: PropTypes.number,
    hasEditRole: PropTypes.bool,
    onChange: PropTypes.func
};

export default BillingExceptionMonthRow;
