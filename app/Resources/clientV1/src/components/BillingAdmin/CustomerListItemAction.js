import React from 'react';
import {FormattedMessage} from 'react-intl';
import {Link} from 'react-router';

class CustomerListItemAction extends React.Component {
    render() {
        let actions = [];
        const billing = this.props.billing;
        const state = billing.state;

        //Add default view action
        actions.push({text: 'admin_overview.customers.action.view', url: `/billing/detail/${billing.id}`});

        if (state == 'billed' || state == 're_opened' || state == 'approved') {
            actions.push({
                text: 'admin_overview.customers.action.record_payment',
                url: `/billing/record-payment/${billing.id}`
            })
        }
        else if (state == 'paid') {
            actions.push({
                text: 'admin_overview.customers.action.confirm_payment',
                url: `/billing/confirm-payment/${billing.id}/0`
            })
        }

        return (
            <div className="actions" style={{position: 'absolute', top: '5px', right: '5px'}}>
                <div className="btn-group">
                    <a className="btn btn-md" href="javascript:;" data-toggle="dropdown"
                       data-hover="dropdown" data-close-others="true">
                        <i className="fa fa-ellipsis-v"></i>
                    </a>
                    <ul className="dropdown-menu pull-right">
                        {actions.map((action, index) =>
                            <li key={index}>
                                <Link to={action.url}><FormattedMessage id={action.text}/></Link>
                            </li>
                        )}
                    </ul>
                </div>
            </div>
        );
    }
}

CustomerListItemAction.propTypes = {
    billing: React.PropTypes.object.isRequired
}
export default CustomerListItemAction;
