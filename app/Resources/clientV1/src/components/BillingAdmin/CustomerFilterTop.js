import React, {PropTypes} from 'react';
import {FormattedMessage} from 'react-intl';
class CustomerFilterTop extends React.Component {
    render() {
        const className="btn blue btn-outline btn-circle btn-sm mt-ladda-btn ladda-button ";
        return (
            <div >
                <div className="actions">
                    <div className="btn-group btn-group-devided" data-toggle="buttons">
                        <button
                                onClick={this.props.filterCustomerByDate.bind(null, 30)}
                                data-style="zoom-in"
                                data-size="zoom-in"
                                className={this.props.period == 30 ? className + " active" : className}>
                            <span className="ladda-label">
                                <FormattedMessage id="admin_overview.billing_overview.filter.30_days"/>
                            </span>
                            <span className="ladda-spinner"></span>
                        </button>
                        <button
                                onClick={this.props.filterCustomerByDate.bind(null, 60)}
                                className={this.props.period == 60 ? className + " active" : className}>
                            <span className="ladda-label">
                                <FormattedMessage id="admin_overview.billing_overview.filter.60_days"/>
                            </span>
                            <span className="ladda-spinner"></span>
                        </button>
                        <button
                                onClick={this.props.filterCustomerByDate.bind(null, 90)}
                                className={this.props.period == 90 ? className + " active" : className}>
                            <span className="ladda-label">
                                <FormattedMessage id="admin_overview.billing_overview.filter.90_days"/>
                            </span>
                            <span className="ladda-spinner"></span>
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

CustomerFilterTop.propsType = {
    filterCustomerByDate: PropTypes.func.isRequired,
    period: PropTypes.number,
};

export default CustomerFilterTop;
