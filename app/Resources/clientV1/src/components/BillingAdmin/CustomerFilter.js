import React, {PropTypes} from 'react';
import CustomerFilterTimeline from './CustomerFilterTimeline';
import CustomerFilterTop from './CustomerFilterTop';

class CustomerFilter extends React.Component {
    render() {
        return (
            <div>
                <CustomerFilterTop period={this.props.period} filterCustomerByDate={this.props.filterCustomerByDate}/>
                <CustomerFilterTimeline
                    statusesStatistic={this.props.statusesStatistic}
                    filterCustomerByState={this.props.filterCustomerByState}
                />
            </div>
        );
    }
}

CustomerFilter.propsType = {};

export default CustomerFilter;
