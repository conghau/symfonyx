import React, {PropTypes} from 'react';
import UploadStatusMapping from '../common/upload/UploadStatusMapping';
import UploadGoToNextStep from '../common/upload/UploadGoToNextStep';

const UploadLatestListRow = ({row, uploadTransactionStatus}) => {
    return (
        <tr>
            <td>{row.id}</td>
            <td>{row.fileName}
                <a href={row.fileUrl} className='m-l-xs text-info'>
                    <i className='fa fa-download'/>
                </a>
            </td>
            <td><UploadStatusMapping status={uploadTransactionStatus[row.status]}/></td>
            <td>{row.createdAt}</td>
            <td>
                {(row.status != 0 ?
                        <UploadGoToNextStep status={uploadTransactionStatus[row.status]} transactionId={row.id}/> : ''
                )}
            </td>
        </tr>
    );
};

UploadLatestListRow.propTypes = {
    row: PropTypes.object.isRequired
};
export default UploadLatestListRow;
