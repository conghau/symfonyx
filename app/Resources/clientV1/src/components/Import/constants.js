export default {
  uploadTransactionStatus : [
    {
      'name': 'ABORTED',
      'className': 'badge badge-roundless badge-alert',
      'nextStepUrl': '',
      'nextStepText': ''
    },
    {
      'name': 'UPLOADED',
      'className': 'badge badge-roundless badge-warning',
      'nextStepUrl': '/employee/import/mapping/select/',
      'nextStepText': 'Select Mapping'
    },
    {
      'name': 'MAPPED',
      'className': 'badge badge-roundless badge-info',
      'nextStepUrl': '/employee/import/mapping/validate/',
      'nextStepText': 'Validation Result'
    },
    {
      'name': 'QUEUED',
      'className': 'badge badge-roundless badge-primary',
      'nextStepUrl': '/employee/import/mapping/processing/',
      'nextStepText': 'Processing Result'
    },
    {
      'name': 'COMPLETED',
      'className': 'badge badge-roundless badge-success',
      'nextStepUrl': '/employee/import/mapping/statistic/',
      'nextStepText': 'Statistic Result'
    }
  ],
  mappingTypes: {
    'fullName' : {
      'type': 'text',
      'validation': { 'required':true },
      'data': ''
    },
    'employeeId' : {
      'type': 'text',
      'validation': { 'required':true, 'number':true, 'unique':true },
      'data': ''
    },
    'socialId' : {
      'type': 'text',
      'validation': { 'required':true },
      'data': ''
    },
    'phone' : {
      'type': 'text',
      'validation': { 'required':true, 'phone':true },
      'data': ''
    },
    'hiringDate' : {
      'type': 'text',
      'validation': { 'required':true, 'date':'yyyy-mm-dd' },
      'data': ''
    },
    'jobTitle' : {
      'type': 'text',
      'validation': { 'required':true },
      'data': ''
    },
    'department' : {
      'type': 'text',
      'validation': { 'required':true },
      'data': ''
    },
    'gender' : {
      'type': 'select',
      'validation': { 'required':false },
      'data': [{ 'value': 1, 'text': 'Male' }, { 'value': 2, 'text': 'Female' }, { 'value': 3, 'text': 'Not Specified' }]
    },
    'birthDate' : {
      'type': 'text',
      'validation': { 'required':false, 'date':'yyyy-mm-dd' },
      'data': ''
    },
    'email' : {
      'type': 'text',
      'validation': { 'required':false, 'email': true },
      'data': ''
    },
    'address' : {
      'type': 'text',
      'validation': { 'required':false },
      'data': ''
    },
    'salary' : {
      'type': 'text',
      'validation': { 'required':false, 'number':true },
      'data': ''
    },
    'contractType' : {
      'type': 'text',
      'validation': { 'required':false },
      'data': ''
    },
    'contractEndDate' : {
      'type': 'text',
      'validation': { 'required':false, 'date':'yyyy-mm-dd' },
      'data': ''
    },
    'salaryPaymentMethod' : {
      'type': 'text',
      'validation': { 'required':false },
      'data': ''
    },
    'bankAccountNumber' : {
      'type': 'text',
      'validation': { 'required':false },
      'data': ''
    },
    'bankName' : {
      'type': 'text',
      'validation': { 'required':false },
      'data': ''
    }
  }
};
