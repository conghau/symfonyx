import React, {PropTypes} from 'react';
import MappingValidateResultRow from './MappingValidateResultRow';
import {FormattedMessage} from 'react-intl';

class MappingValidateResult extends React.Component {
    render() {
        const errors = this.props.errors;
        return (
            <div>
                <form>
                    <div className='fixed-table-header'>
                        <table className='table table-hover'>
                            <thead>
                            <tr>
                                <th width='3%'>
                                    <div className='th-inner '>
                                        <FormattedMessage id="mapping.num"/>
                                    </div>
                                    <div className='fht-cell'/>
                                </th>
                                <th width='40%'>
                                    <div className='th-inner '><FormattedMessage id="mapping_validate.list.data"/></div>
                                    <div className='fht-cell'/>
                                </th>
                                <th width='25%'>
                                    <div className='th-inner '><FormattedMessage id="mapping_validate.list.status"/>
                                    </div>
                                    <div className='fht-cell'/>
                                </th>

                            </tr>
                            </thead>
                            <tbody>
                            {Object.keys(this.props.content).map((row, index) =>
                                <MappingValidateResultRow
                                    key={index}
                                    index={index}
                                    row={this.props.content[row]}
                                    errors={(errors[row] ? errors[row] : null)}
                                />
                            )}
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colSpan='3'>
                                    <a onClick={this.props.redirectBack} className='btn btn-default btn-lg pull-left'>
                                        <FormattedMessage id="mapping.back"/>
                                    </a>
                                    <a
                                        disabled={this.props.saving}
                                        onClick={this.props.onSave}
                                        className='btn btn-primary  btn-lg pull-left'
                                    >
                                        {this.props.saving ? <FormattedMessage id='mapping.importing'/> :
                                            <FormattedMessage id='mapping.start_import'/> }
                                    </a>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </form>
            </div>
        );
    }
}

MappingValidateResult.propTypes = {
    errors: PropTypes.object.isRequired,
    content: PropTypes.object.isRequired,
    redirectBack: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired,
    saving: PropTypes.bool
};

export default MappingValidateResult;
