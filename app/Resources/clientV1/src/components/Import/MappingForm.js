import React, {PropTypes} from 'react';
import MappingFormRow from './MappingFormRow';
import {Link} from 'react-router';
import {FormattedNumber, FormattedMessage, FormattedDate} from 'react-intl';

class MappingForm extends React.Component {
    render() {
        const error = [];
        if (this.props.errors) {
            Object.keys(this.props.errors).map((key, index) => {
                error.push(<li key={index}>{this.props.errors[key]}</li>);
            });
        }
        return (
            <div>
                {(error.length > 0 && <div className='alert alert-danger'>
                    <ul id='mapping-error'>
                        {error}
                    </ul>
                </div>)}

                <form>
                    <div className='fixed-table-header'>
                        <table className='table table-hover'>
                            <thead>
                            <tr>
                                <th width='3%'>
                                    <div className='th-inner '><FormattedMessage id='mapping.num'/></div>
                                    <div className='fht-cell'/>
                                </th>
                                <th width='25%'>
                                    <div className='th-inner '><FormattedMessage id='mapping.column'/></div>
                                    <div className='fht-cell'/>
                                </th>
                                <th width='25%'>
                                    <div className='th-inner '><FormattedMessage id='mapping.selection'/></div>
                                    <div className='fht-cell'/>
                                </th>
                                <th width='25%'/>
                            </tr>
                            </thead>
                            <tbody>
                            {this.props.allColumns.map((column, index) =>
                                <MappingFormRow
                                    key={index}
                                    selectedMappings={this.props.selectedMappings}
                                    column={column}
                                    index={index}
                                    allMappings={this.props.allMappings}
                                    onChange={this.props.onChange}
                                    errors={this.props.errors}
                                    totalColumn={this.props.allColumns.length}
                                />
                            )}
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colSpan='3'>
                                    <Link className='btn btn-lg btn-default pull-left'
                                          to='/employee/import/mapping/upload'><FormattedMessage
                                        id='mapping.back'/></Link>
                                    <button
                                        type='button'
                                        className='btn btn-lg btn-primary pull-left'
                                        disabled={this.props.reseting}
                                        onClick={this.props.onResetMapping}>{this.props.reseting ?
                                        <FormattedMessage id='mapping.processing'/> :
                                        <FormattedMessage id='mapping.reset'/>}
                                    </button>
                                    <button
                                        className='btn btn-lg btn-primary pull-left'
                                        disabled={this.props.saving}
                                        onClick={this.props.onSave}>{this.props.saving ?
                                        <FormattedMessage id='mapping.saving'/> : <FormattedMessage id='mapping.save'/>}
                                    </button>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </form>
            </div>
        );
    }
}

MappingForm.propTypes = {
    selectedMappings: PropTypes.object,
    allColumns: PropTypes.array.isRequired,
    allMappings: PropTypes.array.isRequired,
    onResetMapping: PropTypes.func,
    onSave: PropTypes.func,
    onChange: PropTypes.func,
    reseting: PropTypes.bool,
    saving: PropTypes.bool,
    errors: PropTypes.object
};

export default MappingForm;
