import React from 'react';
import { Link } from 'react-router';
import {FormattedMessage} from 'react-intl';

class StatisticTopErrorRow extends React.Component {
  render () {
    const error = this.props.error;
    const resolveLink = '/employee/import/mapping/resolve/' + this.props.transId + '/' + error.id;
    return (
      <tr>
        <td>{error.fullName}</td>
        <td>{error.employeeId}</td>
        <td>{error.hiringDate}</td>
        <td>{error.socialId}</td>
        <td>{error.phone}</td>
        <td>
          <Link to={resolveLink}><FormattedMessage id="mapping.resolve"/></Link>
        </td>
      </tr>
        );
  }
}

export default StatisticTopErrorRow;
