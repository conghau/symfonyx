import React, {PropTypes} from 'react';
import SelectInput from '../common/SelectInput';
import ValidationError from '../common/upload/ValidationError';

const MappingValidateResultRow = ({index, row, errors}) => {
    return (
        <tr>
            <td>{index + 1}</td>
            <td>{row.join(', ')}</td>
            <td>{(errors == null ?
                <span className='label label-lg label-success'>success</span>
                : <ValidationError errors={errors} rowNumber={index}/>)}
            </td>
        </tr>
    );
};
MappingValidateResultRow.propTypes = {
    index: PropTypes.number.isRequired,
    row: PropTypes.array.isRequired,
    errors: PropTypes.array
};
export default MappingValidateResultRow;
