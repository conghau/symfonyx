import React, {PropTypes} from 'react';
import SelectInput from '../common/SelectInput';

const MappingFormRow = ({selectedMappings, column, index, allMappings, onChange, errors, totalColumn}) => {
    const name = `mapping_${index}`;
    const nameHidden = `mapping_field_${index}`;
    const field = selectedMappings && selectedMappings[index] ? selectedMappings[index]['name'] : '';
    const hasError = (errors && field !== '' && errors[field] !== undefined);

    let disableOptions = [];
    if (selectedMappings) {
        disableOptions = Object.keys(selectedMappings).map((key, index) => {
            if (index >= totalColumn) {
                return '';
            } else {
                return (selectedMappings[key] !== null && selectedMappings[key] !== undefined) ? selectedMappings[key]['id'] : '';
            }
        });
    }

    const error = hasError ? errors[field] : null;
    return (
        <tr>
            <td>{index + 1}</td>
            <td>{column}</td>
            <td className={(hasError ? 'has-error' : (errors ? 'has-success' : ''))}>
                <SelectInput
                    name={name}
                    value={selectedMappings && selectedMappings[index] ? (selectedMappings[index]['id'] + '') : ''}
                    defaultOption='None'
                    options={allMappings}
                    disableOptions={disableOptions}
                    onChange={onChange}
                    error={error}
                />
                <input type='hidden' name={nameHidden}
                       value={selectedMappings && selectedMappings[index] ? selectedMappings[index]['name'] : ''}/>
            </td>
        </tr>
    );
};
MappingFormRow.propTypes = {
    allMappings: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
    selectedMappings: PropTypes.object,
    errors: PropTypes.object,
    column: PropTypes.string,
    index: PropTypes.number
};
export default MappingFormRow;
