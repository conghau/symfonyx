import React from 'react';
import DoughnutChart from '../common/DoughnutChart';
import {FormattedMessage} from 'react-intl';

class StatisticChart extends React.Component {
    render() {
        return (
            <div className='portlet'>
                <div className='portlet-title'>
                    <div className='caption'>
                        <i className='fa fa-pie-chart'/> <FormattedMessage id="upload_summary.overview"/>
                    </div>
                </div>
                <div className='portlet-body'>
                    <div className='text-center'>
                        <DoughnutChart error={this.props.statuses.error}
                                       success={this.props.statuses.processed - this.props.statuses.error}/>
                    </div>
                </div>
            </div>

        );
    }
}

export default StatisticChart;
