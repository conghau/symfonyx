import React, {PropTypes} from 'react';
import StatisticListErrorRow from './StatisticListErrorRow';
import ReactPaginate from 'react-paginate';
import {FormattedMessage} from 'react-intl';
import SearchBar from '../../components/ICareMember/SearchBar';

class StatisticListError extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            offset: 0,
            loading: false
        };
        this.handlePageClick = this.handlePageClick.bind(this);
        this.onSearch = this.onSearch.bind(this);
    }

    handlePageClick(data) {
        let selected = data.selected;
        let offset = Math.ceil(selected * this.props.perPage);
        this.props.handlePageClick(selected + 1);
    }
    onSearch(term) {
        this.props.searchErrorEmployeeByName(term);
    }

    render() {
            return (
                <div className='portlet'>
                    <div className='portlet-title'>
                        <div className='caption'>
                            <i className='fa fa-list'/><FormattedMessage id="upload_summary.list_errors"/>
                        </div>
                        <div className="actions col-md-3">
                            <SearchBar
                                onSearch={this.onSearch}
                                placeHolderText="upload_summary.search_by_employee_name"
                            />
                        </div>
                    </div>
                    <div className='portlet-body'>
                        <div className='fixed-table-header'>
                            <table className='table table-hover'>
                                <thead>
                                <tr>
                                    <th width='15%'>
                                        <div className='th-inner '>
                                            <FormattedMessage id="employeeData.fullName"/>
                                        </div>
                                        <div className='fht-cell'/>
                                    </th>
                                    <th width='15%'>
                                        <div className='th-inner '>
                                            <FormattedMessage id="employeeData.employeeId"/>
                                        </div>
                                        <div className='fht-cell'/>
                                    </th>
                                    <th width='15%'>
                                        <div className='th-inner '>
                                            <FormattedMessage id="employeeData.hiringDate"/>
                                        </div>
                                        <div className='fht-cell'/>
                                    </th>
                                    <th width='15%'>
                                        <div className='th-inner '>
                                            <FormattedMessage id="employeeData.socialId"/>
                                        </div>
                                        <div className='fht-cell'/>
                                    </th>

                                    <th width='15%'>
                                        <div className='th-in
                                        ner '><FormattedMessage id="employeeData.phone"/></div>
                                        <div className='fht-cell'/>
                                    </th>
                                    <th width='15%'>
                                        <div className='th-inner '>
                                            <FormattedMessage id="upload.list.action"/></div>
                                        <div className='fht-cell'/>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                { (this.props.errors.data && this.props.errors.data.length > 0)? this.props.errors.data.map((error, index) =>
                                    <StatisticListErrorRow key={index} index={index} error={error}
                                                           transId={this.props.transID}/>
                                ): null}
                                </tbody>
                            </table>
                            <ReactPaginate previousLabel={'<'}
                                           nextLabel={'>'}
                                           breakLabel={<a href=''>...</a>}
                                           breakClassName={'break-me'}
                                           pageNum={this.props.errors.total / 10}
                                           marginPagesDisplayed={2}
                                           pageRangeDisplayed={10}
                                           clickCallback={this.handlePageClick}
                                           containerClassName={'pagination'}
                                           subContainerClassName={'pages pagination'}
                                           activeClassName={'active'}/>
                        </div>
                    </div>
                </div>

            );
    }
}
StatisticListError.propTypes = {
    transID: PropTypes.string.isRequired,
    searchErrorEmployeeByName: PropTypes.func,
    handlePageClick: PropTypes.func,
    perPage: PropTypes.string,
    errors: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ])
};

export default StatisticListError;
