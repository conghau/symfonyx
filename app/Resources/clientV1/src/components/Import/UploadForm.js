import React, {PropTypes} from 'react';
import AlertMessage from '../common/AlertMessage';
import {FormattedNumber, FormattedMessage, FormattedDate} from 'react-intl';

const UploadForm = ({uploadResponse}) => {
    var messageClassName = 'danger';
    var message = '';

    if (Object.keys(uploadResponse).length !== 0) {
        if (uploadResponse.code == 200) {
            messageClassName = 'success';
            message = 'Successfully!';
        } else {
            message = uploadResponse.message;
        }
    }

    return (
        <div className='portlet light bordered'>
            <div className='portlet-title'>
                <div className='caption'>
                    <i className='icon-equalizer font-red-sunglo'/>
                    <span
                        className='caption-subject font-red-sunglo bold uppercase'><FormattedMessage
                        id='upload.uploadFile'/></span>
                </div>
            </div>
            <div className='portlet-body form'>
                {(Object.keys(uploadResponse).length !== 0 ?
                    <AlertMessage message={message} messageClassName={messageClassName}/> : '')}
                <form className='form-horizontal' method='post' encType='multipart/form-data'>
                    <div className='form-group'>
                        <label className='control-label col-md-3'><FormattedMessage id='upload.file'/></label>
                        <div className='col-md-3'>
                            <div className='fileinput fileinput-new' data-provides='fileinput'>
                                <div className='input-group input-large'>
                                    <div className='form-control uneditable-input input-fixed input-medium'
                                         data-trigger='fileinput'>
                                        <i className='fa fa-file fileinput-exists'/>&nbsp;
                                        <span className='fileinput-filename'/>
                                    </div>
                                    <span className='input-group-addon btn default btn-file'>
                                        <span className='fileinput-new'> <FormattedMessage
                                            id='upload.selectFile'/> </span>
                                        <span className='fileinput-exists'> <FormattedMessage
                                            id='upload.change'/> </span>
                                        <input type='file' name='file_upload'/>
                                      </span>
                                    <a href='javascript:;' className='input-group-addon btn red fileinput-exists'
                                       data-dismiss='fileinput'> <FormattedMessage id='upload.remove'/> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='form-group'>
                        <div className='col-md-3'/>
                        <div className='col-md-3 m-l-md'>
                            <div className='checkbox checkbox-success'>
                                <input id='cbxHeader' type='checkbox' name='has_header'/>
                                <label htmlFor='cbxHeader'>&nbsp; <FormattedMessage
                                    id='upload.thisFileHasheader'/></label>
                            </div>

                        </div>
                    </div>
                    <div className='form-actions'>
                        <div className='col-md-offset-3'>
                            <button
                                className='btn btn-lg btn-primary pull-left'
                            >
                                <FormattedMessage id='upload.upload'/>
                            </button>
                        </div>
                    </div>
                    <input type='hidden' name='upload_type' value='1'/>
                </form>
            </div>
        </div>
    );
};
UploadForm.propTypes = {
    uploadResponse: PropTypes.object.isRequired
};

export default UploadForm;
