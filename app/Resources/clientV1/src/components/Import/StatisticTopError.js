import React from 'react';
import {FormattedNumber, FormattedMessage, FormattedDate} from 'react-intl';

const StatisticTopError = ({errors}) => {
    const listErrors = [];
    if (errors) {
        Object.keys(errors).map((key, index) => {
            if (errors[key] > 0) {
                listErrors.push(<li key={index}>
                    <a className='text-danger'>
            <span className='badge badge-danger pull-right'>
              {errors[key]} </span>
                        <FormattedMessage id={`summary.top_error.${key}`}/>
                    </a>
                </li>);
            }
        });
    }
    if (listErrors.length > 0) {
        return (
            <div className='portlet'>
                <div className='portlet-title'>
                    <div className='caption'>
                        <i className='fa fa-exclamation-triangle'/><FormattedMessage id='summary.top_errors'/>
                    </div>
                </div>
                <div className='portlet-body'>
                    <div className='todo-project-list'>
                        <ul className='nav nav-stacked'>
                            {listErrors}
                        </ul>
                    </div>
                </div>
            </div>
        );
    } else {
        return false;
    }
};

export default StatisticTopError;
