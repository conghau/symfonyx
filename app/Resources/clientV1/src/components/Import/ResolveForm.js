import React, {PropTypes} from 'react';
import {Link} from 'react-router';
import AlertMessage from '../common/AlertMessage';
import {FormattedNumber, FormattedMessage, FormattedDate} from 'react-intl';
import ResolveFormRow from './ResolveFormRow';

class ResolveForm extends React.Component {
    constructor(props, context) {
        super(props, context);
        const cloneEmployeeData = Object.assign({}, this.props.employeeData);
        this.state = {currentEmployeeData: cloneEmployeeData};
        this.handleTextChange = this.handleTextChange.bind(this);
        this.onSave = this.onSave.bind(this);
    }

    handleTextChange(event) {
        this.state.currentEmployeeData[event.target.name] = event.target.value;
    }

    onSave(event) {
        event.preventDefault();
        this.props.onSave(this.state.currentEmployeeData);
    }

    render() {
        var messageClassName = 'danger';
        var message = '';
        const error = [];
        if (this.props.errors) {
            Object.keys(this.props.errors).map((key, index) => {
                error.push(<li key={index}>{this.props.errors[key]}</li>);
            });
        }
        var backUrl = `/employee/import/mapping/statistic/${this.props.transactionId}`;
        return (
            <div>
                <div className='portlet-body form'>
                    {(Object.keys(uploadResponse).length !== 0 ?
                        <AlertMessage message={message} messageClassName={messageClassName}/> : '')}
                    <form className='form-horizontal' method='post'>

                        {this.props.mappings.map(mapping =>
                            <ResolveFormRow
                                key={mapping.text}
                                fieldId={mapping.value}
                                fieldData={this.props.employeeData[mapping.text]}
                                fieldName={mapping.text}
                                onChange={this.handleTextChange}
                                error={this.props.errors[mapping.text]}
                            />
                        )}
                        <div className='form-group row'>
                            <Link className='btn btn-lg btn-default pull-left'
                                  to={backUrl}><FormattedMessage id='mapping.back'/></Link>
                            <button
                                className='btn btn-lg btn-primary pull-left'
                                disabled={this.props.saving}
                                onClick={this.onSave}>{this.props.saving ? <FormattedMessage id='mapping.saving'/> :
                                <FormattedMessage id='mapping.save'/>}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

ResolveForm.propTypes = {
    employeeData: PropTypes.object.isRequired,
    mappings: PropTypes.array.isRequired,
    transactionId: PropTypes.string.isRequired,
    onSave: PropTypes.func,
    saving: PropTypes.bool,
    errors: PropTypes.object
};

export default ResolveForm;
