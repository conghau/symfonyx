import React, {PropTypes} from 'react';
import UploadLatestListRow from './UploadLatestListRow';
import AlertMessage from '../common/AlertMessage';
import LoadingDots from '../common/LoadingDots';
import constants from './constants';
import {FormattedNumber, FormattedMessage, FormattedDate} from 'react-intl';

class UploadLatestList extends React.Component {
    render() {
        return (
            <div className='portlet light bordered'>
                <div className='portlet-title'>
                    <div className='caption'>
                        <i className='icon-equalizer font-red-sunglo'/>
                        <span
                            className='caption-subject font-red-sunglo bold uppercase'><FormattedMessage
                            id='upload.list.uploadLatestList'/></span>
                    </div>
                </div>

                <div className='portlet-body'>
                    {(Object.keys(this.props.uploadLatestList).length > 0 ?
                            <div className='table-responsive'>
                                <table className='table table-bordered'>
                                    <thead>
                                    <tr>
                                        <th><FormattedMessage id='upload.list.id'/></th>
                                        <th><FormattedMessage id='upload.list.fileName'/></th>
                                        <th><FormattedMessage id='upload.list.status'/></th>
                                        <th><FormattedMessage id='upload.list.date'/></th>
                                        <th><FormattedMessage id='upload.list.action'/></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {Object.keys(this.props.uploadLatestList).map((row, index) =>
                                        <UploadLatestListRow
                                            key={index}
                                            row={this.props.uploadLatestList[row]}
                                            uploadTransactionStatus={constants.uploadTransactionStatus}
                                        />
                                    )}
                                    </tbody>
                                </table>
                            </div> : <LoadingDots />
                    )}
                </div>
            </div>
        );
    }
}

UploadLatestList.propTypes = {
    uploadLatestList: PropTypes.array.isRequired,
    content: PropTypes.object,
    errors: PropTypes.object
};

export default UploadLatestList;
