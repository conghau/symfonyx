/* global
 iCareLocale
 */
import React, {PropTypes} from 'react';
import DatePicker from 'react-datepicker';
import constants from './constants';
import Select from '../common/Select';
import Textbox from '../common/Textbox';
import moment from 'moment';
import {FormattedMessage} from 'react-intl';

import 'react-datepicker/dist/react-datepicker.css';

class ResolveFormRow extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.state = {fieldName: this.props.fieldName, fieldData: this.props.fieldData};
        this.handleTextChange = this.handleTextChange.bind(this);
        this.onDateChange = this.onDateChange.bind(this);
    }
    componentWillReceiveProps(nextProps) {
        if( nextProps.fieldData != this.state.fieldData){
            this.setState({fieldData: nextProps.fieldData});
        }
    }
    handleTextChange(event) {
        this.props.onChange(event);
        this.setState({fieldData: event.target.value});
    }

    onDateChange(date) {
        const data = {'target': {'value': date.format('YYYY-MM-DD'), 'name': this.props.fieldName}};
        this.props.onChange(data);
        this.setState({fieldData: date.format('YYYY-MM-DD')});
    }

    render() {
        const hasError = (this.props.error);
        const mappingTypes = constants.mappingTypes;
        const inputType = mappingTypes[this.props.fieldName];
        const dataType = inputType.data;

        const error = hasError ? <FormattedMessage id={`summary.top_error.${this.props.error}`}/> : '';
        const classErrorMessage = hasError ? 'col-xs-6 has-error' : 'col-xs-6';
        const messageId = `employeeData.${this.props.fieldName}`;
        const message = iCareLocale[messageId];
        let formComponent = ``;
        const placeHolder = `Please enter ${message}`;
        const defaultOption = `Please select ${message}`;
        const selectedDate = (this.state.fieldData) ? moment(`${this.state.fieldData}`, 'YYYY-MM-DD') : null;
        switch (inputType.type) {
            case 'text':
                if (inputType.validation.date) {
                    formComponent =
                        <DatePicker
                            dateFormat='YYYY-MM-DD'
                            selected={selectedDate}
                            onChange={this.onDateChange}
                            tabIndex={this.props.fieldId}
                        />;
                } else {
                    formComponent =
                        <Textbox
                            name={this.props.fieldName}
                            onChange={this.handleTextChange}
                            id={this.props.fieldName}
                            value={this.state.fieldData}
                            tabindex={this.props.fieldId}
                            placeholder={placeHolder}
                            className='form-control'
                        />;
                }
                break;
            case 'select':
                formComponent =
                    <Select
                        name={this.props.fieldName}
                        onChange={this.handleTextChange}
                        value={this.state.fieldData}
                        options={dataType}
                        defaultOption={defaultOption}
                        tabindex={this.props.fieldId}
                    />;
                break;
            default:
                formComponent =
                    <Textbox
                        name={this.props.fieldName}
                        onChange={this.handleTextChange}
                        id={this.props.fieldName}
                        value={this.state.fieldData}
                        tabindex={this.props.fieldId}
                        placeholder={placeHolder}
                        className='form-control'
                    />;
                break;
        }

        return (
            <div className='form-group row'>
                <label htmlFor={this.props.fieldName} className='col-xs-3 col-form-label'>
                    <FormattedMessage id={messageId}/>
                </label>
                <div className={classErrorMessage}>
                    {formComponent}
                    <span className='help-block'>{error}</span>
                </div>
            </div>
        );
    }

}
;
ResolveFormRow.propTypes = {
    fieldData: PropTypes.string,
    fieldName: PropTypes.string,
    fieldId: PropTypes.number,
    error: PropTypes.string,
    onChange: PropTypes.func
};
export default ResolveFormRow;
