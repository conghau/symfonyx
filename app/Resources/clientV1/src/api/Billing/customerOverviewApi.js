import $ from 'jquery';

class customerOverviewApi {
    static getStatistics() {
        return new Promise((resolve, reject) => {
            $.ajax({
                method: "GET",
                url: "/billing/api/customer-overview/statistic",
                dataType: "json"
            }).done(function (res) {
                resolve({statistics: res.data});
            }).fail(function () {
                reject('Cannot load billing customer statistic');
            });
        });
    }

    static getLatestBillingInfo() {
        return new Promise((resolve, reject) => {
            $.ajax({
                method: "GET",
                url: "/billing/api/customer-overview/latest-billing-info",
                dataType: "json"
            }).done(function (res) {
                resolve({latestBillingInfo: res.data})
            }).fail(function () {
                reject('Cannot load latest billing information');
            });
        });
    }

    static getBillingList(params={}) {
        var params = Object.keys(params).map(function(k) {
            return encodeURIComponent(k) + '=' + encodeURIComponent(params[k])
        }).join('&');

        var url = "/billing/api/customer-overview/billing-list?" + params;

        return new Promise((resolve, reject) => {
            $.ajax({
                method: "GET",
                url: url,
                dataType: "json"
            }).done(function (res) {
                resolve({billingList: res})
            }).fail(function () {
                reject('Cannot load billing list');
            });
        });
    }
}

export default customerOverviewApi;
