import $ from 'jquery';

class adminOverviewApi {
    static getStatistics(period = '0') {
        let strPeriod = '';
        if (period != '0') {
            strPeriod = `?period=${period}`;
        }
        return new Promise((resolve, reject) => {
            $.ajax({
                method: "GET",
                url: `/billing/api/admin-overview/statistic${strPeriod}`,
                dataType: "json"
            }).done(function (res) {
                resolve(res.data);
            }).fail(function () {
                reject('Can not load billing customer statistic');
            });
        });
    }

    static getCustomers(urlConditions='') {

        return new Promise((resolve, reject) => {
            $.ajax({
                method: "GET",
                url: `/billing/api/admin-overview/customers?${urlConditions}`,
                dataType: "json"
            }).done(function (res) {
                resolve(res.data);
            }).fail(function () {
                reject('Can not load billing customer statistic');
            });
        });
    }
}

export default adminOverviewApi;
