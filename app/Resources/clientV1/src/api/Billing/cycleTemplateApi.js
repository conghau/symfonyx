import $ from 'jquery';

class CycleTemplateApi {

  static getBillingCycleTemplates () {
    return new Promise((resolve, reject) => {
      $.ajax({
        method: 'GET',
        url: '/billing/api/cycleTemplateList',
        dataType: 'json'
      }).done(function (res) {
        resolve(res.cycleTemplate.data);
      }).fail(function () {
        reject('Can not load billing cycle template');
      });
    }
        );
  }
  static updateBillingCycleTemplate (data) {
    return new Promise((resolve, reject) => {
      $.ajax({
        method: 'POST',
        url: '/billing/api/cycleTemplateList/update',
        data: data,
        dataType: 'json'
      }).done(function (res) {
        if (res.data.errors != undefined && Object.keys(res.data.errors).length > 0) {
          reject(res.data.errors);
        } else {
          resolve(res);
        }
      }).fail(function () {
        reject('Can not save billing cycle template');
      }
            );
    }
        );
  }
}

export default CycleTemplateApi;
