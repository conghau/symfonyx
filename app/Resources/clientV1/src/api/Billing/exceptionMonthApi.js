import $ from 'jquery';

class ExceptionMonthApi {

  static getExceptionMonths () {
    return new Promise((resolve, reject) => {
      $.ajax({
        method: 'GET',
        url: '/billing/api/exceptionMonthList',
        dataType: 'json'
      }).done(function (res) {
        resolve({ data: res.exceptionMonth.data, hasEditRole: res.hasEditRole });
                    // resolve(res.exceptionMonth.data);
      }).fail(function () {
        reject('Can not load billing exception month');
      });
    }
        );
  }
  static updateExceptionMonth (data) {
    return new Promise((resolve, reject) => {
      $.ajax({
        method: 'POST',
        url: '/billing/api/exceptionMonthList/update',
        data: data,
        dataType: 'json'
      }).done(function (res) {
        if (res.exceptionMonth.errors != undefined && Object.keys(res.exceptionMonth.errors).length > 0) {
          reject(res.data.errors);
        } else {
          resolve({ data: res.exceptionMonth.data, hasEditRole: res.hasEditRole });
        }
      }).fail(function () {
        reject('Can not save billing exception month');
      }
            );
    }
        );
  }
}

export default ExceptionMonthApi;
