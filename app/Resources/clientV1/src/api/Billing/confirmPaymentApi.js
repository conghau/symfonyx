import $ from 'jquery';

class ConfirmPaymentApi {
    static getBillingPaymentInfo(billingId, paymentId) {
        return $.ajax({
            method: 'GET',
            url: `/billing/api/billings/${billingId}/payments/${paymentId}`,
            dataType: 'json'
        });
    }

    static confirmPayment(billingId, paymentId, data) {
        return new Promise((resolve, reject) => {
                $.ajax({
                    method: 'POST',
                    url: `/billing/api/billings/${billingId}/payments/${paymentId}/confirm`,
                    data: {data: data},
                    dataType: 'json'
                }).done(function (res) {
                    resolve(res);
                }).fail(function () {
                    reject('Can not approve the bill');
                });
            }
        );
    }
}

export default ConfirmPaymentApi;
