import $ from 'jquery';

class SettingApi {
    static getBillingInfo(billingId) {
        let url = '/billing/api/getBillingInfo';
        if (billingId != '') {
            url += '?id=' + billingId;
        }
        return $.ajax({
            method: 'GET',
            url: url,
            dataType: 'json'
        });
    }

    static saveRejectClient(billingId ,data, client) {
        return new Promise((resolve, reject) => {
                $.ajax({
                    method: 'POST',
                    url: '/billing/api/saveRejectClient',
                    data: {
                        data: data,
                        client: client,
                        icare_member_id: client.iCareMemberId,
                        billing_id: billingId
                    },
                    dataType: 'json'
                }).done(function (res) {
                    if (res.errors != undefined) {
                        reject(res.errors);
                    } else {
                        resolve(res);
                    }
                }).fail(function () {
                    reject('Can not save mappings');
                });
            }
        );
    }

    static approveBill(billApprovalId, data) {
        return new Promise((resolve, reject) => {
                $.ajax({
                    method: 'POST',
                    url: '/billing/api/approveBill/' + billApprovalId,
                    data: {data: data},
                    dataType: 'json'
                }).done(function (res) {
                    resolve(res);
                }).fail(function () {
                    reject('Can not approve the bill');
                });
            }
        );
    }
    static reopenBill(id) {
        return new Promise((resolve, reject) => {
                $.ajax({
                    method: 'POST',
                    url: '/billing/api/reopenBill/' + id,
                    dataType: 'json'
                }).done(function (res) {
                    resolve(res);
                }).fail(function () {
                    reject('Can not reopen the bill');
                });
            }
        );
    }
    static voidBill(id) {
        return new Promise((resolve, reject) => {
                $.ajax({
                    method: 'POST',
                    url: '/billing/api/voidBill/' + id,
                    dataType: 'json'
                }).done(function (res) {
                    resolve(res);
                }).fail(function () {
                    reject('Can not void the bill');
                });
            }
        );
    }

}

export default SettingApi;
