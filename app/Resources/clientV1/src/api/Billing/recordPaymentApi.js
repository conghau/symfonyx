import $ from 'jquery';

class BillingApi {

    static getBillingApproval(billApprovalId) {
        return new Promise((resolve, reject) => {
                $.ajax({
                    method: "GET",
                    url: "/billing/api/billingApproval/" + billApprovalId,
                    dataType: "json"
                }).done(function (res) {
                    resolve({data: res.data});
                }).fail(function () {
                    reject('Can not load billing exception month');
                });
            }
        );
    }
    static recordBillingPayment (data) {
        return new Promise((resolve, reject) => {
            $.ajax({
                method: 'POST',
                url: '/billing/api/payment/record',
                data: {recordPayment:data},
                dataType: 'json'
            }).done(function (res) {
                resolve(res);
                // if (res.data.errors != undefined && Object.keys(res.data.errors).length > 0) {
                //     resolve(res);
                // } else {
                //     resolve(res);
                //}
            }).fail(function () {
                    reject('Can not save billing cycle template');
                }
            );
        }
        );
    }
}

export default BillingApi;
