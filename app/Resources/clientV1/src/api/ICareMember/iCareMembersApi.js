import $ from 'jquery';

class iCareMembersApi {
    static searchICareMembers(term) {
        return $.ajax({
            method: 'GET',
            url: `/icare/employee/api/list`,
            dataType: 'json'
        }).
        done(function (res) {
            return res;
        });
    }

    // static updatePersonalInfo(iCareMemberId, personalInfo) {
    //     return new Promise((resolve, reject) => {
    //             $.ajax({
    //                 method: "POST",
    //                 url: `/employee/api/icaremembers/${iCareMemberId}/updatePersonalInfo`,
    //                 data: {data: personalInfo},
    //                 dataType: "json"
    //             }).done(function (res) {
    //                 if (res.errors != undefined) {
    //                     reject(res)
    //                 } else {
    //                     resolve(res);
    //                 }
    //             }).fail(function () {
    //                 reject('Can not update the personal information');
    //             });
    //         }
    //     );
    // }
}
export default iCareMembersApi;
