import $ from 'jquery';

class iCareMemberApi {
  static searchICareMember (term) {
    return $.ajax({
      method: 'GET',
      url: `/employee/api/find-icaremember-by-social-id?id=${term}`,
      dataType: 'json'
    });
  }

  static updatePersonalInfo(iCareMemberId, personalInfo) {
    return new Promise((resolve, reject) => {
          $.ajax({
            method: "POST",
            url: `/employee/api/icaremembers/${iCareMemberId}/updatePersonalInfo`,
            data: {data: personalInfo},
            dataType: "json"
          }).done(function (res) {
            if (res.errors != undefined) {
              reject(res)
            } else {
              resolve(res);
            }
          }).fail(function () {
            reject('Can not update the personal information');
          });
        }
    );
  }
}
export default iCareMemberApi;
