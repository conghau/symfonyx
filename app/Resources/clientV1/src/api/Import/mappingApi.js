import $ from 'jquery';

// This file mocks a web API by working with the hard-coded data below.
// It uses setTimeout to simulate the delay of an AJAX call.
// All calls return promises.
const mappings = [
  {
    id: '1',
    name: 'full_name'
  },
  {
    id: '2',
    name: 'employee_id'
  },
  {
    id: '3',
    name: 'social_id'
  },
  {
    id: '4',
    name: 'phone'
  }
];

class MappingApi {
  static getAllMappings (uploadTransId) {
    return $.ajax({
      method: 'GET',
      url: '/employee/api/import/mapping/getListMappings?id=' + uploadTransId,
      dataType: 'json'
    });
  }

  static saveMappings (uploadTransId, updateMappings) {
    return new Promise((resolve, reject) => {
      $.ajax({
        method: 'POST',
        url: '/employee/api/import/mapping/saveListMappings/' + uploadTransId,
        data: { mappings: updateMappings },
        dataType: 'json'
      }).done(function (res) {
        if (res.errors != undefined) {
          reject(res.errors);
        } else {
          resolve(res);
        }
      }).fail(function () {
        reject('Can not save mappings');
      });
    }
        );
  }

  static getValidationResults (uploadTransId) {
    return new Promise((resolve, reject) => {
      $.ajax({
        method: 'GET',
        url: '/employee/api/import/mapping/validateListMappings/' + uploadTransId,
        dataType: 'json'
      }).done(function (res) {
        resolve({ content: res.content, errors: res.errors });
      }).fail(function () {
        reject('Can not save mappings');
      });
    }
        );
  }

  static startImport (uploadTransId) {
    return new Promise((resolve, reject) => {
      $.ajax({
        method: 'POST',
        url: '/employee/api/import/mapping/startImport/' + uploadTransId,
        dataType: 'json'
      }).done(function (res) {
        if (res.status == 200) {
          resolve(res);
        } else {
          reject(res);
        }
      }).fail(function () {
        reject('Can not save mappings');
      });
    }
        );
  }

  static getProcessingResults (uploadTransId) {
    return new Promise((resolve, reject) => {
      $.ajax({
        method: 'GET',
        url: '/employee/api/import/mapping/getProcessingStatus/' + uploadTransId,
        dataType: 'json'
      }).done(function (res) {
        resolve({ statuses: res.data });
      }).fail(function () {
        reject('Can not save mappings');
      });
    }
        );
  }

}

export default MappingApi;
