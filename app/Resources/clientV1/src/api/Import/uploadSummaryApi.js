import $ from 'jquery';

class UploadSummaryApi {
  static getUploadSummary (uploadTransId) {
    return new Promise((resolve, reject) => {
      $.ajax({
        method: 'GET',
        url: '/employee/api/import/uploadSummary/' + uploadTransId,
        dataType: 'json'
      }).done(function (res) {
        resolve({ topErrors: res.data['top_errors'], listErrorRecords: res.data['list_error_records'] });
      }).fail(function () {
        reject('Can not save mappings');
      });
    }
        );
  }

  static getUploadSummaryListErrors (uploadTransId, page) {
    return new Promise((resolve, reject) => {
      $.ajax({
        method: 'GET',
        url: '/employee/api/import/uploadSummary/listErrors/' + uploadTransId + '/page/' + page,
        dataType: 'json'
      }).done(function (res) {
        resolve({ listErrorRecords: res.data });
      }).fail(function () {
        reject('Can not save mappings');
      });
    }
        );
  }

  static getErrorEmployeesByName(uploadTransId, term, page) {
    return new Promise((resolve, reject) => {
          $.ajax({
            method: 'GET',
            url: '/employee/api/import/search/errorEmployee/' + uploadTransId + '/page/' + page + `?term=${term}`,
            dataType: 'json'
          }).done(function (res) {
            resolve({ listErrorRecords: res.data });
          }).fail(function () {
            reject('Can not save mappings');
          });
        }
    );
  }
}

export default UploadSummaryApi;
