import $ from 'jquery';

// This file mocks a web API by working with the hard-coded data below.
// It uses setTimeout to simulate the delay of an AJAX call.
// All calls return promises.

class ResolveApi {
  static rawEmployeeData (uploadTransId, id) {
    return $.ajax({
      method: 'GET',
      url: '/employee/api/import/mapping/loadEmployee/' + uploadTransId + '/' + id,
      dataType: 'json'
    });
  }

  static updateEmployeeData (uploadTransId, id, data) {
    return new Promise((resolve, reject) => {
      $.ajax({
        method: 'POST',
        url: '/employee/api/import/mapping/updateEmployee/' + uploadTransId + '/' + id,
        data: data,
        dataType: 'json'
      }).done(function (res) {
        if (res.data.errors != undefined && Object.keys(res.data.errors).length > 0) {
          reject(res);
        } else {
          resolve(res);
        }
      }).fail(function () {
        reject('Can not save mappings');
      }
                );
    }
        );
  }

}

export default ResolveApi;
