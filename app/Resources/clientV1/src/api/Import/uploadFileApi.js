import $ from 'jquery';

// This file mocks a web API by working with the hard-coded data below.
// It uses setTimeout to simulate the delay of an AJAX call.
// All calls return promises.

class UploadFileApi {
  static uploadFile () {
    return $.ajax({
      method: 'GET',
      url: '/employee/api/import/mapping/getListMappings',
      dataType: 'json'
    });
  }

  static getUploadLatestList () {
    return $.ajax({
      method: 'GET',
      url: '/employee/api/import/uploadLatestList',
      dataType: 'json'
    });
  }
}

export default UploadFileApi;
