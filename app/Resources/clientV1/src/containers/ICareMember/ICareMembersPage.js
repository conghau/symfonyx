import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as iCareMemberActions from '../../actions/ICareMember/iCareMembersActions';
// import SearchBar from '../../components/ICareMember/SearchBar';
import {Fade, Alert} from 'react-bootstrap';
import {FormattedMessage} from 'react-intl';
// require('react-data-grid');
// import {ReduxTable} from 'react-data-grid';

class ICareMembersPage extends React.Component {
    constructor(props, context) {
        // debugger;
        super(props, context);
        // this.searchICareMembers(this);
        // this.state = {isShowMessage: false};
        // this.onSearch = this.onSearch.bind(this);
        // this.onSavePersonalInfo = this.onSavePersonalInfo.bind(this);
        // this.onShowMessage = this.onShowMessage.bind(this);
        this.onSearch(this);
    }

    // componentWillReceiveProps(nextProps) {
    //     const message = nextProps.message || {};
    //     if (message.hasOwnProperty('type') && message.hasOwnProperty('value')) {
    //         this.setState({isShowMessage: true});
    //     }
    // }
    //
    onSearch(term) {
        this.props.actions.searchICareMembers(term);
    }
    //
    // onSavePersonalInfo(data) {
    //     this.props.actions.updatePersonalInfo(this.props.id, data);
    // }
    //
    // onShowMessage() {
    //     setTimeout(function () {
    //         this.setState({isShowMessage: !this.state.isShowMessage});
    //     }.bind(this), 3000);
    // }
    render() {
        return (
            <div className="portlet light">
                <div className="portlet-body">
                    <table className="table table-border">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.props.listMembers && this.props.listMembers.map((item,i)=>
                               <tr>
                                   <td>{i}</td>
                                   <td>{item.last_name} {item.first_name}</td>
                                   <td>{item.status.toString()}</td>
                               </tr>
                            )}
                        </tbody>
                    </table>

                </div>
            </div>
        );
        // return(
        //     <ReactDataGrid
        //         enableCellSelect={true}
        //         columns={columns}
        //         rowGetter={this.rowGetter}
        //         rowsCount={this.state.rows.length}
        //         minHeight={500}
        //         onRowUpdated={this.handleRowUpdated} />
        // )
    }
}

ICareMembersPage.propTypes = {
    actions: PropTypes.object,
    data: PropTypes.object,
    errors: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ]),
    message: PropTypes.shape({
        type: PropTypes.string,
        value: PropTypes.string
    }),
    listMembers : PropTypes.array,
};


function mapStateToProps(state, ownProps) {
    let message = state.iCareMembers.message || {};
    if (!message.hasOwnProperty('type') || !message.hasOwnProperty('value')) {
        if (typeof state.iCareMembers.errors === 'string') {
            message = {type: 'danger', value: state.iCareMembers.errors}
        }
    }
    return {
        data: state.iCareMembers.data,
        listMembers: state.iCareMembers.listMembers,
        errors: state.iCareMembers.errors,
        message
    };
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(iCareMemberActions, dispatch)
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(ICareMembersPage);
