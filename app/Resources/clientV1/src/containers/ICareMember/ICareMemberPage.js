import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as iCareMemberActions from '../../actions/ICareMember/iCareMemberActions';
import SearchBar from '../../components/ICareMember/SearchBar';
import PersonalInfo from '../../components/ICareMember/PersonalInfo';
import {Fade, Alert} from 'react-bootstrap';
import {FormattedMessage} from 'react-intl';

class ICareMemberPage extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {isShowMessage: false};
        this.onSearch = this.onSearch.bind(this);
        this.onSavePersonalInfo = this.onSavePersonalInfo.bind(this);
        this.onShowMessage = this.onShowMessage.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        const message = nextProps.message || {};
        if (message.hasOwnProperty('type') && message.hasOwnProperty('value')) {
            this.setState({isShowMessage: true});
        }
    }

    onSearch(term) {
        this.props.actions.searchICareMember(term);
    }

    onSavePersonalInfo(data) {
        this.props.actions.updatePersonalInfo(this.props.id, data);
    }

    onShowMessage() {
        setTimeout(function () {
            this.setState({isShowMessage: !this.state.isShowMessage});
        }.bind(this), 3000);
    }

    render() {
        let isDisable = this.props.id == 0;
        return (
            <div className="portlet light">
                <div className="portlet-body">
                    { this.state.isShowMessage ?
                        <Fade in={this.state.isShowMessage} transitionAppear={true} onEntered={this.onShowMessage}>
                            <Alert bsStyle={this.props.message.type}><FormattedMessage id={'personal_info.messages.' + this.props.message.value} tagName="strong"/></Alert>
                        </Fade> :
                        <span/>
                    }
                    <div className="tabbable-custom ">
                        <ul className="nav nav-tabs ">
                            <li className="active">
                                <a href="#personal_info_tab" data-toggle="tab"> <FormattedMessage id="personal_info.title" /></a>
                            </li>
                            <SearchBar 
                                onSearch={this.onSearch}
                                placeHolderText="personal_info.search_by_social_id"
                            />
                        </ul>
                        <div className="tab-content">
                            <div className="tab-pane active" id="personal_info_tab">
                                <PersonalInfo personalInfo={this.props.personalInfo} onSave={this.onSavePersonalInfo}
                                              isDisable={isDisable} errors={this.props.errors}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

ICareMemberPage.propTypes = {
    actions: PropTypes.object,
    id: PropTypes.number,
    personalInfo: PropTypes.object,
    errors: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ]),
    message: PropTypes.shape({
        type: PropTypes.string,
        value: PropTypes.string
    }),
};

function mapStateToProps(state, ownProps) {
    let message = state.iCareMember.message || {};
    if (!message.hasOwnProperty('type') || !message.hasOwnProperty('value')) {
        if (typeof state.iCareMember.errors === 'string') {
            message = {type: 'danger', value: state.iCareMember.errors}
        }
    }
    return {
        id: state.iCareMember.id,
        personalInfo: state.iCareMember.personalInfo,
        errors: state.iCareMember.errors,
        message
    };
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(iCareMemberActions, dispatch)
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(ICareMemberPage);
