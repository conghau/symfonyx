import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import moment from 'moment';
import {FormattedMessage} from 'react-intl';
import LoadingDots from '../../components/common/LoadingDots';
import billingConstants from '../../components/Billing/constants';
import * as settingActions from '../../actions/Billing/settingActions';
import BillingOverview from '../../components/Billing/BillingOverview';
import BillingDetail from '../../components/Billing/BillingDetail';
import BillingDetailAction from '../../components/Billing/BillingDetailAction';
import BillingExclude from '../../components/Billing/BillingExclude';
import BillingExcludeForm from '../../components/Billing/BillingExcludeForm';

class DetailPage extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.changePaymentAmount = this.changePaymentAmount.bind(this);
        this.handleExcludeClient = this.handleExcludeClient.bind(this);
        this.cancelRejectClient = this.cancelRejectClient.bind(this);
        this.saveRejectClient = this.saveRejectClient.bind(this);
        this.updateRejectClient = this.updateRejectClient.bind(this);
        this.approveBill = this.approveBill.bind(this);
        this.reopenBill = this.reopenBill.bind(this);
        this.voidBill = this.voidBill.bind(this);
    }

    componentWillMount() {
        this.props.actions.loadBillingInfo(this.props.billingId);
    }

    changePaymentAmount(client, paymentChangeAmount) {
        let amount = parseInt(paymentChangeAmount);
        amount = isNaN(amount) ? 0 : amount;
        this.props.actions.changePaymentAmount(client, amount);
    }

    // Click exclude button  on detail page
    handleExcludeClient(client) {
        this.props.actions.rejectClient(null, client);
    }

    cancelRejectClient(client) {
        this.props.actions.cancelRejectClient(client);
    }

    saveRejectClient(data, client) {
        const newClient = Object.assign({}, client);
        this.props.actions.saveRejectClient(this.props.billingId, data, newClient, this.props.totalPayment).then(() => {
            this.cancelRejectClient(newClient);
        });
    }

    updateRejectClient(data, client) {
        this.props.actions.rejectClient(data, client);
    }

    approveBill() {
        const customers = this.props.customers;
        const exclude = this.props.exclude;
        const id = this.props.billingApproval.id;
        let data = {totalPayment: this.props.totalPayment, clients: [], exclude: this.props.exclude};
        if (this.props.detail.length > 0) {
            this.props.detail.map((client, index) => {
                if (!exclude[client['clientExternalId']]) {
                    const newClient = Object.assign({}, client);
                    if (customers[client['clientExternalId']]) {
                        newClient.iCareMemberId = customers[client['clientExternalId']];
                    }

                    if (!client.hasOwnProperty('outstandingAmount')) {
                        newClient.outstandingAmount = client.previousOutstandingAmount + client.principalDueAmount - client.paidAmount;
                    }
                    data.clients.push(newClient);
                }
            });
        }
        this.props.actions.approveBill(id, data);
    }

    reopenBill() {
        const id = this.props.billingApproval.id;
        this.props.actions.reopenBill(id);
    }

    voidBill() {
        const id = this.props.billingApproval.id;
        this.props.actions.voidBill(id);
    }

    render() {
        const billingApproval = this.props.billingApproval;
        const translateStatus = billingConstants.billingStatus.find(status=> {
            return status.value == billingApproval.state;
        });
        if (this.props.billAvailable === true) {
            const readOnlyBill = (billingApproval.state == 'billed' || billingApproval.state == 're_opened') ? false : true;
            return (
                <div className='portlet light'>
                    <div className='portlet-title'>
                        <div className='caption'>
                            <i className='icon-equalizer font-red-sunglo'/>
                            <span className='caption-subject font-red-sunglo bold uppercase'>
                                {
                                    (Object.keys(billingApproval).length > 0) ?
                                        <FormattedMessage
                                            id='bill_setting.billing_period_with_date'
                                            values={{
                                                start_date: moment(billingApproval.start_date).format('LL'),
                                                end_date: moment(billingApproval.end_date).format('LL')
                                            }}
                                        />
                                        : <FormattedMessage id='bill_setting.billing_period'/>
                                }
                              </span>
                        </div>
                        <label className="badge badge-success">
                            {translateStatus ? <FormattedMessage id={translateStatus.text}/> : ''}
                        </label>
                        <BillingDetailAction
                            billing={billingApproval}
                            handleApproveBill={this.approveBill}
                            handleReopenBill={this.reopenBill}
                            handleVoidBill={this.voidBill}
                        />

                    </div>
                    <div className='portlet-body'>
                        {(Object.keys(this.props.billingApproval).length > 0) ?
                            <div>
                                <div className='row'>
                                    <div className='col-md-12'>
                                        <BillingOverview billingApproval={this.props.billingApproval}
                                                         totalPayment={this.props.totalPayment}
                                        />
                                    </div>
                                </div>

                                <div className='row'>
                                    <div className='col-md-12'>
                                        {this.props.detail && (Object.keys(this.props.detail).length > 0) ?
                                            <BillingDetail
                                                readOnlyBill={readOnlyBill}
                                                handleChangePaymentAmount={this.changePaymentAmount}
                                                handleExcludeClient={this.handleExcludeClient}
                                                detail={this.props.detail}
                                                customers={this.props.customers}
                                                exclude={this.props.exclude}
                                                accept={this.props.accept}
                                            /> : <span />}
                                    </div>
                                </div>

                                <div className='row'>
                                    <div className='col-md-12'>
                                        {this.props.exclude && (Object.keys(this.props.exclude).length > 0) ?
                                            <BillingExclude
                                                readOnlyBill={readOnlyBill}
                                                listClients={this.props.detail}
                                                exclude={this.props.exclude}
                                                updateRejectClient={this.updateRejectClient}
                                            /> : <span />}
                                    </div>
                                </div>
                                <div className='row'>
                                    <BillingExcludeForm
                                        client={this.props.currentClient}
                                        exclude={this.props.exclude}
                                        showModal={this.props.showModal}
                                        cancelRejectClient={this.cancelRejectClient}
                                        saveRejectClient={this.saveRejectClient}
                                    />
                                </div>
                            </div> :
                            <LoadingDots />
                        }
                    </div>
                </div>
            );
        } else {
            return (
                <FormattedMessage id='bill_setting.billing_not_available'/>
            );
        }
    }
}
DetailPage.propTypes = {
    actions: PropTypes.object,
    customers: PropTypes.object,
    detail: PropTypes.array,
    exclude: PropTypes.object,
    accept: PropTypes.object,
    billingApproval: PropTypes.object,
    totalPayment: PropTypes.number

};
DetailPage.contextTypes = {
    router: PropTypes.object
};

function mapStateToProps(state, ownProps) {
    let billingId = ownProps.params.hasOwnProperty('id') ? ownProps.params.id : '';
    let props = {
        billingId: billingId,
        billingApproval: state.billingSetting.billingApproval,
        detail: state.billingSetting.detail,
        exclude: state.billingSetting.exclude,
        accept: state.billingSetting.accept,
        customers: state.billingSetting.customers,
        billAvailable: state.billingSetting.billAvailable,
        totalPayment: state.billingSetting.totalPayment,
        showModal: state.billingSetting.showModal,
        currentClient: state.billingSetting.currentClient
    };

    if (billingId != '' && billingId != state.billingSetting.billingApproval.id) {
        props = Object.assign(props, {billingApproval: {}});
    }
    return props;
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(settingActions, dispatch)
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(DetailPage);
