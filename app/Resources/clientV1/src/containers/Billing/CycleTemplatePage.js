import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CycleTemplateList from '../../components/Billing/CycleTemplateList';
import LoadingDots from '../../components/common/LoadingDots';
import * as cycleTemplateActions from '../../actions/Billing/cycleTemplateActions';

class CycleTemplatePage extends React.Component {
  constructor (props, context) {
    super(props, context);
    this.state = {
      saving: false
    };
    this.saveCycleTemplateItem = this.saveCycleTemplateItem.bind(this);
    this.cancelCycleTemplateItem = this.cancelCycleTemplateItem.bind(this);
  }

  componentWillMount () {
    this.props.actions.getBillingCycleTemplatesAction();
  }

  saveCycleTemplateItem (data) {
    this.setState({ saving: true });
    this.props.actions.updateBillingCycleTemplateData(data).then(() => {
      this.setState({ saving: false });
    }).catch(errors => {
      this.setState({
        saving: false
      });
            // App.scrollTo($("#billing-cycle-template-error"));
    });
  }

  cancelCycleTemplateItem (data) {
    this.props.actions.cancelBillingCycleTemplateData(data);
  }

  render () {
    return (
      <div className='portlet light'>
        <div className='portlet-body'>
          <div className='row'>
            <div className='col-md-12'>
              {(this.props.cycleTemplateRecords ?
                <CycleTemplateList
                  cycleTemplate={this.props.cycleTemplateRecords}
                  errors={this.props.errors}
                  onSave={this.saveCycleTemplateItem}
                  onCancel={this.cancelCycleTemplateItem}
                                /> : <LoadingDots />)}

            </div>
          </div>

        </div>
      </div>
        );
  }
}
CycleTemplatePage.propTypes = {
  cycleTemplateRecords: PropTypes.object,
  errors: PropTypes.object
};
CycleTemplatePage.contextTypes = {
  router: PropTypes.object
};

function mapStateToProps (state) {
  return {
    cycleTemplateRecords: state.cycleTemplate,
    errors: state.cycleTemplate.errors
  };
}
function mapDispatchToProps (dispatch) {
  return {
    actions: bindActionCreators(cycleTemplateActions, dispatch)
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(CycleTemplatePage);
