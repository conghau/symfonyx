import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as billingApprovalActions from '../../actions/Billing/recordPaymentActions';
import PaymentForm from '../../components/Billing/PaymentForm';

class RecordPaymentPage extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.onSubmitPayment = this.onSubmitPayment.bind(this);
    }
    componentWillMount() {
        this.props.actions.getBillingAction(this.props.billingId);
    }
    redirectToSelect () {
        this.context.router.push(this.props.redirectUrl);
    }
    onSubmitPayment (data) {
        this.props.actions.recordBillingPaymentAction(data).then(function() {
            if (Object.keys(this.props.errors).length > 0) {
                App.scrollTo($('#record-payment-error'));
            }else{
                this.redirectToSelect();
            }
        }.bind(this));
    }

    render() {
        return (
            <div className="row">
                <PaymentForm
                    billing={this.props.billing}
                    billingID={this.props.billingId}
                    onSubmitPayment={this.onSubmitPayment}
                    data={this.props.data}
                    errors={this.props.errors}
                />
            </div>
        );
    }
}
RecordPaymentPage.propTypes = {
    billing: PropTypes.array,
    billingId: PropTypes.string.isRequired,
    data: PropTypes.object,
    errors: PropTypes.object,
    redirectUrl: PropTypes.string
};
RecordPaymentPage.contextTypes = {
    router: PropTypes.object
};

function mapStateToProps(state, ownProps) {
    const billingId = ownProps.params.id;
    return {
        billing: state.recordPayment.billing,
        data: state.recordPayment.data,
        errors: state.recordPayment.errors,
        billingId: billingId,
        redirectUrl: state.recordPayment.redirectUrl,
    };
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(billingApprovalActions, dispatch)
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(RecordPaymentPage);