import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import LoadingDots from '../../components/common/LoadingDots';
import BillingOverview from '../../components/BillingAdmin/BillingOverview';
import BillingCustomer from '../../components/BillingAdmin/BillingCustomer';

import BillingExceptionMonth from '../../components/BillingAdmin/BillingExceptionMonth';
import * as adminOverviewActions from '../../actions/Billing/adminOverviewActions';


class AdminOverviewPage extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.filterStatistic = this.filterStatistic.bind(this);
        this.changeStatusExceptionMonth = this.changeStatusExceptionMonth.bind(this);
        this.filterCustomerByName = this.filterCustomerByName.bind(this);
        this.filterCustomerByState = this.filterCustomerByState.bind(this);
        this.filterCustomerByDate = this.filterCustomerByDate.bind(this);

    }

    componentWillMount() {
        this.props.actions.loadBillingOverview();
    }

    filterStatistic(period, event) {
        debugger;
        App.blockUI({
            target: '#billing_admin_overview_block .portlet',
            animate: true
        });

        this.props.actions.loadStatistics(period).then(()=> {
            App.unblockUI('#billing_admin_overview_block .portlet');
        });
    }

    changeStatusExceptionMonth(data) {
        this.setState({saving: true});
        this.props.actions.updateExceptionMonthData(data).then(() => {
            this.setState({saving: false});
        }).catch(errors => {
            this.setState({
                saving: false
            });
        });
    }

    filterCustomerByDate(period, event) {
        App.blockUI({
            target: '#billing_admin_customer_block .portlet',
            animate: true
        });
        this.props.actions.loadCustomers(
            {
                state: this.props.customerFilter.state,
                keyword: this.props.customerFilter.keyword,
                period: period,
            }).then(()=> {
            App.unblockUI('#billing_admin_customer_block .portlet');
        });

    }
    filterCustomerByState(state, event) {
        App.blockUI({
            target: '#billing_admin_customer_block .portlet',
            animate: true
        });
        this.props.actions.loadCustomers(
            {
                period: this.props.customerFilter.period,
                keyword: this.props.customerFilter.keyword,
                state: state
            }).then(()=> {
            App.unblockUI('#billing_admin_customer_block .portlet');
        });
    }

    filterCustomerByName(keyword, event) {
        App.blockUI({
            target: '#billing_admin_customer_block .portlet',
            animate: true
        });
        this.props.actions.loadCustomers(
            {
                period: this.props.customerFilter.period,
                state: this.props.customerFilter.state,
                keyword: keyword
            }).then(()=> {
            App.unblockUI('#billing_admin_customer_block .portlet');
        });
    }

    render() {
        return (
            <div>
                {  this.props.exceptionMonth && (Object.keys(this.props.exceptionMonth).length > 0) ?
                    <div >
                        <div className="row m-b-lg" style={{display: "flex"}}>
                            <BillingOverview
                                period={this.props.statistic.period}
                                statistic={this.props.statistic}
                                filterStatistic={this.filterStatistic}
                            />
                            <BillingExceptionMonth
                                exceptionMonthRecords={this.props.exceptionMonth}
                                hasEditRole={this.props.hasEditRole}
                                onChange={this.changeStatusExceptionMonth}
                            />

                        </div>
                        <div id="billing_admin_customer_block" className="row">
                            { <BillingCustomer
                                period={this.props.customerFilter.period}
                                billings={this.props.billings}
                                statusesStatistic={this.props.statusesStatistic}
                                customers={this.props.customers}
                                filterCustomerByName={this.filterCustomerByName}
                                filterCustomerByDate={this.filterCustomerByDate}
                                filterCustomerByState={this.filterCustomerByState}
                            />
                            }

                        </div>
                    </div>
                    : <LoadingDots />
                }

            </div>
        );
    }
}
AdminOverviewPage.propTypes = {
    statistic: PropTypes.object.isRequired,
    exceptionMonth: PropTypes.object,
    hasEditRole: PropTypes.bool,
    customers: PropTypes.array,
    billings: PropTypes.array,
    customerFilter: PropTypes.object
};
AdminOverviewPage.contextTypes = {
    router: PropTypes.object
};

function mapStateToProps(state, ownProps) {
    return {
        statistic: state.adminOverview.statistic,
        exceptionMonth: state.adminOverview.exceptionMonth,
        hasEditRole: state.adminOverview.hasEditRole,
        customers: state.adminOverview.customers,
        billings: state.adminOverview.billings,
        statusesStatistic: state.adminOverview.statusesStatistic,
        customerFilter: state.adminOverview.customerFilter,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(adminOverviewActions, dispatch)
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(AdminOverviewPage);
