import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import LoadingDots from '../../components/common/LoadingDots';
import * as customerOverviewActions from '../../actions/Billing/customerOverviewActions';

import Statistic from '../../components/BillingCustomer/Statistic';
import LatestBillingInfo from '../../components/BillingCustomer/LatestBillingInfo';
import LatestBillingList from '../../components/BillingCustomer/LatestBillingList';

class CustomerOverviewPage extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.filterByState = this.filterByState.bind(this);
        this.pagingList = this.pagingList.bind(this);
    }

    componentWillMount() {
        // TODO something
        this.setState({loading: true});
        this.props.actions.loadBillingCustomerOverview().then(()=> {
            this.setState({loading: false})
        });
    }

    filterByState(event) {
        App.blockUI({
            target: '#customer_billing_list_block .portlet',
            animate: true
        });

        this.props.actions.loadBillingList(event).then(()=> {
            App.unblockUI('#customer_billing_list_block .portlet');
        });
    }

    pagingList(event) {
        App.blockUI({
            target: '#customer_billing_list_block .portlet',
            animate: true
        });

        console.log(event);
        this.props.actions.loadBillingList(event).then(()=> {
            App.unblockUI('#customer_billing_list_block .portlet');
        });
    }

    render() {
        var isLoading = (this.state == null) ? false : this.state.loading;
        if (!isLoading) {
            return (
                <span>
                    <Statistic exceptionMonths={this.props.exceptionMonths} statistics={this.props.statistics}/>
                <div className="row">
                    <LatestBillingInfo info={this.props.latestBillingInfo}/>
                </div>
                <div id="customer_billing_list_block" className="row">
                    <LatestBillingList billingList={this.props.billingList} onChange={this.filterByState}
                                       onPagingClick={this.pagingList} totalRecords={this.props.totalRecordsBillingList}/>
                </div>
            </span>
            );
        } else {
            return (<LoadingDots/>)
        }
    }
}

CustomerOverviewPage.propTypes = {
    statistics: PropTypes.object,
    exceptionMonths: PropTypes.object,
    latestBillingInfo: PropTypes.object,
    billingList: PropTypes.array,
    totalRecordsBillingList: PropTypes.string
};

function mapStateToProps(state) {
    return {
        statistics: state.customerOverview.statistics,
        exceptionMonths: state.customerOverview.exceptionMonths,
        latestBillingInfo: state.customerOverview.latestBillingInfo,
        billingList: state.customerOverview.billingList,
        totalRecordsBillingList: state.customerOverview.totalRecordsBillingList
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(customerOverviewActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomerOverviewPage);
