import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import LoadingDots from '../../components/common/LoadingDots';
import * as confirmPaymentActions from '../../actions/Billing/confirmPaymentActions';
import ConfirmPaymentForm from '../../components/Billing/ConfirmPaymentForm';

class ConfirmPaymentPage extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.onConfirmPayment = this.onConfirmPayment.bind(this);
        this.billingId = props.params.billingId;
        this.paymentId = props.params.paymentId;
        this.onGoBack = this.onGoBack.bind(this);
    }

    componentWillMount() {
        this.props.actions.loadBillingPaymentInfo(this.props.params.billingId, this.props.params.paymentId);
    }
    componentWillReceiveProps (nextProps) {
        if (nextProps.billPaymentInfo.hasOwnProperty('id')) {
            this.paymentId = nextProps.billPaymentInfo.id;
        }
    }
    onGoBack() {
        this.context.router.push('/billing/admin/overview');
    }

    onConfirmPayment(data) {
        this.props.actions.confirmPayment(this.billingId, this.paymentId, data);
    }

    render() {
        return (
            <div>
                { this.props.billPaymentAvailable ?
                    <ConfirmPaymentForm billPaymentInfo={this.props.billPaymentInfo} onConfirm={this.onConfirmPayment}
                                        onGoBack={this.onGoBack}/>
                    :
                    <LoadingDots/>
                }
            </div>
        );
    }
}
ConfirmPaymentPage.contextTypes = {
    router: PropTypes.object
};

function mapStateToProps(state) {
    return {
        billPaymentAvailable: state.confirmPayment.billPaymentAvailable,
        billPaymentInfo: state.confirmPayment.billPaymentInfo
    };
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(confirmPaymentActions, dispatch)
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(ConfirmPaymentPage);