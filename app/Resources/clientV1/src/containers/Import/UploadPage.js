import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import UploadForm from 'components/Import/UploadForm';
import UploadLatestList from 'components/Import/UploadLatestList';
import * as uploadFileAction from 'actions/Import/uploadFileActions';

class UploadPage extends React.Component {
  constructor (props, context) {
    super(props, context);
    this.state = {
      errors: {},
      uploadResponse: uploadResponse
    };
    if (Object.keys(uploadResponse).length !== 0 && uploadResponse.code == 200) {
      uploadResponse = {};
      this.redirectToSelect(this.state.uploadResponse);
    }
  }

  componentWillMount () {
    this.props.actions.loadUploadLatestList();
  }

  redirectToSelect (uploadResponse) {
    this.context.router.push('/employee/import/mapping/select/' + uploadResponse.upload_transaction_id);
  }

  render () {
    return (
      <div>
        <UploadForm uploadResponse={this.state.uploadResponse} />
        {(this.props.uploadLatestList.length > 0) ?
          <UploadLatestList errors={this.props.errors} content={this.props.content}
            uploadLatestList={this.props.uploadLatestList} /> : null}
      </div>

        );
  }
}
UploadPage.propTypes = {
  content: PropTypes.object,
  errors: PropTypes.object,
  uploadLatestList: PropTypes.array
};
UploadPage.contextTypes = {
  router: PropTypes.object.isRequired
};

function mapStateToProps (state, ownProps) {
  if (state.uploadFile) {
    return {
      errors: state.uploadFile.errors,
      content: state.uploadFile.content,
      uploadLatestList: state.uploadFile.uploadLatestList
    };
  } else {
    return {
      errors: {},
      content: {},
      uploadLatestList: []
    };
  }
}

function mapDispatchToProps (dispatch) {
  return {
    actions: bindActionCreators(uploadFileAction, dispatch)
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(UploadPage);
