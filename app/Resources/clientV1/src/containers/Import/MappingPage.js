/* global
 iCareLocale
 */
import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import MappingForm from 'components/Import/MappingForm';
import { bindActionCreators } from 'redux';
import * as mappingActions from 'actions/Import/mappingActions';
import LoadingDots from 'components/common/LoadingDots';
import $ from 'jquery';
import {FormattedNumber, FormattedMessage, FormattedDate} from 'react-intl';

class MappingPage extends React.Component {
  constructor (props, context) {
    super(props, context);
    this.state = {
      saving: false
    };
    this.updateMappingState = this.updateMappingState.bind(this);
    this.saveMappings = this.saveMappings.bind(this);
    this.redirectNext = this.redirectNext.bind(this);
    this.onUpdateMapping = this.onUpdateMapping.bind(this);
    this.onResetMapping = this.onResetMapping.bind(this);
  }

  componentWillMount () {
    this.props.actions.loadMappings(this.props.uploadTransId);
  }

  componentWillReceiveProps (nextProps) {
    if (this.props.selectedMappings !== nextProps.selectedMappings) {
      this.setState({ selectedMappings: Object.assign({}, nextProps.selectedMappings) });
    }
  }

  updateMappingState (event) {
    const field = event.target.name;
    const index = parseInt(field.replace('mapping_', ''));

    const currentSelect = $('input[name=mapping_field_' + index + ']').val();
        // clear status error
    if (this.state.errors) {
      if (this.state.errors[currentSelect] !== undefined) {
        delete this.state.errors[currentSelect];
      }
    }
    const mappings = this.props.mappings;

    const idSelected = event.target.value;
    const selectedItem = mappings.filter(mapping => mapping.value == idSelected);
    const stateSelectedMappings = this.state.selectedMappings;
    if (selectedItem.length === 0) {
      const selectedMappings = Object.assign({}, stateSelectedMappings, {
        [index]: ''
      });
      return this.setState({ selectedMappings: selectedMappings });
    } else {
      const selectedMappings = Object.assign({}, stateSelectedMappings, {
        [index]: {
          id: selectedItem[0].value,
          name: selectedItem[0].text
        }
      });
      return this.setState({ selectedMappings: selectedMappings });
    }
  }

  redirectNext () {
    this.context.router.push('/employee/import/mapping/validate/' + this.props.uploadTransId);
  }

  onUpdateMapping (event) {
    this.props.actions.updateSelectMapping(event);
  }

  onResetMapping (event) {
    this.props.actions.resetSelectMapping(event);
  }

  saveMappings (event) {
    event.preventDefault();
    this.setState({ saving: true });
    this.props.actions.saveMappings(this.props.uploadTransId, this.props.selectedMappings).then(() => {
      this.setState({ saving: false });
      this.redirectNext();
    }).catch(errors => {
      this.setState({
        saving: false,
        errors: errors
      });
      App.scrollTo($('#mapping-error'));
    });
  }

  render () {
    return (
      <div className='portlet light bordered'>
        <div className='portlet-title'>
          <div className='caption'>
            <i className='icon-equalizer font-red-sunglo' />
            <span
              className='caption-subject font-red-sunglo bold uppercase'>
              <FormattedMessage id='mapping.select_mapping' />
            </span>
          </div>
        </div>
        <div className='portlet-body form'>
          {(this.props.mappings.length > 0 ?
            <MappingForm
              selectedMappings={this.props.selectedMappings}
              allColumns={this.props.columns}
              allMappings={this.props.mappings}
              onChange={this.onUpdateMapping}
              onResetMapping={this.onResetMapping}
              onSave={this.saveMappings}
              saving={this.state.saving}
              errors={this.props.errors}
                        /> : <LoadingDots />)}
        </div>
      </div>
        );
  }
}
MappingPage.propTypes = {
  actions: PropTypes.object.isRequired,
  mappings: PropTypes.array.isRequired,
  columns: PropTypes.array.isRequired,
  uploadTransId: PropTypes.string.isRequired,
  selectedMappings: PropTypes.object,
  errors: PropTypes.object

};
MappingPage.contextTypes = {
  router: PropTypes.object
};

function mapStateToProps (state, ownProps) {
  const uploadTransId = ownProps.params.id;

  const mappingsFormattedForDropdown = state.selectMappings.mappings.map(mapping => {
    return {
      value: mapping.id,
      text: iCareLocale[`employeeData.${mapping.name}`]
    };
  });
  return {
    uploadTransId: uploadTransId,
    selectedMappings: state.selectMappings.selectedMappings,
    columns: state.selectMappings.columns,
    errors: state.selectMappings.errors,
    mappings: mappingsFormattedForDropdown
  };
}

function mapDispatchToProps (dispatch) {
  return {
    actions: bindActionCreators(mappingActions, dispatch)
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(MappingPage);
