import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as uploadSummaryActions from 'actions/Import/uploadSummaryActions';
import StatisticChart from 'components/Import/StatisticChart';
import StatisticTopError from 'components/Import/StatisticTopError';
import StatisticListError from 'components/Import/StatisticListError';

class StatisticPage extends React.Component {
  constructor (props, context) {
    super(props, context);
    this.state = {
      term: "",
      page: 1
    };
    this.handlePageClick = this.handlePageClick.bind(this);
    this.searchErrorEmployee = this.searchErrorEmployee.bind(this);
  }

  componentWillMount () {
    this.props.actions.getUploadSummary(this.props.uploadTransId);
  }

  handlePageClick (page) {
    this.setState({ page: page });
    if(this.state.term){
      this.searchErrorEmployee(this.props.uploadTransId, this.state.term, page);
    }else{
      this.props.actions.getUploadSummaryListErrors(this.props.uploadTransId, page);
    }
  }

  searchErrorEmployee(term) {
    this.setState({ term: term });
    this.props.actions.getErrorEmployeesByName(this.props.uploadTransId, term, this.state.page);
  }

  render () {
    var statuses = this.props.statuses;
    return (
      <div className='portlet light'>
        <div className='portlet-title'>
          <div className='caption'>
            <i className='icon-equalizer font-red-sunglo' />
            <span
              className='caption-subject font-red-sunglo bold uppercase'>Upload Summary</span>
          </div>
        </div>
        <div className='portlet-body'>
          <div className='row'>
            <div className='col-md-6'>
              <StatisticChart statuses={statuses} />
            </div>
            <div className='col-md-5 col-md-offset-1'>
              <StatisticTopError errors={this.props.topErrors} />
            </div>
          </div>
          <div className='row'>
            <div className='col-md-12'>
              <StatisticListError
                errors={this.props.listErrorRecords}
                handlePageClick={this.handlePageClick}
                perPage='10'
                searchErrorEmployeeByName={this.searchErrorEmployee}
                transID={this.props.uploadTransId}
              />
            </div>
          </div>

        </div>
      </div>
        );
  }
}
StatisticPage.propTypes = {
  statuses: PropTypes.object.isRequired,
  topErrors: PropTypes.object.isRequired,
  listErrorRecords: PropTypes.object.isRequired,
  uploadTransId: PropTypes.string.isRequired
};
StatisticPage.contextTypes = {
  router: PropTypes.object
};

function mapStateToProps (state, ownProps) {
  const uploadTransId = ownProps.params.id;
  return {
    statuses: state.uploadSummary.statuses,
    topErrors: state.uploadSummary.topErrors,
    listErrorRecords: state.uploadSummary.listErrorRecords,
    uploadTransId: uploadTransId
  };
}

function mapDispatchToProps (dispatch) {
  return {
    actions: bindActionCreators(uploadSummaryActions, dispatch)
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(StatisticPage);
