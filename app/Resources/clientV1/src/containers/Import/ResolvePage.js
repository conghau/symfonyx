import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ResolveForm from 'components/Import/ResolveForm';
import {FormattedNumber, FormattedMessage, FormattedDate} from 'react-intl';
import LoadingDots from 'components/common/LoadingDots';
import * as resolveActions from 'actions/Import/resolveActions';

class ResolvePage extends React.Component {
  constructor (props, context) {
    super(props, context);
    this.state = {
      saving: false
    };
    this.saveEmployee = this.saveEmployee.bind(this);
    this.redirectNext = this.redirectNext.bind(this);
  }

  componentWillMount () {
    this.props.actions.loadEmployeeData(this.props.uploadTransId, this.props.id);
    //this.props.actions.loadMappings(this.props.uploadTransId);
  }

  saveEmployee (data) {
    this.setState({ saving: true });
    this.props.actions.updateEmployeeData(this.props.uploadTransId, this.props.id, data).then(() => {
      this.setState({ saving: false });
      this.redirectNext();
    }).catch(errors => {
      this.setState({
        saving: false,
        errors: errors
      });
      App.scrollTo($('#mapping-error'));
    });
  }
  redirectNext () {
    this.context.router.push('/employee/import/mapping/statistic/' + this.props.uploadTransId);
  }

  render () {
    return (
      <div className='portlet light bordered'>
        <div className='portlet-title'>
          <div className='caption'>
            <i className='icon-equalizer font-red-sunglo' />
            <span
              className='caption-subject font-red-sunglo bold uppercase'>
              <FormattedMessage id='employeeData.employeeInformation' />
            </span>
          </div>
        </div>
        <div className='portlet-body form'>

          {((this.props.employeeData && Object.keys(this.props.employeeData).length > 0) ?
            <ResolveForm
              employeeData={this.props.employeeData}
              mappings={this.props.mappings}
              transactionId={this.props.uploadTransId}
              onSave={this.saveEmployee}
              saving={this.state.saving}
              errors={this.props.errors}
                        /> : <LoadingDots />)}
        </div>
      </div>
        );
  }
}
ResolvePage.propTypes = {
  actions: PropTypes.object.isRequired,
  employeeData: PropTypes.object.isRequired,
  uploadTransId: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  errors: PropTypes.object

};

ResolvePage.contextTypes = {
  router: PropTypes.object
};

function mapStateToProps (state, ownProps) {
  const uploadTransId = ownProps.params.transId;
  const id = ownProps.params.id;
  const mappingsFormattedForDropdown = [
    {value: 1, text: "fullName"},
    {value: 2, text: "employeeId"},
    {value: 3, text: "socialId"},
    {value: 4, text: "phone"},
    {value: 5, text: "hiringDate"},
    {value: 6, text: "jobTitle"},
    {value: 7, text: "department"},
    {value: 8, text: "gender"},
    {value: 9, text: "birthDate"},
    {value: 10, text: "email"},
    {value: 11, text: "address"},
    {value: 12, text: "salary"},
    {value: 13, text: "contractType"},
    {value: 14, text: "contractEndDate"},
    {value: 15, text: "salaryPaymentMethod"},
    {value: 16, text: "bankAccountNumber"},
    {value: 17, text: "bankName"}
  ];
  return {
    uploadTransId: uploadTransId,
    id: id,
    employeeData: state.employeeData.data,
    mappings: mappingsFormattedForDropdown,
    errors: state.employeeData.error
  };
}

function mapDispatchToProps (dispatch) {
  return {
    actions: bindActionCreators(resolveActions, dispatch)
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(ResolvePage);
