import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as processingActions from 'actions/Import/processingActions';
import ProgressBar from 'react-bootstrap/lib/ProgressBar';

class ProcessingStatisticPage extends React.Component {
  constructor (props, context) {
    super(props, context);
    this.redirectNext = this.redirectNext.bind(this);
  }

  componentWillMount () {
    this.props.actions.loadProcessingResults(this.props.uploadTransId);
    this.props.actions.listenRealTimeProcessingResults(this.props.uploadTransId);
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.statuses.processed === nextProps.statuses.total) {
      this.redirectNext();
    } else {
      setTimeout(function () {
        this.props.actions.loadProcessingResults(this.props.uploadTransId);
      }.bind(this), 8000);
    }
  }

  redirectNext () {
    this.context.router.push('/employee/import/mapping/statistic/' + this.props.uploadTransId);
  }

  render () {
    const now = parseInt((parseInt(this.props.statuses.processed) * 100) / (this.props.statuses.total));
    return (
      <div className='portlet light bordered'>
        <div className='portlet-title'>
          <div className='caption'>
            <i className='icon-equalizer font-red-sunglo' />
            <span
              className='caption-subject font-red-sunglo bold uppercase'>Import Processing</span>
          </div>
        </div>
        <div className='portlet-body form'>
          <div className='page-header'>
            <h1>Statistic
            </h1>
          </div>
          <div>
            <p>Total number of employees in the list: {this.props.statuses.total}</p>
            <ProgressBar active bsStyle='success' label={`${now}%`} now={now} />
          </div>

        </div>
      </div>
        );
  }
}

ProcessingStatisticPage.propTypes = {};
ProcessingStatisticPage.contextTypes = {
  router: PropTypes.object
};

function mapStateToProps (state, ownProps) {
  const uploadTransId = ownProps.params.id;

  return {
    statuses: state.processingResults.statuses,
    uploadTransId: uploadTransId
  };
}

function mapDispatchToProps (dispatch) {
  return {
    actions: bindActionCreators(processingActions, dispatch)
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(ProcessingStatisticPage);
