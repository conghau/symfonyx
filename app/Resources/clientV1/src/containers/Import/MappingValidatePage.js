import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as validateMappingActions from '../../actions/Import/validateMappingActions';
import MappingValidateResult from '../../components/Import/MappingValidateResult';
import LoadingDots from '../../components/common/LoadingDots';
import {FormattedMessage} from 'react-intl';

class MappingValidatePage extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            errors: {},
            saving: false
        };
        this.redirectBack = this.redirectBack.bind(this);
        this.redirectNext = this.redirectNext.bind(this);
        this.onSave = this.onSave.bind(this);
    }

    componentWillMount() {
        this.props.actions.loadValidationResults(this.props.uploadTransId);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.errors !== nextProps.errors) {
            this.setState({errors: nextProps.errors});
        }
    }

    redirectBack() {
        this.context.router.push('/employee/import/mapping/select/' + this.props.uploadTransId);
    }

    redirectNext() {
        this.context.router.push('/employee/import/mapping/processing/' + this.props.uploadTransId);
    }

    onSave() {
        this.setState({saving: true});
        this.props.actions.startImport(this.props.uploadTransId).then(() => {
            this.setState({saving: false});
            this.redirectNext();
        }).catch(errors => {
            this.setState({
                saving: false,
                errors: errors
            });
        });
        ;
    }

    render() {
        return (
            <div>
                <div className='portlet light bordered'>
                    <div className='portlet-title'>
                        <div className='caption'>
                            <i className='icon-equalizer font-red-sunglo'/>
                            <span className='caption-subject font-red-sunglo bold uppercase'>
                                <FormattedMessage id="mapping_validate.validation"/>
                            </span>
                        </div>
                    </div>
                    <div className='portlet-body form'>
                        {(Object.keys(this.props.content).length > 0 ?
                                <MappingValidateResult
                                    redirectBack={this.redirectBack}
                                    onSave={this.onSave}
                                    saving={this.state.saving}
                                    errors={this.props.errors}
                                    content={this.props.content}
                                />
                                : <LoadingDots />
                        )}
                    </div>
                </div>
            </div>
        );
    }
}
MappingValidatePage.propTypes = {
    actions: PropTypes.object.isRequired,
    content: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired,
    uploadTransId: PropTypes.string.isRequired
};
MappingValidatePage.contextTypes = {
    router: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
    const uploadTransId = ownProps.params.id;
    return {
        errors: state.validateMappings.errors,
        content: state.validateMappings.content,
        uploadTransId: uploadTransId
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(validateMappingActions, dispatch)
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(MappingValidatePage);
