import * as types from '../actionTypes';
import {beginAjaxCall, ajaxCallError} from '../ajaxStatusActions';
import iCareMemberApi from '../../api/ICareMember/iCareMemberApi';

function parsePersonalInfoFromResponse(personalInfo) {
    return {
        fullName: personalInfo.fullName,
        socialId: personalInfo.socialId,
        gender: personalInfo.gender,
        birthDate: personalInfo.birthDate,
        phone: personalInfo.phone,
        email: personalInfo.email,
        address: personalInfo.address,
        employeeId: personalInfo.employeeId,
        jobTitle: personalInfo.jobTitle,
        department: personalInfo.department,
        salary: personalInfo.salary,
        hiringDate: personalInfo.hiringDate,
        terminationDate: personalInfo.terminationDate || '',
        contractType: personalInfo.contractType,
        creditLimit: personalInfo.creditLimit,
        dueLimit: personalInfo.dueLimit,
        maternityLeaveStart: personalInfo.maternityLeaveStart || '',
        maternityLeaveEnd: personalInfo.maternityLeaveEnd || '',
        salaryPaymentMethod: personalInfo.salaryPaymentMethod,
        bankName: personalInfo.bankName,
        isActive: personalInfo.hasOwnProperty('isActive') ? personalInfo.isActive : '1'
    };
}
export function loadProfileInfoSuccess(id, personalInfo) {
    return {
        type: types.LOAD_ICAREMEMBER_PERSONAL_INFO_SUCCESS,
        payload: {
            id,
            personalInfo,
            errors: {},
            message: {}
        }
    };
}

export function loadProfileInfoError(errors, message) {
    errors = errors || {};
    message = message || {};

    return {
        type: types.LOAD_ICAREMEMBER_PERSONAL_INFO_ERROR,
        payload: {
            id: 0,
            personalInfo: {},
            errors,
            message
        }
    };
}
export function updatePersonalInfoSuccess(newPersonalInfo) {
    return {
        type: types.UPDATE_ICAREMEMBER_PERSONAL_INFO_SUCCESS,
        payload: {
            personalInfo: newPersonalInfo,
            errors: {},
            message: {
                type: 'success',
                value: 'has_been_saved'
            }
        }
    };
}
export function updatePersonalInfoError(newPersonalInfo, errors, message) {
    errors = errors || {};
    message = message || {};

    return {
        type: types.UPDATE_ICAREMEMBER_PERSONAL_INFO_ERROR,
        payload: {
            personalInfo: newPersonalInfo,
            errors,
            message
        }
    };
}
export function searchICareMember(term) {
    return function (dispatch) {
        dispatch(beginAjaxCall());
        return iCareMemberApi.searchICareMember(term)
            .then((res) => {
                if (!res.data.hasOwnProperty('id') || !res.data.hasOwnProperty('personalInfo')) {
                    dispatch(loadProfileInfoError({}, {type: 'danger', value: 'something_went_wrong'}));
                }
                if (res.data.id == 0) {
                    dispatch(loadProfileInfoError({}, {type: 'warning', value: 'not_found'}));
                } else {
                    const personalInfo = parsePersonalInfoFromResponse(res.data.personalInfo);
                    dispatch(loadProfileInfoSuccess(res.data.id, personalInfo));
                }
            })
            .fail(() => {
                dispatch(loadProfileInfoError({}, {type: 'danger', value: 'something_went_wrong'}));
            });
    };
}

export function updatePersonalInfo(iCareMemberId, personalInfo) {
    return function (dispatch) {
        dispatch(beginAjaxCall());
        return iCareMemberApi.updatePersonalInfo(iCareMemberId, personalInfo)
            .then((res) => {
                const newPersonalInfo = parsePersonalInfoFromResponse(res.data);
                dispatch(updatePersonalInfoSuccess(newPersonalInfo));
            })
            .catch((res) => {
                if (res.code == 400) {
                    dispatch(updatePersonalInfoError(personalInfo, res.errors, {}));
                } else {
                    dispatch(updatePersonalInfoError(personalInfo, {}, {type: 'danger', value: res.errors}));
                }
            });
    };
}