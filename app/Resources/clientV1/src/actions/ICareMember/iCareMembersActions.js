import * as types from '../actionTypes';
import {beginAjaxCall, ajaxCallError} from '../ajaxStatusActions';
import iCareMembersApi from '../../api/ICareMember/iCareMembersApi';

// function parsePersonalInfoFromResponse(personalInfo) {
//     return {
//         fullName: personalInfo.fullName,
//         socialId: personalInfo.socialId,
//         gender: personalInfo.gender,
//         birthDate: personalInfo.birthDate,
//         phone: personalInfo.phone,
//         email: personalInfo.email,
//         address: personalInfo.address,
//         employeeId: personalInfo.employeeId,
//         jobTitle: personalInfo.jobTitle,
//         department: personalInfo.department,
//         salary: personalInfo.salary,
//         hiringDate: personalInfo.hiringDate,
//         terminationDate: personalInfo.terminationDate || '',
//         contractType: personalInfo.contractType,
//         creditLimit: personalInfo.creditLimit,
//         dueLimit: personalInfo.dueLimit,
//         maternityLeaveStart: personalInfo.maternityLeaveStart || '',
//         maternityLeaveEnd: personalInfo.maternityLeaveEnd || '',
//         salaryPaymentMethod: personalInfo.salaryPaymentMethod,
//         bankName: personalInfo.bankName,
//         isActive: personalInfo.hasOwnProperty('isActive') ? personalInfo.isActive : '1'
//     };
// }
export function loadIcareMembersListSuccess(listMembers) {
    return {
        type: types.LOAD_ICAREMEMBERS_LIST_SUCCESS,
        payload: {
            listMembers,
            data : {},
            errors: {},
            message: {}
        }
    };
}

export function searchICareMembers(term) {
    return function (dispatch) {
        dispatch(beginAjaxCall());
        return iCareMembersApi.searchICareMembers(term)
            .then((res) => {
                dispatch(loadIcareMembersListSuccess(res.data));
            })
            // .fail(() => {
            //     dispatch(loadProfileInfoError({}, {type: 'danger', value: 'something_went_wrong'}));
            // })
            ;
    };
}

