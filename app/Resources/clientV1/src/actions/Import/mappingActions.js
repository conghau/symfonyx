import * as types from '../actionTypes';
import { beginAjaxCall, ajaxCallError } from '../ajaxStatusActions';
import mappingApi from '../../api/Import/mappingApi';

export function loadMappingSuccess (mappings) {
  return { type: types.LOAD_MAPPINGS_SUCCESS, mappings };
}

export function updateMappingsSuccess (updateMappings) {
  return { type: types.UPDATE_MAPPINGS_SUCCESS, updateMappings };
}
export function resetMappingsSuccess (resetMappings) {
  return { type: types.RESET_MAPPINGS_SUCCESS, resetMappings };
}
export function updateMappingError (errors) {
  return { type: types.UPDATE_MAPPINGS_ERROR, errors };
}

export function updateSelectMappingSuccess (selectMappings, newError) {
  return { type: types.UPDATE_ITEM_MAPPING, selectMappings, newError };
}

export function loadMappings (uploadTransId) {
  return function (dispatch) {
    dispatch(beginAjaxCall());
    return mappingApi.getAllMappings(uploadTransId).then(mappings => {
      dispatch(loadMappingSuccess(mappings));
    });
  };
}

export function saveMappings (uploadTransId, updateMappings) {
  return function (dispatch, getState) {
    dispatch(beginAjaxCall());
    return mappingApi.saveMappings(uploadTransId, updateMappings).then(updateMappings => {
      dispatch(updateMappingsSuccess(updateMappings));
    }).catch(errors => {
      dispatch(updateMappingError(errors));
      throw (errors);
    });
  };
}

export function updateSelectMapping (event) {
  return (dispatch, getState) => {
    const { selectMappings } = getState();
    const currentSelectMappings = selectMappings;
    const field = event.target.name;
    const index = parseInt(field.replace('mapping_', ''));

    const currentSelect = $('input[name=mapping_field_' + index + ']').val();

    const mappings = currentSelectMappings.mappings;
    const idSelected = event.target.value;
    const selectedItem = mappings.filter(mapping => mapping.id == idSelected);

        // clear status error
    var newError = currentSelectMappings.errors;
    if (currentSelectMappings.errors && selectedItem.length > 0) {
      newError = Object.keys(currentSelectMappings.errors).reduce((obj, key) => {
        if (key !== selectedItem[0].name) {
          return Object.assign({}, obj, { [key]: currentSelectMappings.errors[key] });
        }
        return obj;
      }, {});
    }

    const stateSelectedMappings = currentSelectMappings.selectedMappings;
    if (selectedItem.length === 0) {
      const selectedMappings = Object.assign({}, stateSelectedMappings, {
        [index]: null
      });
      dispatch(updateSelectMappingSuccess(selectedMappings, newError));
    } else {
      const selectedMappings = Object.assign({}, stateSelectedMappings, {
        [index]: {
          id: selectedItem[0].id,
          name: selectedItem[0].name
        }
      });
      dispatch(updateSelectMappingSuccess(selectedMappings, newError));
    }
  };
}

export function resetSelectMapping (event) {
  return (dispatch, getState) => {
    const { selectMappings } = getState();
    const currentSelectMappings = selectMappings;
    let stateSelectedMappings = currentSelectMappings.selectedMappings;
    stateSelectedMappings = Object.assign({}, null);
    dispatch(resetMappingsSuccess(stateSelectedMappings));
  };
}

