import * as types from '../actionTypes';
import {beginAjaxCall, ajaxCallError} from '../ajaxStatusActions';
import mappingApi from '../../api/Import/mappingApi';
import uploadSummaryApi from '../../api/Import/uploadSummaryApi';

export function loadUploadSummarySuccess(payload) {
    return {type: types.LOAD_UPLOAD_SUMMARY_SUCCESS, payload};
}

export function getUploadSummary(uploadTransId) {
    return function (dispatch) {
        dispatch(beginAjaxCall());
        const uploadSummary = uploadSummaryApi.getUploadSummary(uploadTransId);
        const processingResult = mappingApi.getProcessingResults(uploadTransId);
        return Promise.all([uploadSummary, processingResult]).then(values => {
            dispatch(loadUploadSummarySuccess(
                {
                    listErrorRecords: values[0].listErrorRecords,
                    topErrors: values[0].topErrors,
                    statuses: values[1].statuses
                }
            ));
        });
    };
}

export function loadUploadSummaryListErrorsSuccess(payload) {
    return {type: types.LOAD_UPLOAD_SUMMARY_LIST_ERRORS_SUCCESS, payload};
}

export function getUploadSummaryListErrors(uploadTransId, page) {
    return function (dispatch) {
        dispatch(beginAjaxCall());
        return uploadSummaryApi.getUploadSummaryListErrors(uploadTransId, page).then(results => {
            dispatch(loadUploadSummaryListErrorsSuccess(results));
        });
    };
}

export function getErrorEmployeesByName(uploadTransId, term, page) {
    return function (dispatch) {
        dispatch(beginAjaxCall());
        if(term.trim()!=''){
            return uploadSummaryApi.getErrorEmployeesByName(uploadTransId, term, page).then(results => {
                dispatch(loadUploadSummaryListErrorsSuccess(results));
            });
        }else{
            return uploadSummaryApi.getUploadSummaryListErrors(uploadTransId, page).then(results => {
                dispatch(loadUploadSummaryListErrorsSuccess(results));
            });
        }
    };
}

export function loadProcessingResultsSuccess(results) {
    return {type: types.LOAD_PROCESSING_SUCCESS, results};
}

export function loadProcessingResults(uploadTransId) {
    return function (dispatch) {
        dispatch(beginAjaxCall());
        return mappingApi.getProcessingResults(uploadTransId).then(results => {
            dispatch(loadProcessingResultsSuccess(results));
        });
    };
}

