import * as types from '../actionTypes';
import {beginAjaxCall, ajaxCallError} from '../ajaxStatusActions';
import resolveApi from '../../api/Import/resolveApi';
import mappingApi from '../../api/Import/mappingApi';

export function loadEmployeeDataSuccess(employeeData, employeeError) {
    return {type: types.LOAD_EMPLOYEE_DATA_SUCCESS, employeeData, employeeError};
}

export function updateEmployeeDataSuccess(a) {
    return {type: types.UPDATE_EMPLOYEE_DATA_SUCCESS, a};
}

export function updateEmployeeDataError(employeeData, employeeError) {
    return {type: types.UPDATE_EMPLOYEE_DATA_ERROR, employeeData, employeeError};
}

export function loadEmployeeData(uploadTransId, id) {
    return function (dispatch) {
        dispatch(beginAjaxCall());
        return resolveApi.rawEmployeeData(uploadTransId, id).then(results => {
            dispatch(loadEmployeeDataSuccess(results.data, results.error));
        });
    };
}

export function updateEmployeeData(uploadTransId, id, employeeData) {
    return function (dispatch) {
        dispatch(beginAjaxCall());
        return resolveApi.updateEmployeeData(uploadTransId, id, employeeData).then(response => {
            dispatch(updateEmployeeDataSuccess(response.data));
        }).catch(errors => {
            dispatch(updateEmployeeDataError(errors.data.data, errors.data.errors));
            throw (errors.data.errors);
        });
    };
}

