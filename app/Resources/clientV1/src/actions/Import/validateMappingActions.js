import * as types from '../actionTypes';
import { beginAjaxCall, ajaxCallError } from '../ajaxStatusActions';
import mappingApi from '../../api/Import/mappingApi';

export function validationResultsSuccess (results) {
  return { type: types.LOAD_VALIDATE_MAPPINGS_SUCCESS, results };
}

export function startImportSuccess (results) {
  return { type: types.START_IMPORT_SUCCESS, results };
}

export function loadValidationResults (uploadTransId) {
  return function (dispatch) {
    dispatch(beginAjaxCall());
    return mappingApi.getValidationResults(uploadTransId).then(results => {
      dispatch(validationResultsSuccess(results));
    });
  };
}

export function startImport (uploadTransId) {
  return function (dispatch, getState) {
    dispatch(beginAjaxCall());
    return mappingApi.startImport(uploadTransId).then(results => {
      dispatch(startImportSuccess(results));
    }).catch(errors => {
      throw (errors);
    });
  };
}

