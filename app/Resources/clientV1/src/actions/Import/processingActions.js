import * as types from '../actionTypes';
import { beginAjaxCall, ajaxCallError } from '../ajaxStatusActions';
import mappingApi from '../../api/Import/mappingApi';

export function loadProcessingResultsSuccess (results) {
  return { type: types.LOAD_PROCESSING_SUCCESS, results };
}

export function loadProcessingResults (uploadTransId) {
  return function (dispatch) {
    dispatch(beginAjaxCall());
    return mappingApi.getProcessingResults(uploadTransId).then(results => {
      dispatch(loadProcessingResultsSuccess(results));
    });
  };
}

export function listenRealTimeProcessingResults (uploadTransId) {
  return function (dispatch) {
    var webSocket = WS.connect(iCareWebSocketUri);
    webSocket.on('socket/connect', function (session) {
            // session is an Autobahn JS WAMP session.

      console.log('Successfully Connected!');

      session.subscribe('icare/import/' + uploadTransId, function (uri, payload) {
        console.log('Received message', payload.msg);
        dispatch(loadProcessingResultsSuccess({ statuses: payload.msg }));
                // this.state.statuses.processed = payload.msg.msg;
                // console.log("statuses", this.state.statuses);
      });
            // session.publish("icare/import", {msg: 0});
    });

    webSocket.on('socket/disconnect', function (error) {
            // error provides us with some insight into the disconnection: error.reason and error.code
      console.log('Disconnected for ' + error.reason + ' with code ' + error.code);
    });
    return mappingApi.getProcessingResults(uploadTransId).then(results => {
      dispatch(loadProcessingResultsSuccess(results));
    });
  };
}

