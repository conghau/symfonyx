import * as types from '../actionTypes';
import { beginAjaxCall } from '../ajaxStatusActions';
import uploadFileApi from '../../api/Import/uploadFileApi';

export function uploadFileSuccess (uploadFile) {
  return { type: types.UPLOAD_FILE_SUCCESS, uploadFile };
}

export function uploadFile () {
  return function (dispatch) {
    dispatch(beginAjaxCall());
    return uploadFileApi.uploadFile().then(uploadFile => {
      dispatch(uploadFileSuccess(uploadFile));
    });
  };
}

export function loadUploadLatestListSuccess (uploadLatestList) {
  return { type: types.LOAD_UPLOAD_LATEST_LIST_SUCCESS, uploadLatestList };
}

export function loadUploadLatestListError (errors) {
  return { type: types.LOAD_UPLOAD_LATEST_LIST_ERROR, errors };
}

export function loadUploadLatestList () {
  return function (dispatch) {
    dispatch(beginAjaxCall());
    return uploadFileApi.getUploadLatestList().then(results => {
      if (results.data !== undefined) {
        dispatch(loadUploadLatestListSuccess(results.data));
      } else {
        dispatch(loadUploadLatestListError(results));
      }
    });
  };
}
