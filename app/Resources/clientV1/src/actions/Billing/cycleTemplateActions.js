import * as types from '../actionTypes';
import { beginAjaxCall, ajaxCallError } from '../ajaxStatusActions';
import cycleTemplateApi from '../../api/Billing/cycleTemplateApi';

export function getBillingCycleTemplatesSuccess (cycleTemplate) {
  return { type: types.LOAD_BILLING_CYCLE_TEMPLATE_SUCCESS, cycleTemplate };
}

export function updateBillingCycleTemplateSuccess (data) {
  return { type: types.UPDATE_BILLING_CYCLE_TEMPLATE_SUCCESS, data };
}

export function cancelBillingCycleTemplateSuccess (data) {
  return { type: types.CANCEL_BILLING_CYCLE_TEMPLATE, data };
}

export function updateBillingCycleTemplateError (errors) {
  return { type: types.UPDATE_BILLING_CYCLE_TEMPLATE_ERROR, errors };
}

export function getBillingCycleTemplatesAction () {
  return function (dispatch) {
    dispatch(beginAjaxCall());
    return cycleTemplateApi.getBillingCycleTemplates().then(results => {
      dispatch(getBillingCycleTemplatesSuccess(results));
    });
  };
}

export function updateBillingCycleTemplateData (data) {
  return function (dispatch) {
    dispatch(beginAjaxCall());
    return cycleTemplateApi.updateBillingCycleTemplate(data).then(response => {
      dispatch(updateBillingCycleTemplateSuccess(response.data));
    }).catch(errors => {
      dispatch(updateBillingCycleTemplateError(errors));
      throw (errors);
    });
  };
}

export function cancelBillingCycleTemplateData (data) {
  return function (dispatch) {
    dispatch(cancelBillingCycleTemplateSuccess(data));
  };
}

