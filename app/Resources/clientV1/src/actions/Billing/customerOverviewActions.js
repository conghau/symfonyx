import * as types from '../actionTypes';
import {beginAjaxCall, ajaxCallError} from '../ajaxStatusActions';
import moment from 'moment';

import customerOverviewApi from '../../api/Billing/customerOverviewApi';
import exceptionMonthApi from '../../api/Billing/exceptionMonthApi'

export function loadBillingCustomerOverviewSuccess(payload) {
    return {type: types.LOAD_BILLING_CUSTOMER_OVERVIEW_STATISTIC_SUCCESS, payload};
}
export function loadBillingListSuccess(payload) {
    return {type: types.LOAD_BILLING_CUSTOMER_LIST_SUCCESS, payload};
}

function parseDataFromResponse(responses) {
    var statistics = responses[0].statistics;
    var exceptionMonths = parseExceptionMonths(responses[1]);
    var latestBillingInfo = responses[2].latestBillingInfo;
    var billingList = parseBillingList(responses[3].billingList);

    var totalRecordsBillingList = billingList['total'];
    delete billingList['total'];

    return {statistics, exceptionMonths, latestBillingInfo, billingList, totalRecordsBillingList};
}

function parseExceptionMonths(exceptionMonths) {
    var months = '{}';
    if (exceptionMonths.data.status == 'success') {
        months = exceptionMonths.data.data;
    }

    return months;
}

function parseBillingList(billingList) {
    let billingListObj = billingList;
    let returnList = [];
    let totalRecordsBillingList = 0;

    if (typeof billingListObj.data != 'undefined' && billingListObj.data.length > 1) {
        totalRecordsBillingList = billingListObj.data.pop();
        totalRecordsBillingList = totalRecordsBillingList.total;
        returnList = billingListObj.data.map(function (billingItem) {
            billingItem.delay = calculateBillingDelay(billingItem);
            return billingItem;
        });
    }

    returnList['total'] = totalRecordsBillingList;

    return returnList;
}

function calculateBillingDelay(billingItem) {
    var delay = '';
    var billingDueDate = moment(billingItem.dueDate);
    var currentDate = moment();

    switch (billingItem.state) {
        case 'billed':
            if (billingDueDate < currentDate.subtract(2, 'days')) {
                delay = 'Approval Delayed';
            }
            break;
        case 're_opened':
            if (billingDueDate < currentDate.subtract(2, 'days')) {
                delay = 'Payment Delayed';
            }
            break;
        case 'approved':
            if (billingDueDate.add(1, 'days') < currentDate) {
                delay = 'Payment Delayed';
            }
            break;
    }

    return delay;
}

export function loadBillingList(parameters) {
    return function (dispatch) {
        dispatch(beginAjaxCall());

        var billingList = customerOverviewApi.getBillingList(parameters);
        return Promise.all([billingList]).then(
            responses => {
                var billingList = parseBillingList(responses[0].billingList);
                var totalRecordsBillingList = billingList['total'];
                delete billingList['total'];

                var payload = {billingList, totalRecordsBillingList};
                dispatch(loadBillingListSuccess(payload))
            }
        );
    }
}

export function loadBillingCustomerOverview() {
    return function (dispatch) {
        dispatch(beginAjaxCall());

        var statistics = customerOverviewApi.getStatistics();
        var exceptionMonths = exceptionMonthApi.getExceptionMonths();
        var latestBillingInfo = customerOverviewApi.getLatestBillingInfo();
        var billingList = customerOverviewApi.getBillingList();

        return Promise.all([statistics, exceptionMonths, latestBillingInfo, billingList]).then(
            responses => {
                const payload = parseDataFromResponse(responses);
                dispatch(loadBillingCustomerOverviewSuccess(payload))
            }
        );
    };
}
