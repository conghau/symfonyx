import * as types from '../actionTypes';
import {beginAjaxCall, ajaxCallError} from '../ajaxStatusActions';
import settingApi from '../../api/Billing/settingApi';

function parseDataFromResponse(billingInfoResponse) {
    const data = billingInfoResponse.data;
    if (!data) {
        return {billAvailable: false};
    }

    const billingApproval = data.hasOwnProperty('billingApproval') ? data.billingApproval : {};
    const exclude = data.hasOwnProperty('exclude') ? data.exclude : {};
    const accept = data.hasOwnProperty('accept') ? data.accept : {};
    const detail = data.hasOwnProperty('detail') ? data.detail : [];
    const customers = data.hasOwnProperty('customers') ? data.customers : {};
    const billAvailable = data.hasOwnProperty('bill_available') ? data.bill_available : true;
    let totalBillAmount = 0;
    if (billingApproval.state == 'billed') {
        totalBillAmount = billingApproval.hasOwnProperty('total_bill') ? billingApproval.total_bill : 0;
        totalBillAmount = reCalcule(totalBillAmount, detail, exclude, accept);
    } else {
        totalBillAmount = billingApproval.hasOwnProperty('total_payment') ? billingApproval.total_payment : 0;
    }
    return {billingApproval, accept, exclude, detail, customers, billAvailable, totalPayment: totalBillAmount};
}
function reCalcule(totalBillAmount, clients, exclude, accept) {
    if (Object.keys(exclude).length > 0) {
        Object.keys(exclude).map((key, index) => {
            const customerId = exclude[key]['customerId'];
            clients.map((client) => {
                if (client.clientExternalId == customerId) {
                    let outstandingAmount = client.previousOutstandingAmount + client.principalDueAmount - client.paidAmount;
                    if (client[outstandingAmount]) {
                        outstandingAmount = client[outstandingAmount];
                    }
                    totalBillAmount = totalBillAmount - outstandingAmount;
                }
            });
        });
    }

    if (Object.keys(accept).length > 0) {
        Object.keys(accept).map((key, index) => {
            const acceptClient = accept[key];
            const customerId = key;
            clients.map((client) => {
                if (client.clientExternalId == customerId && client.outstandingAmount != acceptClient.outstandingAmount) {
                    const outstandingAmount = client.previousOutstandingAmount + client.principalDueAmount - client.paidAmount;
                    totalBillAmount = totalBillAmount - (outstandingAmount - acceptClient.outstandingAmount);
                }
            });
        });
    }
    return totalBillAmount;
}

export function loadBillingInfoSuccess(payload) {
    return {type: types.LOAD_BILLING_INFO_SUCCESS, payload};
}

export function changePaymentAmountSuccess(payload) {
    return {type: types.CHANGE_PAYMENT_AMOUNT_SUCCESS, payload};
}
export function rejectClientSuccess(payload) {
    return {type: types.REJECT_CLIENT_SUCCESS, payload};
}
export function cancelRejectClientSuccess(payload) {
    return {type: types.CANCEL_REJECT_CLIENT_SUCCESS, payload};
}
export function saveRejectClientSuccess(payload) {
    return {type: types.SAVE_REJECT_CLIENT_SUCCESS, payload};
}
export function approveBillSuccess(payload) {
    return {type: types.APPROVE_BILL_SUCCESS, payload};
}
export function reopenBillSuccess(payload) {
    return {type: types.REOPEN_BILL_SUCCESS, payload};
}
export function voidBillSuccess(payload) {
    return {type: types.VOID_BILL_SUCCESS, payload};
}

export function loadBillingInfo(billingId) {
    return function (dispatch) {
        dispatch(beginAjaxCall());
        return settingApi.getBillingInfo(billingId).then(billingInfoResponse => {
            const payload = parseDataFromResponse(billingInfoResponse);
            dispatch(loadBillingInfoSuccess(payload));
        });
    };
}

export function changePaymentAmount(client, paymentChangeAmount) {
    return function (dispatch, getState) {
        const totalPayment = getState().billingSetting.totalPayment;
        let accept = getState().billingSetting.accept;
        // Find customer in detail to update payment Amount
        const clientLists = Object.assign([], getState().billingSetting.detail);
        let ownClient = {};
        let newAccept = {};
        if (getState().billingSetting.detail.length > 0) {
            ownClient = clientLists.find(c => {
                return c.clientId == client.clientId;
            });
            ownClient = Object.assign({}, ownClient, {outstandingAmount: paymentChangeAmount});
            newAccept = Object.assign({}, accept[client.clientExternalId], {outstandingAmount: paymentChangeAmount});
        }
        const outstandingAmountClient = accept[client.clientExternalId]['outstandingAmount'];
        const real = totalPayment - (outstandingAmountClient - paymentChangeAmount);
        dispatch(changePaymentAmountSuccess({totalPayment: real, ownClient: ownClient, newAccept: newAccept}));
    };
}
export function cancelRejectClient(client, index) {
    return function (dispatch) {
        dispatch(cancelRejectClientSuccess({showModal: false}));
    };
}
export function rejectClient(data, client) {
    return function (dispatch) {
        dispatch(rejectClientSuccess({showModal: true, currentClient: client, data: data}));
    };
}
export function saveRejectClient(billingId, data, client, totalBillPayment) {
    return function (dispatch, getState) {
        dispatch(beginAjaxCall());
        return settingApi.saveRejectClient(billingId, data, client).then(billingInfoResponse => {
            const oldAcceptList = getState().billingSetting.accept;

            const billingData = billingInfoResponse.data;
            const accept = billingData.hasOwnProperty('accept') ? billingData.accept : {};
            if (Object.keys(oldAcceptList).length !== Object.keys(accept).length) {
                const exclude = billingData.hasOwnProperty('exclude') ? billingData.exclude : {};
                const detail = billingData.hasOwnProperty('detail') ? billingData.detail : {};

                if (accept.hasOwnProperty(client.clientExternalId)) {
                    if (accept[client.clientExternalId]['outstandingAmount']) {
                        client.outstandingAmount = accept[client.clientExternalId]['outstandingAmount'];
                    }
                } else {
                    if (!client.outstandingAmount) {
                        client.outstandingAmount = (client.previousOutstandingAmount + client.principalDueAmount - client.paidAmount);
                    }
                }

                const totalPayment = (data['isExcluded'] == 0) ? (totalBillPayment + parseFloat(client.outstandingAmount)) : (totalBillPayment - parseFloat(client.outstandingAmount));
                let clientInDetail = detail.find(c => {
                    return c.clientExternalId == client.clientExternalId;
                });

                if (accept[client.clientExternalId]) {
                    clientInDetail = accept[client.clientExternalId];
                }
                dispatch(saveRejectClientSuccess({
                    detail: detail,
                    exclude: exclude,
                    accept: accept,
                    totalPayment: totalPayment,
                    clientInDetail: clientInDetail
                }));
            }
            else {
                dispatch(saveRejectClientSuccess({}));
            }
        });
    };
}
export function approveBill(billApprovalId, data) {
    return function (dispatch) {
        dispatch(beginAjaxCall());
        return settingApi.approveBill(billApprovalId, data).then(billingInfoResponse => {
            const payload = parseDataFromResponse(billingInfoResponse);
            dispatch(approveBillSuccess(payload));
        });
    };
}

export function reopenBill(id) {
    return function (dispatch) {
        dispatch(beginAjaxCall());
        return settingApi.reopenBill(id).then(billingInfoResponse => {
            const payload = parseDataFromResponse(billingInfoResponse);
            dispatch(reopenBillSuccess(payload));
        });
    };
}

export function voidBill(id) {
    return function (dispatch) {
        dispatch(beginAjaxCall());
        return settingApi.voidBill(id).then(billingInfoResponse => {
            const payload = parseDataFromResponse(billingInfoResponse);
            dispatch(voidBillSuccess(payload));
        });
    };
}


