import * as types from '../actionTypes';
import { beginAjaxCall, ajaxCallError } from '../ajaxStatusActions';
import confirmPaymentApi from '../../api/Billing/confirmPaymentApi';
import { browserHistory } from 'react-router';

function parseDataFromResponse (billingPaymentInfoResponse) {
    const data = billingPaymentInfoResponse.data;

    if (!data || !data.hasOwnProperty('billing')) {
        return { billPaymentAvailable: false, billPaymentInfo: {} };
    }
    return {
        billPaymentAvailable: true,
        billPaymentInfo: {
            id: data.id,
            billingState: data.billing.state,
            billingMonth: data.billing.billing_month,
            billingYear: data.billing.billing_year,
            totalPayment: data.billing.total_payment,
            paymentType: data.billing.payment_type,
            transactionAmount: data.transaction_amount,
            transactionDate: data.transaction_date,
            transactionType: data.transaction_type,
            bankAccount: data.bank_account,
            bankName: data.bank_name,
            branchName: data.branch_name,
            confirmNote: data.confirm_note || '',
            attachment: data.attachment || ''
        }
    }
}
export function loadBillingPaymentInfoSuccess (payload) {
    return { type: types.LOAD_BILLING_PAYMENT_INFO_SUCCESS, payload };
}
export function confirmBillingPaymentSuccess() {
    browserHistory.push('/billing/admin/overview');
}
export function loadBillingPaymentInfo(billingId, paymentId) {
    return function (dispatch) {
        dispatch(beginAjaxCall());
        return confirmPaymentApi.getBillingPaymentInfo(billingId, paymentId).then(billingPaymentInfoResponse => {
            const payload = parseDataFromResponse(billingPaymentInfoResponse);
            dispatch(loadBillingPaymentInfoSuccess(payload));
        });
    };
}
export function confirmPayment(billingId, paymentId, data) {
    return function (dispatch) {
        dispatch(beginAjaxCall());
        return confirmPaymentApi.confirmPayment(billingId, paymentId, data).then(response => {
            dispatch(confirmBillingPaymentSuccess());
        }).catch(errors => {
            console.log(errors);
        });
    };
}
