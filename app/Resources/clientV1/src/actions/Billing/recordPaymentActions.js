import * as types from '../actionTypes';
import {beginAjaxCall, ajaxCallError} from '../ajaxStatusActions';
import billingApi from '../../api/Billing/recordPaymentApi';

export function getBillingSuccess(recordPayment) {
    return {type: types.LOAD_BILLING_SUCCESS, recordPayment};
}
export function recordBillingPaymentSuccess (recordPayment) {
    return { type: types.RECORD_BILLING_PAYMENT_SUCCESS, recordPayment };
}

export function recordBillingPaymentError (recordPayment) {
    return { type: types.RECORD_BILLING_PAYMENT_ERROR, recordPayment };
}

export function getBillingAction(billApprovalId) {
    return function (dispatch) {
        dispatch(beginAjaxCall());
        return billingApi.getBillingApproval(billApprovalId).then(results => {
            dispatch(getBillingSuccess({billing: results.data, data: {}, errors: {}}));
        });
    }
}

export function recordBillingPaymentAction (data) {
    return function (dispatch) {
        dispatch(beginAjaxCall());
        return billingApi.recordBillingPayment(data).then(response => {
            dispatch(recordBillingPaymentSuccess({billing: response.billing, data: response.data, errors: {}, redirectUrl: response.redirectUrl}));
        }).catch(errors => {
            dispatch(recordBillingPaymentError({billing: errors.billing, data: {}, errors: errors.data, redirectUrl: errors.redirectUrl}));
            throw (errors);
        });
    };
}

