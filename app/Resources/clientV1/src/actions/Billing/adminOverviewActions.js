import * as types from '../actionTypes';
import {beginAjaxCall, ajaxCallError} from '../ajaxStatusActions';
import adminOverviewApi from '../../api/Billing/adminOverviewApi';
import exceptionMonthApi from '../../api/Billing/exceptionMonthApi';

export function updateBillingOverviewSuccess(payload) {
    return {type: types.LOAD_BILLING_ADMIN_OVERVIEW_SUCCESS, payload};
}

export function loadStatisticsSuccess(payload) {
    return {type: types.LOAD_BILLING_ADMIN_OVERVIEW_STATISTIC_SUCCESS, payload};
}

export function loadCustomersSuccess(payload) {
    return {type: types.LOAD_BILLING_ADMIN_OVERVIEW_CUSTOMERS_SUCCESS, payload};
}

export function updateExceptionMonthError(errors) {
    return {type: types.UPDATE_BILLING_EXCEPTION_MONTH_ERROR, errors};
}

export function loadBillingOverview() {
    return function (dispatch) {
        dispatch(beginAjaxCall());
        const statistic = adminOverviewApi.getStatistics();
        const exceptionMonth = exceptionMonthApi.getExceptionMonths();
        const customers = adminOverviewApi.getCustomers();

        return Promise.all([statistic, exceptionMonth, customers]).then(values => {
            dispatch(updateBillingOverviewSuccess(
                {
                    statistic: values[0],
                    exceptionMonth: values[1].data,
                    hasEditRole: values[1].hasEditRole,
                    customers: values[2].customers,
                    billings: values[2].billings,
                    statusesStatistic: values[2].statusesStatistic[0],
                    customerFilter: {
                        period: values[2].period,
                        state: values[2].state,
                        keyword: values[2].keyword
                    },
                }
            ));
        });
    };
}


export function loadStatistics(period) {
    return function (dispatch) {
        debugger;
        dispatch(beginAjaxCall());
        return adminOverviewApi.getStatistics(period).then(result=> {
            dispatch(loadStatisticsSuccess(
                {
                    statistic: result,
                    statisticPeriod: period,
                }
            ));
        });
    };
}

export function loadCustomers(conditions) {
    return function (dispatch) {
        dispatch(beginAjaxCall());
        //parser conditions to string url
        const urlConditions = Object.keys(conditions).map(function (k) {
            return encodeURIComponent(k) + "=" + encodeURIComponent(conditions[k]);
        }).join('&');

        const customers = adminOverviewApi.getCustomers(urlConditions);
        return Promise.all([customers]).then(values => {
            dispatch(loadCustomersSuccess(
                {
                    customers: values[0].customers,
                    billings: values[0].billings,
                    statusesStatistic: values[0].statusesStatistic[0],
                    customerFilter: {period: values[0].period, state: values[0].state, keyword: values[0].keyword}
                }
            ));
        });
    };
}

export function updateExceptionMonthData(data) {
    return function (dispatch) {
        dispatch(beginAjaxCall());
        return exceptionMonthApi.updateExceptionMonth(data).then(response => {
            dispatch(updateBillingOverviewSuccess(
                {
                    exceptionMonth: response.data,
                    hasEditRole: response.hasEditRole,
                }
            ));
        }).catch(errors => {
            dispatch(updateExceptionMonthError(errors));
            throw (errors);
        });
    };
}
