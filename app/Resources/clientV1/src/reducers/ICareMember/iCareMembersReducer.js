import * as types from '../../actions/actionTypes';
import initialState from '../initialState';

export default function iCareMembersReducer(state = initialState.iCareMembers, action) {
    switch (action.type) {
        case types.LOAD_ICAREMEMBERS_LIST_SUCCESS:
            return Object.assign({}, state, action.payload);
        default:
            return state;
    }
}
