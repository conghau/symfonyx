import * as types from '../../actions/actionTypes';
import initialState from '../initialState';

export default function iCareMemberReducer(state = initialState.iCareMember, action) {
    switch (action.type) {
        case types.LOAD_ICAREMEMBER_PERSONAL_INFO_SUCCESS:
            return Object.assign({}, state, action.payload);
        case types.LOAD_ICAREMEMBER_PERSONAL_INFO_ERROR:
            return Object.assign({}, state, action.payload);
        case types.UPDATE_ICAREMEMBER_PERSONAL_INFO_SUCCESS:
            return Object.assign({}, state, action.payload);
        case types.UPDATE_ICAREMEMBER_PERSONAL_INFO_ERROR:
            return Object.assign({}, state, action.payload);
        default:
            return state;
    }
}
