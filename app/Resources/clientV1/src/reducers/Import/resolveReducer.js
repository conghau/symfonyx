import * as types from 'actions/actionTypes';
import initialState from 'reducers/initialState';

export default function resolveReducer(state = initialState.employeeData, action) {
    switch (action.type) {
        case types.LOAD_EMPLOYEE_DATA_SUCCESS:
            return Object.assign({}, state, {
                data: action.employeeData,
                error: action.employeeError
            });
        case types.UPDATE_EMPLOYEE_DATA_SUCCESS:
            return Object.assign({}, state, {
                result: action.result
            });
        case types.UPDATE_EMPLOYEE_DATA_ERROR:
            return Object.assign({}, state, {
                data: action.employeeData,
                error: action.employeeError
            });
        default:
            return state;
    }
}
