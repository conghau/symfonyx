import * as types from 'actions/actionTypes';
import initialState from 'reducers/initialState';

export default function mappingReducer (state = initialState.selectMappings, action) {
  switch (action.type) {
    case types.LOAD_MAPPINGS_SUCCESS:
      return Object.assign({}, state, {
        mappings: action.mappings.mappings,
        columns: action.mappings.columns,
        selectedMappings: action.mappings.selectedMappings
      });
    case types.RESET_MAPPINGS_SUCCESS:
      return Object.assign({}, state, { selectedMappings: action.updateMappings });
    case types.UPDATE_MAPPINGS_SUCCESS:
      return Object.assign({}, state, { selectedMappings: action.updateMappings });

    case types.UPDATE_MAPPINGS_ERROR:
      return Object.assign({}, state, { errors: action.errors });

    case types.UPDATE_ITEM_MAPPING:
      return Object.assign({}, state, { selectedMappings: action.selectMappings, errors: action.newError });

    default:
      return state;
  }
}
