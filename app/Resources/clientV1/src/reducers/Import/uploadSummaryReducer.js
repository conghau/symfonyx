import * as types from 'actions/actionTypes';
import initialState from 'reducers/initialState';

export default function uploadSummaryReducer (state = initialState.uploadSummary, action) {
  switch (action.type) {
    case types.LOAD_UPLOAD_SUMMARY_SUCCESS:
      return Object.assign({}, state, action.payload);
    case types.LOAD_UPLOAD_SUMMARY_LIST_ERRORS_SUCCESS:
      return Object.assign({}, state, action.payload);
    default:
      return state;
  }
}
