import * as types from 'actions/actionTypes';
import initialState from 'reducers/initialState';

export default function processingReducer (state = initialState.processingResults, action) {
  switch (action.type) {
    case types.LOAD_PROCESSING_SUCCESS:
      return Object.assign({}, state, action.results);
    default:
      return state;
  }
}
