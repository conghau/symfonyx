import * as types from 'actions/actionTypes';
import initialState from 'reducers/initialState';
//
// import uploadFileApi from 'api/uploadFileApi';
//
// export function uploadFileSuccess (uploadFile) {
//   return { type: types.UPLOAD_FILE_SUCCESS, uploadFile };
// }
//
// export function uploadFile () {
//   return function (dispatch) {
//     dispatch(beginAjaxCall());
//     return uploadFileApi.uploadFile().then(uploadFile => {
//       dispatch(uploadFileSuccess(uploadFile));
//     });
//   };
// }
//
// export function loadUploadLatestListSuccess (uploadLatestList) {
//   return { type: types.LOAD_UPLOAD_LATEST_LIST_SUCCESS, uploadLatestList };
// }
//
// export function loadUploadLatestListError (errors) {
//   return { type: types.LOAD_UPLOAD_LATEST_LIST_ERROR, errors };
// }
//
// export function loadUploadLatestList () {
//   return function (dispatch) {
//     dispatch(beginAjaxCall());
//     return uploadFileApi.getUploadLatestList().then(results => {
//       if (results.data !== undefined) {
//         dispatch(loadUploadLatestListSuccess(results.data));
//       } else {
//         dispatch(loadUploadLatestListError(results));
//       }
//     });
//   };
// }

export default function uploadFileReducer (state = initialState.uploadFile, action) {
  switch (action.type) {
    case types.LOAD_UPLOAD_LATEST_LIST_SUCCESS:
      return Object.assign({}, state, {
        uploadLatestList: action.uploadLatestList
      });
    case types.LOAD_UPLOAD_LATEST_LIST_ERROR:
      return Object.assign({}, state, {
        errors: action.errors.code,
        content: action.errors.message
      });
    default:
      return state;
  }
}
