import * as types from 'actions/actionTypes';
import initialState from 'reducers/initialState';

export default function validateMappingReducer (state = initialState.validationResults, action) {
  switch (action.type) {
    case types.LOAD_VALIDATE_MAPPINGS_SUCCESS:
      return Object.assign({}, state, action.results);
    case types.START_IMPORT_SUCCESS:
      return Object.assign({}, state, action.results);
    default:
      return state;
  }
}
