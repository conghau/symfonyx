import { combineReducers } from 'redux';
import { intlReducer } from 'react-intl-redux';

export const rootReducer = (asyncReducers) => {
  return combineReducers({
    intl: intlReducer,
    ...asyncReducers
  });
};

export const injectReducer = (store, key, reducer) => {
  store.asyncReducers[key] = reducer;
  store.replaceReducer(rootReducer(store.asyncReducers));
};

export default rootReducer;
