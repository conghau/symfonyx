import * as types from '../../actions/actionTypes';
import initialState from '../initialState';

export default function recordPaymentReducer(state = initialState.recordPayment, action) {
    switch (action.type) {
        case types.LOAD_BILLING_SUCCESS:
            return Object.assign({}, state, {
                billing: action.recordPayment.billing,
                data: {},
                errors: {}
            });
        case types.RECORD_BILLING_PAYMENT_SUCCESS:
            return Object.assign({}, state, {
                billing: action.recordPayment.billing,
                data: action.recordPayment.data.data,
                errors: action.recordPayment.data.errors,
                redirectUrl: action.recordPayment.redirectUrl
            });
        case types.RECORD_BILLING_PAYMENT_ERROR:
            return Object.assign({}, state, {
                billing: action.recordPayment.billing,
                data: {},
                errors: action.recordPayment.errors.errors,
                redirectUrl: action.recordPayment.redirectUrl
            });
        default:
            return state;
    }
}
