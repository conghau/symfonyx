import * as types from '../../actions/actionTypes';
import initialState from '../initialState';

export default function settingReducer(state = initialState.billingSetting, action) {
    switch (action.type) {
        case types.LOAD_BILLING_INFO_SUCCESS:
            return Object.assign({}, state, action.payload);
        case types.CHANGE_PAYMENT_AMOUNT_SUCCESS:
            var payload = action.payload;
            var newDetail = state.detail;
            if (payload.hasOwnProperty('ownClient') && state.detail.length > 0) {
                const i = state.detail.findIndex((c) => {
                    return c.clientId == payload.ownClient.clientId;
                });
                newDetail = Object.assign([], state.detail, {[i]: payload.ownClient});
            }
            const newAccept = Object.assign([], state.accept, {[payload.newAccept['clientExternalId']]: payload.newAccept});

            return Object.assign({}, state, {detail: newDetail, totalPayment: payload.totalPayment, accept: newAccept});
        case types.REJECT_CLIENT_SUCCESS:
            return Object.assign({}, state, action.payload);
        case types.CANCEL_REJECT_CLIENT_SUCCESS:
            return Object.assign({}, state, action.payload);
        case types.SAVE_REJECT_CLIENT_SUCCESS:
            var payload = action.payload;
            if (Object.keys(payload).length > 0) {
                const i = state.detail.findIndex((c) => {
                    return c.clientId == payload.clientInDetail.clientId;
                });
                var newDetail = Object.assign([], state.detail, {[i]: payload.clientInDetail});
                let newAccept = payload.accept;

                // Check current client in list accept, => update new info from detail
                if (payload.accept[payload.clientInDetail['clientExternalId']]) {
                    newAccept = Object.assign({}, payload.accept, {[payload.clientInDetail['clientExternalId']]: payload.clientInDetail});
                }
                Object.keys(newAccept).map(customerId => {
                    if (state.accept[customerId]) {
                        newAccept = Object.assign({}, newAccept, {[customerId]: state.accept[customerId]});
                    }
                });

                return Object.assign({}, state, {
                    detail: newDetail,
                    exclude: payload.exclude,
                    accept: newAccept,
                    totalPayment: payload.totalPayment
                });
            }
            else {
                return state;
            }

        case types.APPROVE_BILL_SUCCESS:
            return Object.assign({}, state, action.payload);
        case types.REOPEN_BILL_SUCCESS:
            return Object.assign({}, state, action.payload);
        case types.VOID_BILL_SUCCESS:
            return Object.assign({}, state, action.payload);
        default:
            return state;
    }
}
