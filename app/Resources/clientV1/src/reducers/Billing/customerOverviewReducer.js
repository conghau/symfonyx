import * as types from '../../actions/actionTypes';
import initialState from '../initialState';

export default function customerOverviewReducer (state = initialState.customerOverview, action) {
  switch (action.type) {
    case types.LOAD_BILLING_CUSTOMER_OVERVIEW_STATISTIC_SUCCESS:
      return Object.assign({}, state, action.payload);
    case types.LOAD_BILLING_CUSTOMER_LIST_SUCCESS:
      return Object.assign({}, state, action.payload);
    default:
      return state;
  }
}
