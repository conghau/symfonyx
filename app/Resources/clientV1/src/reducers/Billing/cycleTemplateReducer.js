import * as types from '../../actions/actionTypes';
import initialState from '../initialState';

export default function cycleTemplateReducer (state = initialState.cycleTemplate, action) {
  switch (action.type) {
    case types.LOAD_BILLING_CYCLE_TEMPLATE_SUCCESS:
      return Object.assign({}, state, {
        data: action.cycleTemplate.data
      });
    case types.UPDATE_BILLING_CYCLE_TEMPLATE_SUCCESS:
      return Object.assign({}, state, {
        data: action.data.data,
        errors: action.data.errors
      });
    case types.CANCEL_BILLING_CYCLE_TEMPLATE:
      return Object.assign({}, state, {
        errors: {}
      });
    case types.UPDATE_BILLING_CYCLE_TEMPLATE_ERROR:
      return Object.assign({}, state, {
        errors: action.errors
      });
    default:
      return state;
  }
}
