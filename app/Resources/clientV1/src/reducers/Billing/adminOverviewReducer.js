import * as types from '../../actions/actionTypes';
import initialState from '../initialState';

export default function adminOverviewReducer(state = initialState.adminOverview, action) {
    switch (action.type) {
        case types.LOAD_BILLING_ADMIN_OVERVIEW_SUCCESS:
            return Object.assign({}, state, action.payload);
        case types.LOAD_BILLING_ADMIN_OVERVIEW_STATISTIC_SUCCESS:
            return Object.assign({}, state, action.payload);
        case types.LOAD_BILLING_ADMIN_OVERVIEW_CUSTOMERS_SUCCESS:
            return Object.assign({}, state, action.payload);
        default:
            return state;
    }
}
