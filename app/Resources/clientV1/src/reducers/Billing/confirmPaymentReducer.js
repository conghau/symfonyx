import * as types from '../../actions/actionTypes';
import initialState from '../initialState';

export default function confirmPaymentReducer(state = initialState.confirmPayment, action) {
    switch (action.type) {
        case types.LOAD_BILLING_PAYMENT_INFO_SUCCESS:
            return Object.assign({}, state, action.payload);
        default:
            return state;
    }
}
