export default {
    selectMappings: {
        mappings: [],
        columns: [],
        selectedMappings: {},
        errors: {}
    },
    uploadFile: {
        errors: {},
        content: {},
        uploadLatestList: []
    },
    validationResults: {
        errors: {},
        content: {}
    },
    processingResults: {
        statuses: {}
    },
    uploadSummary: {
        topErrors: {},
        listErrorRecords: {},
        statuses: {}
    },
    employeeData: {
        data: {},
        result: null
    },
    ajaxCallsInProgress: 0,
    billingSetting: {
        billingApproval: {},
        detail: [],
        exclude: {},
        accept: {},
        customers: {},
        totalPayment: 0,
        showModal: false,
        currentClient: {},
        billAvailable: true
    },
    cycleTemplate: {
        data: {},
        errors: {}
    },
    customerOverview: {
        statistics: {},
        exceptionMonths: {},
        latestBillingInfo: {},
        billingList: [],
        totalRecordsBillingList: '0'
    },
    iCareMember: {
        id: 0,
        personalInfo: {},
        errors: {},
        message: {}
    },
    adminOverview: {
        statistic: {},
        exceptionMonth: {},
        hasEditRole: false,
        customers: [],
        billings: [],
        statusesStatistic: {}
    },
    recordPayment: {
        data: {},
        errors: {},
        billing: [],
        redirectUrl: ""
    },
    confirmPayment: {
        billPaymentAvailable: false,
        billPaymentInfo: {}
    },
    iCareMembers: {
        data: {},
        errors: {},
        message: {},
        listMembers: []
    },
};
