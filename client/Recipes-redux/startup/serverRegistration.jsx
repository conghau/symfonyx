import ReactOnRails from 'react-on-rails';
import RecipesApp from './RecipesAppServer';
import configureStore from '../store/RecipesStore';

var recipesStore = configureStore;

ReactOnRails.registerStore({recipesStore})
ReactOnRails.register({ RecipesApp });
