<?php

namespace AppBundle\Entity;

class Recipe
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $slug;

    /**
     * @var string
     */
    public $image;
}
