<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\Comment;
use AppBundle\Entity\Post;

/**
 * Class RecipeController
 * @package AppBundle\Controller
 *
 * @Route("/recipe")
 */
class RecipeController extends Controller
{
    /**
     * @Route("/l", defaults={"page": 1}, name="recipe_index")
     * @Route("/page/{page}", requirements={"page": "[1-9]\d*"}, name="recipe_index_paginated")
     * @Method("GET")
     * @Cache(smaxage="10")
     */
    public function indexAction($page)
    {
        $posts = $this->getDoctrine()->getRepository('AppBundle:Post')->findLatest($page);

        return $this->render('blog/index.html.twig', array('posts' => $posts));
    }

    /**
     * @Route("/", name="recipe_home")
     * @Method("GET")
     */
    public function homeAction(Request $request)
    {
        $serializer = $this->get('serializer');
        $posts = $this->getDoctrine()->getRepository('AppBundle:Post')->findLatest(1);
        return $this->render('recipe/home.html.twig', [
            // We pass an array as props
            'props' => json_encode(
                [
                    'recipes' => $posts,
                    // '/' or maybe '/app_dev.php/', so the React Router knows about the root
                    'baseUrl' => $this->generateUrl('homepage'),
                    'location' => $request->getRequestUri()
                ])
        ]);
    }

    /**
     * @Route("/recipe/{slug}", name="recipe")
     */
    public function recipeAction($slug, Request $request)
    {
        $serializer = $this->get('serializer');
        if (!$recipe = $this->get('recipe.manager')->findOneBySlug($slug)) {
            throw $this->createNotFoundException('The recipe does not exist');
        }
        return $this->render('recipe/recipe.html.twig', [
            // A JSON string also works
            'props' => $serializer->serialize(
                ['recipe' => $this->get('recipe.manager')->findOneBySlug($slug),
                    'baseUrl' => $this->generateUrl('homepage'),
                    'location' => $request->getRequestUri()
                ], 'json')
        ]);
    }

    /**
     * @Route("/api/recipes", name="api_recipes")
     *
     * Needed for client-side navigation after initial page load
     */
    public function apiRecipesAction(Request $request)
    {
        $serializer = $this->get('serializer');
        return new JsonResponse($this->get('recipe.manager')->findAll()->recipes);
    }

    /**
     * @Route("/api/recipes/{slug}", name="api_recipe")
     *
     * Needed for client-side navigation after initial page load
     */
    public function apiRecipeAction($slug, Request $request)
    {
        $serializer = $this->get('serializer');
        return new JsonResponse($this->get('recipe.manager')->findOneBySlug($slug));
    }
}
