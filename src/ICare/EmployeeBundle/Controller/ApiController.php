<?php

namespace ICare\EmployeeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiController extends Controller
{
    public function listAction()
    {
//        $entityManager = $this->getDoctrine()->getManager();
//        $posts = $entityManager->getRepository('AppBundle:Post')->findAll();
        $data = [];

        for($i=1; $i<=5; $i++) {
            $a = new \stdClass();
            $a->id = $i;
            $a->last_name = "Hau $i";
            $a->first_name = "Truong $i";
            $a->status = true;
            $data['data'][] = $a;
        }
        return new JsonResponse($data);
    }
}
