<?php
/**
 * Created by PhpStorm.
 * User: conghau
 * Date: 23/11/2016
 * Time: 00:45
 */

namespace ICare\EmployeeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Yaml\Yaml;

/**
 * Class ImportController
 *
 * @package ICare\EmployeeBundle\Controller
 */
class ICareMemberController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $file = __DIR__."/../Resources/translations/messages.".$request->getLocale().".yml";
        $trans = Yaml::parse(file_get_contents($file));
        $iCareMembersTranslations = $trans['employee']['icare_members'];

        return $this->render(
            'ICareEmployeeBundle:ICareMember:index.html.twig',
            array(
                'iCareLocale' => $iCareMembersTranslations,
            )
        );
    }
}
