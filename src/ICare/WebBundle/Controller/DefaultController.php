<?php

namespace ICare\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ICareWebBundle:Default:index.html.twig');
    }
}
