<?php

namespace ICare\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ICareUserBundle:Default:index.html.twig');
    }
}
